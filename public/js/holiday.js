// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_holiday")) {
        table = $('#table_holiday').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/holiday/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.is_deleted = jQuery("#is_deleted").val();

                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name',  orderable: true, searchable: false},
                { data: 'holiday_date',  name: 'holiday_date', orderable: true, searchable: false},
                { data: 'created_at', name: 'created_at', searchable: false },
                { data: 'status', name: 'status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })
    

    // Function to format a date string to "m-d-Y" format
    function formatDate(dateString) {
        var date = new Date(dateString);
        var month = (date.getMonth() + 1).toString().padStart(2, '0'); // Add 1 to the month because it's 0-indexed
        var day = date.getDate().toString().padStart(2, '0');
        var year = date.getFullYear();
        return month + '-' + day + '-' + year;
    }
    
    var DateArray = $('#dateArr').val(); 
    var dateArr = JSON.parse(DateArray);
    var disabledDates = [];
    // Construct & format the disabled dates based on the dateArr
    for (var i = 0; i < dateArr.length; i++) {
        var holiday_date = dateArr[i];
        disabledDates.push(formatDate(holiday_date));
    }

    // Calculate the maximum date (today + 5 year)
    var maxDate = new Date();
    maxDate.setFullYear(maxDate.getFullYear() + 5);
    maxDate.setMonth(11); // December (0-indexed, so 11 is December)
    maxDate.setDate(31); // 31st day of December
    
    // //Start Highlight date
    // var dateStrings = ['2023-10-23', '2023-10-24', '2023-10-25'];
    // var unixTimestamps = [];
    // // Loop through the date strings and convert them to Unix timestamps
    // for (var i = 0; i < dateStrings.length; i++) {
    //     var dateString = dateStrings[i];
    //     var dateParts = dateString.split('-'); // Split the date string into parts
    //     var year = parseInt(dateParts[0], 10); // Parse year as an integer
    //     var month = parseInt(dateParts[1], 10) - 1; // Parse month as an integer (subtract 1 as months are 0-indexed)
    //     var day = parseInt(dateParts[2], 10); // Parse day as an integer

    //     var unixTimestamp = Date.parse(new Date(year, month, day));
    //     unixTimestamps.push(unixTimestamp); // Add the Unix timestamp to the array
    // }
    // //End


    var date1 = $("#holiday_date").flatpickr({
        enableTime: false,
        dateFormat: "m-d-Y",
        minDate: "today",
        maxDate: maxDate,
        disable: [
            function(date) {
                // Check if the date is a Saturday (6) or Sunday (0) or in disabledDates
                var day = date.getDay();
                var formattedDate = formatDate(date);
    
                // Disable Saturdays (6), Sundays (0), and dates in disabledDates
                return day === 6 || day === 0 || disabledDates.includes(formattedDate);
            }
        ],
    });
    // flatpickr('#holiday_date', {
    //     onDayCreate: function(dObj, dStr, fp, dayElem) {
    //         if (unixTimestamps.indexOf(+dayElem.dateObj) !== -1) {
    //             dayElem.className += " cool-date";
    //             // var toolTip = document.createElement('span');
    //             // toolTip.innerHTML = 'Oh hi there.';
    //             // dayElem.appendChild(toolTip);
    //         }
    //     }
    // });
    
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
                'name': {
                    maxlength:50,
                    require_field: true,
                    nospaces: true,
                },
                'description': {
                    maxlength: 500,
                    nospaces: true 
                }
            },
            messages: {
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});