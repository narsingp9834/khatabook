initializeFlatpickr(0);
$(document).ready(function () {
    $('.select2').select2({
        width: '100%',
    });
    
    $(".addg").attr('disabled',true);
    updateAddgFields();

    $('input[name="checkGenerator"]').on("change", function () {
        var checkbox = $(this);
        if (checkbox.is(':checked') && checkbox.val() === "on") {
            // Show loader when the checkbox is changed
            // $('#loader').show();
            // if($('#address_type_id').val() != ''){
            //     checkAddressType(function (result) {
            //         // Hide the loader after the checkAddressType function completes
            //         $('#loader').hide();
            
            //         if (result) {
            //             checkbox.prop('checked', true);
            
            //             if (checkbox.val() == "on") {
            //                 $(".addg").attr("disabled", false);
            //             } else {
            //                 $(".addg").attr("disabled", true);
            //             }
            //         } else {
            //             checkbox.prop('checked', false);
            //             $(".addg").attr("disabled", true);
            //             var addressTypeMsg = $('#addressTypeMsg').val();
            //             bootbox.alert({
            //                 message: addressTypeMsg,
            //                 backdrop: true
            //             });
            //         }
            //     });
            // }else{
            //     bootbox.alert({
            //         message: "Please Select Address Type.",
            //         backdrop: true,
            //         callback: function () {
            //           $('#loader').hide();
            //           checkbox.prop('checked', false);
            //           $(".addg").attr("disabled", true);
            //       }
            //     });
            // }
            $(".addg").attr("disabled", false);
        }else{
            $(".addg").attr("disabled", true);
        }
      });

    // $("#address_type_id").on("change", function (e) {

    //     if ($('input[name="checkGenerator"]:checked').val() == "on") {
    //         $(".addg").attr("disabled", false);
    //     } else {
    //         $(".addg").attr("disabled", true);
    //     }
    // });
        
    
    if($('input[name="checkGenerator"]:checked').val() == 'on'){
        $('#checkGenerator').trigger('change');
        $(".addg").attr('disabled',false);
    }else{
        $(".addg").attr('disabled',true);
    }

    $("#customer_type").change(function(){
        if($(this).val() == "Residential"){
            $("#custDiv").removeClass('d-none');
            $("#comDiv").addClass('d-none');
        }else{
            $("#custDiv").addClass('d-none');
            $("#comDiv").removeClass('d-none');
        }
    });
});

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("validate_email", function(value, element) {
            if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Please enter valid email");
        jQuery.validator.addMethod("require_field", function(value, element) {
            if(value.trim() ==''){
                return false;
            }
            return true;
        }, "This field is required.");
        jQuery.validator.addMethod("phoneNoValidation", function(value, element) {
            if(value == "(000)000-0000" || value == "0000000000"){
            return false;
            }
            return true;
        }, "* Please enter valid phone no");
        jQuery.validator.addMethod("strongPassword", function(value) {
            return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
        },"The password must contain at least 1 number, 1 lower case letter,1 upper case letter and 1 special symbol"); 

        jqForm.validate({
            rules: {
                'user_fname': {
                    maxlength:50,
                    require_field: true,
                },
                'user_lname': {
                    maxlength:50,
                    require_field: true,
                },
                'email': {
                    required: true,
                    maxlength:50,
                    email: true,
                    validate_email: true,
                    require_field: true,
                },
                'password': {
                    minlength:8,
                    maxlength: 30,
                    required: true,
                    require_field: true,
                    strongPassword:true,
                },
                'user_phone_no': {
                    phoneNoValidation:true,
                    required: true,
                    minlength: 13
                },
                'generator_name':{
                    require_field: true
                },
                'model_number':{
                    required_field: true
                },
                'serial_number':{
                    required_field: true
                },
                'address_line_1':{
                    require_field: true
                }
            },
            messages: {
                'user_phone_no': {
                    phoneNoValidation:"Please enter valid phone no",
                    minlength :"Please enter at least 10 characters."
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            }
            // submitHandler: function (form) {
            //     $('#loader').show(); 
            //     form.submit();
            //   }
        });
    }

    $("#user_phone_no").on("keydown", function (e) {
        check_phone_format();
    });
});

function check_phone_format()
{
    $('#user_phone_no').on('input', function () {
        var number = $(this).val().replace(/[^\d]/g, '');

        if (number.length == 4) {
            number = number.replace(/(\d\d\d)/, "($1)");
        }
        else if (number.length == 7) {
            number = number.replace(/(\d\d\d)(\d\d\d)/, "($1)$2");
        }
        else if (number.length == 10) {
            number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
        }
        else
        {
            number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
        }
        $(this).val(number);
        $('#user_phone_no').attr({maxlength: 13});
    });
}

function updateAddgFields() {
    if ($('input[name="checkGenerator"]:checked').val() == "on") {
      $('#checkGenerator').trigger('change');
      $(".addg").prop("disabled", false);
    } else {
      $(".addg").prop("disabled", true);
    }
}

function checkAddressType(callback) {
    var addressTypeId = $("#address_type_id").val();
    var token = jQuery("input[name='_token']").val();
    $.ajax({
      url: base_url + "/common/checkAddressType",
      method: "POST",
      data: {
        _token: token,
        id: addressTypeId,
      },
      dataType: "json",
      success: function (result) {
          callback(result.result);
      },
    });
}

$('#sameAsAbove').on("change", function () {
    // Show loader when the checkbox is changed
    var addresscheckbox = $(this);
    if (addresscheckbox.is(':checked') && addresscheckbox.val() === "on") {
        copyAddressFields();
    }else{
        removeAddressValues();
    }
  });