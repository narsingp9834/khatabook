$('.select2').select2({
    width: '100%',
});
// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_technician_leave")) {
        table = $('#table_technician_leave').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/leave/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.technician_id = jQuery("#technician_id").val();
                    data.leaveType = jQuery("#leave_type").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'user_name', name: 'user_name'},
                { data: 'leave_type', name: 'leave_type',  orderable: true, searchable: true},
                { data: 'from_date',  name: 'technician_leaves.from_date', orderable: true, searchable: false},
                { data: 'to_date',  name: 'technician_leaves.to_date', orderable: true, searchable: false},
                // { data: 'applied_by',  name: 'applied_by', orderable: true, searchable: false},
                { data: 'created_at', name: 'technician_leaves.created_at', searchable: false },
                { data: 'status', name: 'technician_leaves.status', orderable: true, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })

    $('#technician_id').on('change', function () {
        table.draw();
    })
    
    $('#leave_type').on('change', function () {
        table.draw();
    })

    // Function to format a date string to "m-d-Y" format
    function formatDate(dateString) {
        var date = new Date(dateString);
        var month = (date.getMonth() + 1).toString().padStart(2, '0'); // Add 1 to the month because it's 0-indexed
        var day = date.getDate().toString().padStart(2, '0');
        var year = date.getFullYear();
        return month + '-' + day + '-' + year;
    }
    if (document.getElementById("dateArr") || document.getElementById("holidayDateArr")) {

        var DateArray = $('#dateArr').val(); 
        var dateArr = JSON.parse(DateArray);
        var disabledRanges = [];
        // Construct the disabled date ranges based on the dateArr
        for (var i = 0; i < dateArr.length; i++) {
            var from = dateArr[i].from_date;
            var to = dateArr[i].to_date;
            disabledRanges.push({                
                from: formatDate(from),
                to: formatDate(to)
            });
        }  

        // Holiday
        var HolidayDateArray = $('#holidayDateArr').val(); 
        var holidayDateArr = JSON.parse(HolidayDateArray);
        var holidayDates = [];
        // Construct & format the disabled dates based on the holidayDateArr
        for (var i = 0; i < holidayDateArr.length; i++) {
            var holiday_date = holidayDateArr[i];
            holidayDates.push(formatDate(holiday_date));
        }

        // Calculate the maximum date (today + 1 year)
        var maxDate = new Date();
        maxDate.setFullYear(maxDate.getFullYear() + 1);  
    
        var date1 = $("#from_date").flatpickr({
            enableTime: false,
            dateFormat: "m-d-Y",
            minDate: "today",
            maxDate: maxDate,
            // disable: disabledRanges,
            disable: [
                function(date) {
                    // Check if the date is a Saturday (6) or Sunday (0)
                    var day = date.getDay();
                    var formattedDate = formatDate(date);

                    // Disable Saturdays (6) and Sundays (0)
                    if (day === 6 || day === 0) {
                        return true;
                    }

                    // Check if the date is in the disabledRanges
                    for (var i = 0; i < disabledRanges.length; i++) {
                        if (formattedDate >= disabledRanges[i].from && formattedDate <= disabledRanges[i].to) {
                            return true;
                        }
                    }

                    // Check if the date is in the holidayDates
                    if (holidayDates.includes(formattedDate)) {
                        return true;
                    }

                    return false;
                }
            ],
            onChange: function(selectedDates, dateStr, instance) {
                date2.set('minDate', dateStr)
            }
        });
        var from_date2 = $('#from_date').val();

        var date2 = $("#to_date").flatpickr({
            enableTime: false,
            dateFormat: "m-d-Y",
            minDate: from_date2,
            maxDate: maxDate,
            // disable: disabledRanges,
            disable: [
                function(date) {
                    // Check if the date is a Saturday (6) or Sunday (0)
                    var day = date.getDay();
                    var formattedDate = formatDate(date);

                    // Disable Saturdays (6) and Sundays (0)
                    if (day === 6 || day === 0) {
                        return true;
                    }

                    // Check if the date is in the disabledRanges
                    for (var i = 0; i < disabledRanges.length; i++) {
                        if (formattedDate >= disabledRanges[i].from && formattedDate <= disabledRanges[i].to) {
                            return true;
                        }
                    }

                    // Check if the date is in the holidayDates
                    if (holidayDates.includes(formattedDate)) {
                        return true;
                    }

                    return false;
                }
            ],
            // onOpen: function(selectedDates, dateStr, instance) {
            //     // date2.set('minDate', date1.selectedDates[0]);
            //     if(from_date2 != ''){
            //         date2.set('minDate', from_date2);
            //     }
            // }
        });
        $('#from_date').prop("readonly", false);
        $('#to_date').prop("readonly", false);
        
        if(document.getElementById("technician")) {
            $('#technician').on('change', function() {
                var user_id = $(this).val();
                $('#loader').show();
                $.ajax({
                    type: 'GET',
                    dataType: "JSON",
                    url: base_url+'/leave/get-dates/'+user_id,
                    success: function(data) {
                        $('#loader').hide();
                    // success
                        var status = data.status;
                        if(status == 'success') {
                            // toastr.success(msg);
                            var technician_leaves = data.technician_leaves;
                            var holidays = data.holidays;
                            //Leaves
                            var DateArray = technician_leaves; 
                            var dateArr = JSON.parse(DateArray);
                            var disabledRanges = [];
                            // Construct the disabled date ranges based on the dateArr
                            for (var i = 0; i < dateArr.length; i++) {
                                var from = dateArr[i].from_date;
                                var to = dateArr[i].to_date;
                                var type = dateArr[i].leave_type;
                                if(type == 'day'){
                                    disabledRanges.push({                
                                        from: formatDate(from),
                                        to: formatDate(to)
                                    });
                                }
                                // if(type == 'slot'){
                                //     var slots = dateArr[i].slot_id;
                                //     const slotsArray = slots.split(",");

                                //     // Loop through each checkbox and disable if its value is in the array
                                //     $('#jquery-val-form input[type="checkbox"]').each(function () {
                                //         var checkboxValue = $(this).val();
                                //         if ($.inArray(checkboxValue, slotsArray) !== -1) {
                                //             $(this).prop('disabled', true);
                                //         }
                                //     });
                                // }
                            }    
                            
                            // Holiday
                            var HolidayDateArray = holidays; 
                            var holidayDateArr = JSON.parse(HolidayDateArray);
                            var holidayDates = [];
                            // Construct & format the disabled dates based on the holidayDateArr
                            for (var i = 0; i < holidayDateArr.length; i++) {
                                var holiday_date = holidayDateArr[i];
                                holidayDates.push(formatDate(holiday_date));
                            }
                            $("#from_date").flatpickr().destroy();
                            $("#to_date").flatpickr().destroy();

                            var date1 = $("#from_date").flatpickr({
                                enableTime: false,
                                dateFormat: "m-d-Y",
                                minDate: "today",
                                maxDate: maxDate,
                                // disable: disabledRanges,
                                disable: [
                                    function(date) {
                                        // Check if the date is a Saturday (6) or Sunday (0)
                                        var day = date.getDay();
                                        var formattedDate = formatDate(date);
                    
                                        // Disable Saturdays (6) and Sundays (0)
                                        if (day === 6 || day === 0) {
                                            return true;
                                        }
                    
                                        // Check if the date is in the disabledRanges
                                        for (var i = 0; i < disabledRanges.length; i++) {
                                            if (formattedDate >= disabledRanges[i].from && formattedDate <= disabledRanges[i].to) {
                                                return true;
                                            }
                                        }
                    
                                        // Check if the date is in the holidayDates
                                        if (holidayDates.includes(formattedDate)) {
                                            return true;
                                        }
                    
                                        return false;
                                    }
                                ],
                                onChange: function(selectedDates, dateStr, instance) {
                                    date2.set('minDate', dateStr)
                                }
                            });
                            var date2 = $("#to_date").flatpickr({
                                enableTime: false,
                                dateFormat: "m-d-Y",
                                // minDate: date1,
                                maxDate: maxDate,
                                // disable: disabledRanges,
                                disable: [
                                    function(date) {
                                        // Check if the date is a Saturday (6) or Sunday (0)
                                        var day = date.getDay();
                                        var formattedDate = formatDate(date);
                    
                                        // Disable Saturdays (6) and Sundays (0)
                                        if (day === 6 || day === 0) {
                                            return true;
                                        }
                    
                                        // Check if the date is in the disabledRanges
                                        for (var i = 0; i < disabledRanges.length; i++) {
                                            if (formattedDate >= disabledRanges[i].from && formattedDate <= disabledRanges[i].to) {
                                                return true;
                                            }
                                        }
                    
                                        // Check if the date is in the holidayDates
                                        if (holidayDates.includes(formattedDate)) {
                                            return true;
                                        }
                    
                                        return false;
                                    }
                                ],
                            });
                            $('#from_date').prop("readonly", false);
                            $('#to_date').prop("readonly", false);
                        }else{
                            $('#loader').hide();
                            toastr.error('Something went wrong');
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }              
                    },
                });
            })

            $('#from_date').on('change',function(){
                $('#reason').val('');
                $('#jquery-val-form input[type="checkbox"]').each(function () {
                    // Reset each checkbox to its initial state (unchecked) and enable them
                    $(this).prop('checked', false).prop('disabled', false);
                });
                var user_id = $('#technician').val();
                var date    = $('#from_date').val();
                $.ajax({
                    type: 'GET',
                    dataType: "JSON",
                    url: base_url+'/leave/get-slots/'+user_id+'/'+date,
                    success: function(data) {
                        $('#loader').hide();
                    // success
                        var status = data.status;
                        if(status == 'success') {
                            var slotsArray = data.technician_slots;

                            var response  = JSON.parse(data.technician_slots);
                            var slotsArray = response[0].slot_id.split(',');
                           
                            // Loop through each checkbox and disable if its value is in the array
                            $('#jquery-val-form input[type="checkbox"]').each(function () {
                                var checkboxValue = $(this).val();
                                if ($.inArray(checkboxValue, slotsArray) !== -1) {
                                    $(this).prop('checked', true);
                                }
                            });
                            var reason = response[0].reason;
                            $('#reason').val(reason);
                        }else{
                            $('#loader').hide();
                            toastr.error('Something went wrong');
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }              
                    },
                });
            })

        }

        // var unixTimestamps = [];
        // // Loop through the date strings and convert them to Unix timestamps
        // for (var i = 0; i < holidayDates.length; i++) {
        //     var dateString = holidayDates[i];
        //     var dateParts = dateString.split('-'); // Split the date string into parts
        //     var year = parseInt(dateParts[0], 10); // Parse year as an integer
        //     var month = parseInt(dateParts[1], 10) - 1; // Parse month as an integer (subtract 1 as months are 0-indexed)
        //     var day = parseInt(dateParts[2], 10); // Parse day as an integer

        //     var unixTimestamp = Date.parse(new Date(year, month, day));
        //     unixTimestamps.push(unixTimestamp); // Add the Unix timestamp to the array
        // }

        // flatpickr('#from_date', {
        //     enableTime: false,
        //     dateFormat: "m-d-Y",
        //     minDate: "today",
        //     maxDate: maxDate,
        //     disable: disabledRanges,
        //     onChange: function(selectedDates, dateStr, instance) {
        //         date2.set('minDate', dateStr)
        //     },
        //     onDayCreate: function(dObj, dStr, fp, dayElem) {
        //         if (unixTimestamps.indexOf(+dayElem.dateObj) !== -1) {
        //             dayElem.className += " cool-date";
        //             // var toolTip = document.createElement('span');
        //             // toolTip.innerHTML = 'Oh hi there.';
        //             // dayElem.appendChild(toolTip);
        //         }
        //     }
        // });

    }

    if (document.getElementsByClassName("cancel-leave")) {
        $(document).on('click', '.cancel-leave', function(e) {
            var technician_leave_id = $(this).attr('data-technician_leave_id');
            var status = 'Cancelled';
            bootbox.confirm({
                title: "Cancel",
                message: "Are you sure you want to cancel your leave?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-secondary'
                    }
                },
                callback: function (result) {
                    if (result == true) {
                        $('#loader').show();
                        $.ajax({
                            type: 'GET',
                            dataType: "JSON",
                            url: base_url+'/leave/cancel/'+technician_leave_id+'/'+status,
                            success: function(data) {
                                $('#loader').hide();
                            // success
                                var status = data.status;
                                var msg = data.msg;
                                if(status == 'success') {
                                    toastr.success(msg);
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 2000);
                                }else{
                                    toastr.error('Something went wrong');
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 2000);
                                }              
                            },
                        });
                    }
                }
            });
        })
    }
    if (document.getElementById("leave_status")) {
        $('#leave_status').on('change', function(){
            var technician_leave_id = $('#technician_leave_id').val();
            var status = $(this).val();
            $('#loader').show(); 
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: base_url+'/leave/cancel/'+technician_leave_id+'/'+status,
                success: function(data) {
                // success
                    var status = data.status;
                    var msg = data.msg;
                    $('#loader').hide(); 
                    if(status == 'success') {
                        toastr.success(msg);
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }else{
                        toastr.error('Something went wrong');
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }              
                },
            });
        })
    }
    
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
                'reason': {
                    maxlength:500,
                    require_field: true,
                    nospaces: true,
                },

            },
            messages: {
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }

    $('#submitLeave').on('click', function (e) {
        $('#loader').show();
        e.preventDefault();
    
        if ($('#jquery-val-form').valid()) {
            var checkedValues = [];

            $('.slots:checked').each(function() {
                checkedValues.push($(this).val());
            });
            var token = jQuery("input[name='_token']").val();
            var from_date = jQuery("#from_date").val();
            var to_date = jQuery("#to_date").val();
            var technician_id = jQuery("#technician").val();
            $.ajax({
                url: base_url +'/leave/checkTechAptByDate',
                method: 'POST',
                data: {
                    '_token': token,
                    'technician_id': technician_id,
                    'from_date' : from_date,
                    'to_date' : to_date,
                    'slotArray': checkedValues,
                },
                dataType: "json",
                success: function (data) {
                    $('#loader').hide();
                    if(data.result == true){

                         bootbox.confirm({
                                title: "Blockout",
                                message: data.msg,
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-danger'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-secondary'
                                    }
                                },
                                callback: function (result) {
                                    if (result == true) {
                                        $('#jquery-val-form').submit();
                                    }
                                }
                            });
                    }else{
                        $('#loader').hide();
                        $('#jquery-val-form').submit();
                    }
                },
                error: function (data) {
                    console.log(data.status + ':' + data.statusText,data.responseText);
                }
            })
        }else{
            $('#loader').hide();
        }
    });

    $('#leaveType').on('change', function () {
       var val = $(this).val();
       getSlotDiv(val);
        
    })

    if($('#leaveType').val() != ''){
        var val = $('#leaveType').val();
        getSlotDiv(val);
    }

    function getSlotDiv(val){
        if(val == 'day'){
            $('.fullDayDiv').show();
            $('.slotsDiv').hide();
            $('.slots').attr('required',false);
       }else if(val == 'slot'){
            $('.slotsDiv').show();
            $('.fullDayDiv').hide();
            $('.to_date').attr('required',false);
            $('.slots').attr('required',true);
       }else{
            $('.slotsDiv').hide();
            $('.fullDayDiv').hide();
            $('.slots').attr('required',false);
            $('.to_date').attr('required',false);
       }
    }       
});