// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_transfer_switch")) {
        table = $('#table_transfer_switch').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/transfer-switch/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.type = jQuery("#type").val();
                    data.manufacturer = jQuery("#manufacturer").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'transfer_switch_name',  name: 'transfer_switch_name', orderable: true, searchable: true},
                { data: 'manufacturer_name',  name: 'manufacturers.manufacturer_name', orderable: true, searchable: true},
                { data: 'model_number',  name: 'model_number', orderable: true, searchable: true},
                { data: 'serial_number',  name: 'serial_number', orderable: true, searchable: true},
                { data: 'amperage',  name: 'amperage', orderable: true, searchable: true},
                { data: 'switch_type_name',  name: 'transfer_switch_types.switch_type_name', orderable: true, searchable: true},
                { data: 'created_at', searchable: false },
                { data: 'transfer_switch_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })

    $('#type').on('change', function () {
        table.draw();
    })
    
    $('#manufacturer').on('change', function () {
        table.draw();
    })
    
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
                'transfer_switch_name' : {
                    required: true,
                    require_field: true,
                },
                'model_number' : {
                    required: true,
                    require_field: true,
                },
                'serial_number' : {
                    required: true,
                    require_field: true,
                },
                'amperage' : {
                    required: true,
                    require_field: true,
                },
                'zone_id': {
                    required: true,
                },
                'zip-code': {
                    required: true,
                    maxlength: 5,
                    require_field: true,
                },
            },
            messages: {
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

if (document.getElementsByClassName('editTransferSwitch').length > 0) {
    // Get the value of installed_date
    const installedDateValue = $("#installed_date").val();

    // Check if the installedDateValue is not empty
    if (installedDateValue) {
        // Split the date string into day, month, and year components
        const dateComponents = installedDateValue.split("-");
        if (dateComponents.length === 3) {
            const day = parseInt(dateComponents[2]);
            const month = parseInt(dateComponents[1]) - 1; // Months are 0-indexed
            const year = parseInt(dateComponents[0]);

            // Create a Date object and set it one day in the future
            const installedDate = new Date(year, month, day);
            installedDate.setDate(installedDate.getDate() + 1);

            // Format the installedDate as "Y-m-d"
            const formattedInstalledDate = `${year}-${month + 1}-${day + 1}`;

            // Initialize Flatpickr for warranty_expiration_date with minDate set
            const warrantyPicker = flatpickr("#warranty_expiration_date", {
                dateFormat: "Y-m-d",
                minDate: formattedInstalledDate,
            });

            // Get the value of warranty_expiration_date
            const warrantyDateValue = $("#warranty_expiration_date").val();

            // Check if the warrantyDateValue is not empty
            if (warrantyDateValue) {
                const warrantyDateComponents = warrantyDateValue.split("-");
                if (warrantyDateComponents.length === 3) {
                    const warrantyDay = parseInt(warrantyDateComponents[2]);
                    const warrantyMonth = parseInt(warrantyDateComponents[1]) - 1;
                    const warrantyYear = parseInt(warrantyDateComponents[0]);

                    const warrantyDate = new Date(warrantyYear, warrantyMonth, warrantyDay);
                    warrantyPicker.setDate(warrantyDate);
                }
            }
        } else {
            console.log("Invalid date format: " + installedDateValue);
        }
    }
}


flatpickr("#installed_date", {
    dateFormat: "Y-m-d",
    maxDate : "today",
    onClose: function(selectedDates) {
        if (selectedDates[0]) {
            const startDate = new Date(selectedDates[0]);
            startDate.setDate(startDate.getDate() + 1);

            if($('#warranty_expiration_date').val() != '') {
                const warrantyDate = new Date($('#warranty_expiration_date').val());
                    const warrantyPicker =   flatpickr("#warranty_expiration_date", {
                        minDate: startDate,
                        dateFormat: "Y-m-d",
                    });
                    warrantyPicker.setDate(warrantyDate);
                // }
            }else{
                const warrantyPicker =   flatpickr("#warranty_expiration_date", {
                    minDate: startDate,
                    dateFormat: "Y-m-d",
                    disableMobile: "false"
                });
                $('#warranty_expiration_date').removeAttr('disabled')
            }
        }
    }
});