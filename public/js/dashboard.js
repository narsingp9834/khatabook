$(document).ready(function () {
    if (document.getElementById("upcoming_requests")) {
        var upcoming_requests_table = $('#upcoming_requests').DataTable({
            processing: true,
            serverSide: true,
            "bFilter": false,
            "bPaginate": false,
            "bInfo": false,
            order: [0, 'DESC'],
            pageLength : 5,
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/upcoming_requests",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery('#status').val();
                    data.type = jQuery('#type').val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type', name: 'specializations.name' },
                { data: 'slot_time', searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city' },
                { data: 'appointment_status', name: 'appointment_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            
        });

        upcoming_requests_table.on('draw', function () {
            var info = upcoming_requests_table.page.info(); // Get page information
        
            var totalRecords = info.recordsTotal; // Get the total number of records
            $('.upcoming_requests_count').html(totalRecords);
        });
    }

    if (document.getElementById("pending_requests")) {
        var pending_requests_table = $('#pending_requests').DataTable({
            processing: true,
            serverSide: true,
            "bFilter": false,
            "bPaginate": false,
            "bInfo": false,
            order: [0, 'DESC'],
            pageLength : 5,
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/pending_requests",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery('#status').val();
                    data.type = jQuery('#type').val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type', name: 'specializations.name'},
                { data: 'slot_time',searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city',name:'cities.name',orderable: true, searchable: false },
                { data: 'appointment_status',orderable: false, searchable: false},
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            drawCallback: function(dt) {
                $('.aselect2').select2({ tags: true, width: "6em" });
            },
        });

        pending_requests_table.on('draw', function () {
            var info = pending_requests_table.page.info(); // Get page information
        
            var totalRecords = info.recordsTotal; // Get the total number of records
            $('.pending_requests_count').html(totalRecords);
        });
    }

    if (document.getElementById("in_progress_requests")) {
        var in_progress_requests_table = $('#in_progress_requests').DataTable({
            processing: true,
            serverSide: true,
            // "bFilter": false,
            // "bInfo": false,
            order: [0, 'DESC'],
            pageLength : 5,
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/in_progress_requests",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery('#status').val();
                    data.type = jQuery('#type').val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type' },
                { data: 'slot_time',searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city' },
            ],
            
        });
    }
    
    

    if (document.getElementById("cancelled_requests")) {
        var cancelled_requests_table = $('#cancelled_requests').DataTable({
            processing: true,
            serverSide: true,
            // "bFilter": false,
            // "bInfo": false,
            order: [0, 'DESC'],
            pageLength : 5,
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/cancelled_requests",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery('#status').val();
                    data.type = jQuery('#type').val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type' },
                { data: 'slot_time', searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city' },
            ],
            
        });
    }

    if (document.getElementById("total_attended")) {
        var rangePicker = $("#range").flatpickr({
            mode: "range",
            dateFormat: "m-d-Y",
            defaultDate: [
                new Date(new Date().getFullYear(), new Date().getMonth(), 1), // Start of the current month
                new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0), // End of the current month
            ],
        });        
        $('#range').prop("readonly", false); 
        var total_attended_table = $('#total_attended').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/total_attended",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.range = jQuery('#range').val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type',name:'specializations.name' },
                { data: 'slot_time', searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city',name:'cities.name' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            
        });
        var defaultDateValues = rangePicker.selectedDates.map(date => rangePicker.formatDate(date, "m-d-Y"));
        if (defaultDateValues != '' && defaultDateValues != undefined) {
            jQuery('#range').val(defaultDateValues.join(' to '));
            total_attended_table.draw();
        }
        jQuery('#range').on('change', function () {
            total_attended_table.draw();
        });
    }

    // if (document.getElementById("emergency_requests")) {
    //     var emergency_requests_table = $('#emergency_requests').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         "bFilter": false,
    //         "bPaginate": false,
    //         "bInfo": false,
    //         order: [0, 'DESC'],
    //         pageLength : 5,
    //         dom:
    //             '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //         ajax: {
    //             url: base_url + "/dashboard/emergency_requests",
    //             data: function (data) {
    //                 data.timeZone = jQuery("input[name='timeZone']").val();
    //             }
    //         },
    //         "columns": [
    //             { data: 'DT_RowIndex', orderable: false, searchable: false },
    //             { data: 'customer_name', name:'customer_name'},
    //             { data: 'name',name : 'cities.name' },
    //             { data: 'appointment_status',name:'appointment_status',orderable: true, searchable: false  },
    //             { data: 'action', name: 'action', orderable: false, searchable: false },
    //         ],
            
    //     });
    // }
    // if (document.getElementById("estimate_requests")) {
    //     var emergency_requests_table = $('#estimate_requests').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         "bFilter": false,
    //         "bPaginate": false,
    //         "bInfo": false,
    //         order: [0, 'DESC'],
    //         pageLength : 5,
    //         dom:
    //             '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //         ajax: {
    //             url: base_url + "/dashboard/estimate_requests",
    //             data: function (data) {
    //                 data.timeZone = jQuery("input[name='timeZone']").val();
    //             }
    //         },
    //         "columns": [
    //             { data: 'DT_RowIndex', orderable: false, searchable: false },
    //             { data: 'customer_name', name:'customer_name'},
    //             { data: 'appointment_status',name:'appointment_status',orderable: true, searchable: false  },
    //             { data: 'action', name: 'action', orderable: false, searchable: false },
    //         ],
            
    //     });
    // }
    
    // if (document.getElementById("maintenance_requests")) {
    //     var maintenance_requests_table = $('#maintenance_requests').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         "bFilter": false,
    //         "bPaginate": false,
    //         "bInfo": false,
    //         order: [0, 'DESC'],
    //         pageLength : 5,
    //         dom:
    //             '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //         ajax: {
    //             url: base_url + "/dashboard/maintenance_requests",
    //             data: function (data) {
    //                 data.timeZone = jQuery("input[name='timeZone']").val();
    //             }
    //         },
    //         "columns": [
    //             { data: 'DT_RowIndex', orderable: false, searchable: false },
    //             { data: 'customer_name', name:'customer_name'},
    //             { data: 'name',name : 'cities.name' },
    //             { data: 'appointment_status',name:'appointment_status',orderable: true, searchable: false  },
    //             { data: 'action', name: 'action', orderable: false, searchable: false },
    //         ],
            
    //     });
    // }
    // if (document.getElementById("repair_requests")) {
    //     var repair_requests_table = $('#repair_requests').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         "bFilter": false,
    //         "bPaginate": false,
    //         "bInfo": false,
    //         order: [0, 'DESC'],
    //         pageLength : 5,
    //         dom:
    //             '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //         ajax: {
    //             url: base_url + "/dashboard/repair_requests",
    //             data: function (data) {
    //                 data.timeZone = jQuery("input[name='timeZone']").val();
    //             }
    //         },
    //         "columns": [
    //             { data: 'DT_RowIndex', orderable: false, searchable: false },
    //             { data: 'customer_name', name:'customer_name'},
    //             { data: 'appointment_status',name:'appointment_status',orderable: true, searchable: false  },
    //             { data: 'action', name: 'action', orderable: false, searchable: false },
    //         ],
            
    //     });
    // }

    if (document.getElementById("not_confirmed")) {
        var not_confirmed_requests_table = $('#not_confirmed').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/not_confirmed",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.specialization = jQuery("#specialization").val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type',name:'specializations.name',orderable: true, searchable: true  },
                { data: 'slot_time', searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city', name:'cities.name',orderable: true, searchable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            
        });

        jQuery('#specialization').on('change', function () {
            not_confirmed_requests_table.draw();
        });
    }

    if (document.getElementById("rescheduled_requests")) {
        var not_confirmed_requests_table = $('#rescheduled_requests').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/dashboard/rescheduled_requests",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'type',name:'specializations.name',orderable: true, searchable: true  },
                { data: 'slot_time',searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city', name:'cities.name' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            
        });

        jQuery('#specialization').on('change', function () {
            not_confirmed_requests_table.draw();
        });
    }
});

$(document).on('click', '.approveRequest', function(e) {
    var endpoint = base_url+'/appointment/approveRequest';
    var token = $("input[name='_token']").val();
    var message = "Are you sure you want to approve this request?";
    var id = $(this).data('id');
    $.ajax({
        url: base_url+'/appointment/checkTechnician',
        method: 'POST',
        data: {
            '_token': token,
            'id': id,
        },
        dataType: "json",
        success: function (data) {
            if(data.title == 'Error'){
                bootbox.confirm({
                    title: "Check Technician",
                    message: data.message,
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-danger'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-secondary'
                        }
                    },
                    callback: function (result) {
                        if (result == true) {
                            $('#loader').show();
                            $.ajax({
                                url: endpoint,
                                method: 'POST',
                                data: {
                                    '_token': token,
                                    'id': id,
                                },
                                dataType: "json",
                                success: function (data) {
                                    if(data.title == 'Error'){
                                        toastr.success(data.message, data.title);
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 1000);
                                    }else{
                                        toastr.success(data.message, data.title);
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 1000);
                                    }
                                }
                            })
                        }else{
                            $('#loader').hide();
                        }
                    }
                })
            }else{

                bootbox.confirm({
                    title: "Approve Request",
                    message: message,
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-danger'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-secondary'
                        }
                    },
                    callback: function (result) {
                        if (result == true) {
                            $('#loader').show();
                            $.ajax({
                                url: endpoint,
                                method: 'POST',
                                data: {
                                    '_token': token,
                                    'id': id,
                                },
                                dataType: "json",
                                success: function (data) {
                                    if(data.title == 'Error'){
                                        toastr.success(data.message, data.title);
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 1000);
                                    }else{
                                        toastr.success(data.message, data.title);
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 1000);
                                    }
                                }
                            })
                        }else{
                            $('#loader').hide();
                        }
                    }
                })
            }
        }
    })
});
