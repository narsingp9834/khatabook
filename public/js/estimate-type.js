$( document ).ready(function() {    
    // Datatable
    if (document.getElementById("table_estimate_type")) {
        var table = $('#table_estimate_type').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url : base_url + "/estimate-type/list",
                data: function ( data ) {
                    data.timeZone   = jQuery("input[name='timeZone']").val();
                    data.status     = jQuery('#status').val();
                    data.manufacturer_id = jQuery('#manufacturer_id').val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'generator_name'},
                {data: 'generator_size'},
                {data: 'cost'},
                {data: 'created_at', searchable: false},
                {data: 'estimate_type_status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        // Filter data status wise
        jQuery('#status').on('change', function(){
            table.draw();
        });
        jQuery('#manufacturer_id').on('change', function(){
            table.draw();
        });
    }
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("specialNumeric", function (value, element) {
            // Define a regular expression pattern for allowed characters
            var pattern = /^[0-9-]+$/;
            return this.optional(element) || pattern.test(value.trim());
        }, "Only numbers, and - are allowed");

        jQuery.validator.addMethod("specialNumeric2", function (value, element) {
            // Define a regular expression pattern for allowed characters
            var pattern = /^[0-9$-]+$/;
            return this.optional(element) || pattern.test(value.trim());
        }, "Only numbers, $, and - are allowed");

        jQuery.validator.addMethod('ckrequired', function (value, element, params) {
            var idname = jQuery(element).attr('id');
            console.log(idname, 'idname');
            var messageLength =  jQuery.trim ( noteEditor.getData() );
            return !params  || messageLength.length !== 0;
        }, "This field is required");

        jqForm.validate({
            ignore: [],
             rules: {
                description: {
                    ckrequired : true,
                    // maxlength: 1000,
                },
             },
             messages: {
                // description: {
                //     maxlength: '',
                // }
             },
             errorElement: 'span',
             errorPlacement: function (error, element) {
                 element.closest('.form-group').append(error);
             },
             submitHandler: function (form) {
                 $('#loader').show(); 
                 form.submit();
             }
         });
         $('#estimate_type_submit').on('click', function () {
           if ($('#jquery-val-form').valid()) {
             loader('show');
           }
         });
        
        // jqForm.validate({
        //     rules: {
        //         'generator_name': {
        //             required: true,
        //             maxlength:100,
        //             // require_field: true,
        //             nospaces: true,
        //         },
        //         'generator_size': {
        //             required: true,
        //             // max:50,
        //             require_field: true,
        //             maxlength: 50,
        //             specialNumeric: true, //- number 
        //         },
        //         'cost': {
        //             required: true,
        //             // max:50,
        //             require_field: true,
        //             maxlength: 50,
        //             specialNumeric2: true, //- $ number
        //         },
        //     },
        //     messages: {

        //     },
        //     errorElement: 'span',
        //     errorPlacement: function (error, element) {
        //         element.closest('.form-group').append(error);
        //     },
        //     submitHandler: function (form) {
        //         $('#loader').show(); 
        //         form.submit();
        //     }
        // });
    }
});