$(document).on('click', '.status', function(e) {
    var endpoint = base_url+'/common/status';
    var token = $("input[name='_token']").val();
    var message = "Are you sure you want to change the status?";
    var id = $(this).data('id');
    var type = $(this).data('type');
    var model = $(this).data('model');
    var field = $(this).data('field');
    var technician_id = $(this).data('technician_id');
    var customer_id = $(this).data('customer_id');
    bootbox.confirm({
        title: "Status",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                        'type': type,
                        'model':model,
                        'field':field,
                        'technician_id': technician_id,
                        'customer_id': customer_id,
                    },
                    dataType: "json",
                    success: function (data) {
                        if(data.title == 'Error'){
                            $('#loader').hide();
                            toastr.error(data.message, data.title);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                })
            }
        }
    });
});

$(document).on('click', '.delete', function(e) {
    var endpoint = base_url + '/common/delete';
    var token = jQuery("input[name='_token']").val();
    var message = "Are you sure you want to delete this record?";
    var id = $(this).data('id');
    var type = $(this).data('type');
    var model = $(this).data('model');
    bootbox.confirm({
        title: "Delete",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                        'type': type,
                        'model':model,
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#loader').hide();
                        if(data.title == 'Error'){
                            toastr.error(data.message, data.title);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                })
            }
        }
    });
});

// get state according to selected country id
if ($("#country_id").val() != '' && $("#country_id").val() != undefined) {
    var id = $("#country_id").val();
    
    getState(id);
}

$("#country_id").change(function () {
    var id = $("#country_id").val();
    getState(id);
});

if ($("#gcountry_id").val() != '' && $("#gcountry_id").val() != undefined) {
    var id = $("#gcountry_id").val();
    
    getGeneratorState(id);
}

$("#gcountry_id").change(function () {
    var id = $("#gcountry_id").val();
    getGeneratorState(id);
});

// console.log();
// get state according to selected country id

if ($("#state_id").val() != '' && $("#state_id").val() != undefined) {
    var id = $("#state_id").val();
    getCity(id);
}

// get city according to state id
$("#state_id").change(function () {
    var endpoint = base_url + '/common/getCityById';
    var id = $(this).val();
    getCity(id);
});

$("#gstate_id").change(function () {
    var id = $(this).val();
    getGeneratorCity(id);
});


function getState(id) {
    var endpoint = base_url + '/common/getStateById';
    var selected_state = $("#selected_state").val();
    var selectedOldState = $('#oldState').val();
    var token = jQuery("input[name='_token']").val();
    $('#state_id').html('<option value="">Select State</option>');
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'id': id,
        },
        dataType: "json",
        success: function (data) {
            var len = (data).length;
            for (var i = 0; i < len; i++) {
                var selected = '';
                if((selected_state == "" || selected_state == undefined) && data[i].state_id == 3959) {
                    selected = ' selected '; 
                }

                if ((selected_state == data[i].state_id) || (selectedOldState == data[i].state_id)) { 
                    selected = ' selected '; 
                }
                
                $('#state_id').append('<option value="' + data[i].state_id + '"' + selected + '>' + data[i].name + '</option>');
            }
            if ($("#state_id").val() != '') {
                $("#state_id").trigger('change');
            }
        }
    })
}

function getCity(id) {
    var endpoint = base_url + '/common/getCityById';
    var selected_city = $("#selected_city").val();
    var selectedOldCity = $('#oldCity').val();
    var token = jQuery("input[name='_token']").val();
    $('#city_id').html('<option value="">Select City</option>');
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'id': id,
        },
        dataType: "json",
        success: function (data) {
           
            var len = (data).length;
            // if(len > 0) {
            //     $('#gcity_id').prop('disabled',false);
            // }else{
            //     $('#gcity_id').prop('disabled',true);
            // }
            for (var i = 0; i < len; i++) {
                if ((selected_city == data[i].city_id) || (selectedOldCity == data[i].city_id)) { 
                    var selected = ' selected '; 
                } 
                else { var selected = ' '; }
                $('#city_id').append('<option value="' + data[i].city_id + '"' + selected + '>' + data[i].name + '</option>');
            }
        }
    })
}

function getGeneratorState(id) {
    var endpoint = base_url + '/common/getStateById';
    // var selected_state = $("#selected_state").val();
    var selectedOldState = $('#oldGState').val();
    var token = jQuery("input[name='_token']").val();
    $('#gstate_id').html('<option value="">Select State</option>');
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'id': id,
        },
        dataType: "json",
        success: function (data) {
            var len = (data).length;
            for (var i = 0; i < len; i++) {
                var selected = '';
                if(data[i].state_id == 3959) {
                    selected = ' selected '; 
                }

                if (selectedOldState == data[i].state_id) { 
                    selected = ' selected '; 
                }
                
                $('#gstate_id').append('<option value="' + data[i].state_id + '"' + selected + '>' + data[i].name + '</option>');
            }
            if ($("#gstate_id").val() != '') {
                $("#gstate_id").trigger('change');
            }
        }
    })
}

function getGeneratorCity(id){
    var endpoint = base_url + '/common/getCityById';
    var selectedOldCity = $('#oldGCity').val();
    var token = jQuery("input[name='_token']").val();
    $('#gcity_id').html('<option value="">Select City</option>');
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'id': id,
        },
        dataType: "json",
        success: function (data) {
            var gCityId = null;
            var addresscheckbox = $('#sameAsAbove');
            if (addresscheckbox.is(':checked') && addresscheckbox.val() === "on") {
                gCityId = $('#city_id').val();
            }
            var len = (data).length;
            for (var i = 0; i < len; i++) {
                

                if ((selectedOldCity == data[i].city_id) || (gCityId == data[i].city_id)) { 
                    var selected = 'selected'; 
                } 
                else { var selected = ' '; }
                $('#gcity_id').append('<option value="' + data[i].city_id + '"' + selected + '>' + data[i].name + '</option>');
            }
        }
    })
}

$('#email').on('change', function () {
    var endpoint = base_url + '/checkEmail';
    var id = $('#user_id').val();
    var token = jQuery("input[name='_token']").val();
    if ($(this).val() != '') {
        $.ajax({
            url: endpoint,
            method: 'POST',
            data: {
                '_token': token,
                'email': $(this).val(),
                'id': id,
            },
            dataType: "json",
            success: function (data) {
                if(data.result) {
                    $('.emailError').removeClass('d-none');  
                    $('#email-error').show();
                }else{
                    $('.emailError').addClass('d-none');  
                    $('#email-error').hide();
                }
            }
        })
    }else{
        $('.emailError').addClass('d-none');  
    }
});

function initializeFlatpickr(i) {
    var elementSelector = '#installed_date' + i;
    var expirSelector = '#warranty_expiration_date' + i;

    $(elementSelector).flatpickr({
        dateFormat: "Y-m-d",
        maxDate   : 'today'
    });
    $(elementSelector).prop("readonly", false); 

    $(expirSelector).flatpickr({
        dateFormat: "Y-m-d",
    });
    $(expirSelector).prop("readonly", false); 
}

function toggleAddgFields(enabled) {
    $('.addg').prop('disabled', !enabled);
}

$('.installed_date').prop("readonly", false); 

let installed_date_element = document.querySelector('.ginstalled_date');

if (installed_date_element != null) {
    $('.ginstalled_date').flatpickr({
        dateFormat: "Y-m-d",
        maxDate   : "today"
    });
    $('.ginstalled_date').prop("readonly", false);
}

$(document).on('click', '.cancelRequest', function(e) {
    var endpoint = base_url + '/appointment/cancelRequest';
    var token = jQuery("input[name='_token']").val();
    var message = "Are you sure you want to cancel this request?";
    var id = $(this).data('id');
    bootbox.confirm({
        title: "Cancel Request",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#loader').hide();
                        if(data.title == 'Error'){
                            toastr.error(data.message, data.title);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                })
            }
        }
    });
});

$(document).on('change', '.updateStatus', function(e) {
    var endpoint = base_url+'/appointment/updateStatus';
    var token = $("input[name='_token']").val();
    var message = "Are you sure you want to change the status?";
    var id = $(this).data('id');
    var status = $(this).val();
    var defaultValue = $(this).data('default');
    var element = $(this); 
    bootbox.confirm({
        title: "Status",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                        'status' : status
                    },
                    dataType: "json",
                    success: function (data) {
                        if(data.title == 'Error'){
                            toastr.error(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                })
            }else{
                element.val(defaultValue).select2();
            }
        }
    });
});

$(document).on('click', '.removeImg', function(e) {
    var endpoint = base_url + '/common/deleteProfileImg';
    var token = jQuery("input[name='_token']").val();
    var message = "Are you sure you want to delete image?";
    var id = $(this).data('id');
    var model = $(this).data('model');
    var field = $(this).data('field');
    bootbox.confirm({
        title: "Delete Image",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                        'model': model,
                        'field': field,
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#loader').hide();
                        if(data.title == 'Error'){
                            toastr.error(data.message, data.title);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                })
            }
        }
    });
});

function copyAddressFields() {
    $('#gstate_id').val($('#state_id').val());
    $('#gstate_id').trigger('change');
    if($('#gstate_id').val()!= '' && $("#gstate_id").val() != undefined){
      var gstate_id = $("#gstate_id").val();
        getGeneratorCity(gstate_id);
    }
    $('#gcity_id').val($('#city_id').val());
    $('#gcity_id').trigger('change');
    $('#gaddress_line_1').val($('#address_line_1').val());
    $('#gaddress_line_2').val($('#address_line_2').val());
    $('#gzip_code').val($('#zip_code').val());
    $('#gdescription').val($('#description').val());
}

function removeAddressValues(){
    $('#gstate_id').val('');
    $('#gstate_id').trigger('change');
    $('#gcity_id').val('');
    $('#gcity_id').trigger('change');
    $('#gaddress_line_1').val('');
    $('#gaddress_line_2').val('');
    $('#gzip_code').val('');
    $('#gdescription').val('');
}

$(document).on('click', '.rescheduleRequest', function(e) {
    var id = $(this).data('id');
    var endpoint = base_url + '/appointment/rescheduleRequest/'+id;
    var endpoint1 = base_url + '/appointment/rescheduleRequest';
    var token = jQuery("input[name='_token']").val();
    var message = jQuery("#rescheduleMsg").val();
    var role    = $('#role').val();
    var userLoggedIn = $('#userLoggedIn').val();
    
    if(role == 'A' || role == 'SA'){
        // Append a checkbox to the message
        message += '<br/><br/><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="smsNo"><label class="custom-control-label" for="smsNo" value="on"> Do not send SMS/Email to customer</label></div>';
    }
    
    bootbox.confirm({
        title: "Reschedule Request",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                var isSmsNoChecked = $('#smsNo').is(':checked');
                if(isSmsNoChecked != '' && isSmsNoChecked != undefined){
                    if(role === 'A' || role === 'SA'){
                        endpoint += '/'+isSmsNoChecked;
                    }
                }
                $('#loader').show();
                if(role == 'C'){
                    $.ajax({
                        url: endpoint1,
                        method: 'POST',
                        data: {
                            '_token': token,
                            'id': id,
                        },
                        dataType: "json",
                        success: function (data) {
                            $('#loader').hide();
                            if(data.title == 'Error'){
                                toastr.error(data.message, data.title);
                            }else{
                                window.location.href=endpoint;
                            }
                        }
                    })
                }else{
                    window.location.href=endpoint;
                }
                
            }
        }
    });
});

$(document).on('click', '#confirmBtn', function(e) {
    var id = $(this).data('id');
    var endpoint = base_url + '/appointment/confirmRequest';
    var token = jQuery("input[name='_token']").val();
    var message = "Are you sure you want to confirm this request?";
    bootbox.confirm({
        title: "Confirm Request",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#loader').hide();
                        if(data.title == 'Error'){
                            toastr.error(data.message, data.title);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    },
                     error: function(error) {
                        $('#loader').hide();
                        toastr.error('Something went wrong');
                    },
                })
            }
        }
    });
});

$(document).on('click', '.to_invoice', function(e) {
    var endpoint = base_url+'/invoice/makeInvoice';
    var token = $("input[name='_token']").val();
    var message = "Are you sure you want to create the invoice?";
    var id = $(this).data('id');
    bootbox.confirm({
        title: "Invoice",
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-secondary'
            }
        },
        callback: function (result) {
            if (result == true) {
                $('#loader').show();
                $.ajax({
                    url: endpoint,
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                    },
                    dataType: "json",
                    success: function (data) {
                        if(data.title == 'Error'){
                            $('#loader').hide();
                            toastr.error(data.message, data.title);
                        }else{
                            toastr.success(data.message, data.title);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                })
            }
        }
    });
});

$(document).on('click', '.seller_login', function (e ) {
    var message = "Your current session will expire, Please click below to continue.";
    var id = $(this).data('id');
    bootbox.confirm({
        title: "Login as Biller",
        message: message,
        buttons: {
            confirm: {
                label: 'Continue',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Close',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true){
                var urls='loginUser/' +id;
                window.location.href=urls;
            }
        }
    });
});

$(document).on('change', '.updateMode', function(e) {

    var endpoint = base_url+'/buyer/updateMode';
    var token = $("input[name='_token']").val();
    var id = $(this).data('id');
    var amount = $(this).data('amount');
        $.ajax({
            url: endpoint,
            method: 'POST',
            data: {
                '_token': token,
                'id': id,
                'mode' :$(this).val() 
            },
            dataType: "json",
            success: function (data) {
                if(data.title == 'Error'){
                    $('#loader').hide();
                    toastr.error(data.message, data.title);
                }else{
                    toastr.success(data.message, data.title);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            }
        })
});