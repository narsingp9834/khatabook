$('.select2').select2({
    width: '100%',
});

$('.tselect2').select2({
    tags: true
  });

// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_technician")) {
        table = $('#table_technician').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/technician/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.priority = jQuery("#priority").val();
                    data.specialization = jQuery("#specialization").val();
                    data.zone = jQuery("#zone").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'user_fname',  name: 'users.user_fname', orderable: true, searchable: true},
                { data: 'user_lname',  name: 'users.user_lname', orderable: true, searchable: true},
                { data: 'email',  name: 'users.email', orderable: true, searchable: true},
                { data: 'user_phone_no',  name: 'user_phone_no', orderable: true, searchable: false},
                { data: 'zip_code',  name: 'zip_code', orderable: true, searchable: true},
                { data: 'city',  name: 'cities.name', orderable: true, searchable: true},
                { data: 'state',  name: 'states.name', orderable: true, searchable: true},
                { data: 'user_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })

    $('#type').on('change', function () {
        table.draw();
    })
    
    $('#manufacturer').on('change', function () {
        table.draw();
    })

    $('#priority').on('change', function () {
        table.draw();
    })

    $('#specialization').on('change', function () {
        table.draw();
    })

    $('#zone').on('change', function () {
        table.draw();
    })

    // $('#serviceable_zone').on('change', function() {
    //     getServicableZipCodes(this);
    // });

    $('#user_phone_no').on('keydown', function(e){
        check_phone_format();
    });
    
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
                'user_fname': {
                    maxlength:50,
                    require_field: true,
                },
                'user_lname': {
                    maxlength:50,
                    require_field: true,
                },
                'email': {
                    required: true,
                    maxlength:50,
                    email: true,
                    validate_email: true,
                    require_field: true,
                },
                'password': {
                    minlength:8,
                    maxlength: 30,
                    required: true,
                    require_field: true,
                    strongPassword:true,
                },
                'user_phone_no': {
                    phoneNoValidation:true,
                    required: true,
                    minlength: 13
                },
            },
            messages: {
                'user_phone_no': {
                    phoneNoValidation:"Please enter valid phone no",
                    minlength :"Please enter at least 10 characters."
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

$(".specialization").change(function () {
    var text = $(this).next('label').text();
    text     = jQuery.trim( text );
    if($($(this)).is(":checked")) {

        var newDiv = '<div class="col-sm-6" id="'+text+'"><div class="form-group"><label class="form-label" for="zip_code">'+text +' Priority</label>';
        newDiv += '<select class="form-control tselect2" name="priority[]['+text+']">';

        for (var index = 1; index <= 10; index++){
            var selected = '';
            if(index == 1){
                selected = 'selected';
            }
            newDiv += '<option value="' +index + '" ' + selected + '>' + index + '</option>';
           
        }
        
        newDiv += '</select></div></div>';

        // Get the target div
        var targetDiv = document.getElementsByClassName("specDiv")[0];
        // Append the new div to the target div
        $('.specDiv').append(newDiv);
    }else{
        $('#'+text).remove();
    }
    $('.tselect2').select2();
    $('.tselect2').trigger('change');
});
function specializationPriority(){
    // $('.specialization').each(function () {
    //     var val = $(this).val(); 
    //     console.log(val);
    //     if($($(this)).is(":checked")) {
    //         var text = $(this).next('label').text();

    //         var newDiv = document.createElement("div");
    //         newDiv.className = "col-sm-6";

    //         // Set the inner HTML content
    //         newDiv.innerHTML = '<div class="form-group"><label class="form-label" for="zip_code">'+text +' Priority</label>';

    //         // Create select element
    //         var select = document.createElement("select");
    //         select.className = "form-control select2";
    //         select.setAttribute("data-minimum-results-for-search", "Infinity");
    //         select.name = "priority";
    //         select.id = "priority";
            
    //         // Add options from 1 to 10
    //         for (var i = 1; i <= 10; i++) {
    //         var option = document.createElement("option");
    //         option.value = i;
    //         option.text = i;
    //         if (i === 1) {
    //             option.selected = true;
    //         }
    //         select.appendChild(option);
    //         }   
    //         newDiv.appendChild(select);
     
    //         newDiv.innerHTML += '</div>';
        
    //         // Get the target div
    //         var targetDiv = document.getElementsByClassName("specDiv")[0];

    //         // Append the new div to the target div
    //         $('.specDiv').append(newDiv);
    //         // targetDiv.appendChild(targetDiv);
    //         $('#input_'+val).removeAttr('disabled');
    //     }else{
    //         $('#input_'+val).prop('checked', false); 
    //         $('#input_'+val).attr('disabled',true);
    //     }
    // });
}

function check_phone_format()
{
    $('#user_phone_no').on('input', function () {
        var number = $(this).val().replace(/[^\d]/g, '');

        if (number.length == 4) {
            number = number.replace(/(\d\d\d)/, "($1)");
        }
        else if (number.length == 7) {
            number = number.replace(/(\d\d\d)(\d\d\d)/, "($1)$2");
        }
        else if (number.length == 10) {
            number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
        }
        else
        {
            number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
        }
        $(this).val(number);
        $('#user_phone_no').attr({maxlength: 13});
    });
}