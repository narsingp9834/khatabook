
$( document ).ready(function() {      
    if (document.getElementById("table_invoice")) {
        var table = $('#table_invoice').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/invoice/list",
                data: function (data) {
                    data.payment_mode     = jQuery("#payment_mode").val();
                    data.range = jQuery('#range').val();
                    data.month = jQuery('#month').val();
                    data.year = jQuery('#year').val();
                    data.buyer = jQuery('#buyer').val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'date', name: 'date', searchable: false},
                { data: 'invoice_no', name: 'invoice_no', searchable: false},
                { data: 'user_fname', name: 'users.user_fname'},
                { data: 'gst_no', name: 'gst_no', searchable: false},
                { data: 'name', name: 'name', searchable: false},
                { data: 'grand_total', name: 'grand_total', searchable: false},
                { data: 'payment_mode', name: 'payment_mode', searchable: false},
                { data: 'staff', name: 'staff', searchable: false},
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

        jQuery("#payment_mode").on("change", function () {
            table.draw();
        });
    
        jQuery('#range').on('change', function () {
            table.draw();
            fetchData();
        });

        jQuery('#month').on('change', function () {
            table.draw();
            fetchData();
        });

        jQuery('#year').on('change', function () {
            table.draw();
            fetchData();
        });

        jQuery('#buyer').on('change', function () {
            table.draw();
        });
    }

    $("#hsn_id").change(function () {
        var id = $(this).val();
        var endpoint = base_url + '/invoice/gst-hsn';
        var token = jQuery("input[name='_token']").val();
        $.ajax({
            url: endpoint,
            method: 'POST',
            data: {
                '_token': token,
                'id': id,
                'buyer':$('#buyer_id').val()
            },
            dataType: "json",
            success: function (data) {
                $('#cgst').val(data.data.cgst);
                $('#sgst').val(data.data.sgst);
                $('#igst').val(data.data.igst);
            }
        })
    });

    $(".calculate").click(function () {
        calculateTotal();
    });

    $("#buyer_id").change(function () {
        checkCredit(); 
    });

    if ($("#buyer_id").val() != undefined && $("#buyer_id").val() != '')  {
        checkCredit();
    }
    
});

function checkCredit() {
    var endpoint = base_url + '/buyer/checkCredit';
    var token = jQuery("input[name='_token']").val();
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'buyer':$('#buyer_id').val()
        },
        dataType: "json",
        success: function (data) {
            console.log(data.eligible);
            if (data.eligible == false) {
                $("#payment_mode option[value='Credit']").attr("disabled", true);
            }
        }
    })
}

function calculateTotal() {
    var percentage =  Number($('#cgst').val()) + Number($('#sgst').val()) + Number($('#igst').val());
    var totalPrice = 0; 

     for (let index = 0; index < Number($('#totalRows').val()); index++) {
         
         let price = Number($('#price'+index).val());
         let quantity = Number($('#quantity'+index).val());
         let total = price * quantity;
         totalPrice+= total;
     }
     var discount = calculateDiscount(totalPrice,$("#discount").val());
    //  console.log(discount);
     totalPrice = discount;
     console.log(totalPrice);

    // var gst =  calculateGST(totalPrice, percentage);
    // $('#gst_amount').val(gst.gstAmount);
    // $('#total').val(gst.totalAmount);
    // var grand_total = calculateDiscount(totalPrice,$("#discount").val());
    // grand_total = grand_total + gst.gstAmount;

    var gst =  calculateGST(totalPrice, percentage);
    $('#gst_amount').val(gst.gstAmount);
    $('#total').val(gst.totalAmount);
    // var grand_total = calculateDiscount(totalPrice,$("#discount").val());
    var grand_total = totalPrice + gst.gstAmount;

    var advance_amount = $('#advance_amount').val();
    if (advance_amount != '' && advance_amount != undefined) {
        grand_total = grand_total - advance_amount;
    }
    $('#grand_total').val(grand_total);
}

function calculateGST(value, percentage) {
    // Convert percentage to decimal
    var percentageDecimal = percentage / 100;

    // Calculate GST amount
    var gstAmount = value * percentageDecimal;

    // Calculate total amount including GST
    var totalAmount = value + gstAmount;
   

    return {
        gstAmount: gstAmount,
        totalAmount: totalAmount
    };
}

function calculateDiscount(value, percentage) {
    // Convert percentage to decimal
    var percentageDecimal = percentage / 100;

    // Calculate discount amount
    var discountAmount = value * percentageDecimal;

    // Calculate discounted price
    var discountedPrice = value - discountAmount;

    return discountedPrice;
}

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {

        jqForm.validate({
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

var rangePicker = $("#range").flatpickr({
    mode: "range",
    dateFormat: "m-d-Y",
}); 


$("#date").flatpickr({
    defaultDate: new Date()
});
$('#date').attr('readonly',false);

$("#year").flatpickr({
    dateFormat: "Y", // Display only year in the input field
    altFormat: "Y", // Display only year in the calendar dropdown
    altInput: true, // Enable alternate input
    mode: "single",
});


function fetchData() {
    var endpoint = base_url + '/invoice/filter-data';
    var token = jQuery("input[name='_token']").val();
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'month': $('#month').val(),
            'range':$('#range').val(),
            'year' : $('#year').val()
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $('#totalInvoice').html(data.data.totalInvoices);
            $('#dacc').html(data.data.dacc);
            $('#paid').html(data.data.paid);
            $('#credit').html(data.data.credit);
            $('#bill').html(data.data.bill);
        }
    })
}