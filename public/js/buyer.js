
    
  if (document.getElementById("table_buyer")) {
    var table = $('#table_buyer').DataTable({
        processing: true,
        serverSide: true,
        order: [0, 'DESC'],
        dom:
            '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        ajax: {
            url: base_url + "/buyer",
            data: function (data) {
            }
        },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "columns": [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'user_fname', name: 'user_fname' },
            { data: 'gst_no', name: 'gst_no'},
            { data: 'user_phone_no', name: 'user_phone_no'},
            { data: 'state', name: 'state' },
            { data: 'city', name: 'city' },
            { data: 'email', name: 'email'},
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
    });
}

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {

        jqForm.validate({
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

$(document).on('click', '.viewCredit', function(e) {

    var endpoint = base_url+'/buyer/getCreditDetails';
    var token = $("input[name='_token']").val();
    var id = $(this).data('id');
        $.ajax({
            url: endpoint,
            method: 'POST',
            data: {
                '_token': token,
                'id': id,
            },
            dataType: "json",
            success: function (data) {
                if(data.title == 'Error'){
                    $('#loader').hide();
                    toastr.error(data.message, data.title);
                }else{
                    $('#creditAmount').html(data.data.getCreditAmount);
                    var emi = Math.round(Number(data.data.getCreditAmount)/3)
                    $('.emi').html(Math.round(Number(data.data.getCreditAmount)/3));
                    $('#totalEMI').html(emi * 3);
                    $('#totalPrice').html(data.data.totalValue);
                     $('#exampleModal').modal('show');
                }
            }
        })
});