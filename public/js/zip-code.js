// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_zip_code")) {
        table = $('#table_zip_code').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/zip-code/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.zone = jQuery("#zone").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'zip_code',  orderable: true, searchable: true},
                { data: 'zone_name', name: 'zones.zone_name',orderable: true, searchable: true},
                { data: 'created_at', searchable: false },
                { data: 'zip_code_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })

    $('#zone').on('change', function () {
        table.draw();
    })

    
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
                'zone_id': {
                    required: true,
                },
                'zip-code': {
                    required: true,
                    maxlength: 5,
                    require_field: true,
                },
            },
            messages: {
            },
            errorElement: "span",
            errorPlacement: function (error, element) {
                element.closest(".form-group").append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});