$('.select2').select2({
    width: '100%',
});
$( document ).ready(function() {    
    // Datatable
    if (document.getElementById("table_admin_user")) {
        var table = $('#table_admin_user').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url : base_url + "/admin-user/list",
                data: function ( data ) {
                    data.timeZone   = jQuery("input[name='timeZone']").val();
                    data.status     = jQuery('#status').val();         
                    data.role       = jQuery('#role').val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'user_fname', name: 'user_fname', orderable: true, searchable: true},
                {data: 'user_lname', name: 'user_lname',orderable: true, searchable: true},
                {data: 'email', name: 'email', orderable: true, searchable: true},
                {data: 'name', name: 'roles.name',orderable: true, searchable: true},
                {data: 'user_phone_no', orderable: true, searchable: true},
                {data: 'created_at', searchable: false},
                {data: 'user_status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        // Filter data status wise
        jQuery('#status').on('change', function(){
            table.draw();
        });

        jQuery('#role').on('change', function(){
            table.draw();
        });
    }

    $('#user_phone_no').on('keydown', function(e){
        check_phone_format();
    });
});
$(function () {
    jQuery('#user_role').on('change', function(){
        getMenus();
    });
    
    function getMenus(){
        var endpoint = base_url + '/common/get-menus';
        var selected_role = $('#user_role').val();
        var token = jQuery("input[name='_token']").val();
        $.ajax({
            url: endpoint,
            method: 'POST',
            data: {
                '_token': token,
                'id': selected_role,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                if(data.menuArray.length == 0 && data.subMenuArray.length == 0){
                    $('.assignedRole').html('');
                }else{
                    $('.assignedRole').html(data.html);
                }
                
            }
        })
    }

    if($('#user_role').val() != '') {
        getMenus();
    }

    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        
        jqForm.validate({
            rules: {
                'user_fname': {
                    required: true,
                    nospaces: true,
                    maxlength:100,
                },
                'user_lname': {
                    required: true,
                    nospaces: true,
                    maxlength:100,
                },
                'email': {
                    required: true,
                    nospaces: true,
                    maxlength:100,
                    validate_email: true,
                },
                'password': {
                    // required: true,
                    nospaces: true,
                    maxlength:30,
                    minlength:8,
                    strongPassword: true,
                },
                'user_country_code': {
                    required: true,
                },
                'user_phone_no': {
                    required: true,
                    phoneNoValidation:true,
                    minlength: 13
                }
            },
            messages: {
                'user_phone_no': {
                    phoneNoValidation:"Please enter valid phone no",
                    minlength :"Please enter at least 10 characters."
                }
            },
            errorElement: 'span',
                errorPlacement: function (error, element) {
                    element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

function check_phone_format()
{
    $('#user_phone_no').on('input', function () {
        var number = $(this).val().replace(/[^\d]/g, '');

        if (number.length == 4) {
            number = number.replace(/(\d\d\d)/, "($1)");
        }
        else if (number.length == 7) {
            number = number.replace(/(\d\d\d)(\d\d\d)/, "($1)$2");
        }
        else if (number.length == 10) {
            number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
        }
        else
        {
            number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
        }
        $(this).val(number);
        $('#user_phone_no').attr({maxlength: 13});
    });
}