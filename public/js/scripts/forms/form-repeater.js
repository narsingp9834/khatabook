/*=========================================================================================
    File Name: form-repeater.js
    Description: form repeater page specific js
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy HTML Admin Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {
  "use strict";
  if (document.getElementById("productCount")) {
    var i = $("#productCount").val() - 1;
  } else {
    var i = 0;
  }

  // form repeater jquery
  $(".add-invoice").repeater({
    ready: function () {
      
    },
    show: function () {
      ++i;
      var quantity = $(this).find(".quantity");
      if (quantity.length > 0) {
        quantity.attr("id", "quantity" + i);
      }
      var price = $(this).find(".price");
      if (price.length > 0) {
        price.attr("id", "price" + i);
      }
      $('#totalRows').val(i+1);
      $(this).slideDown();
      
      if (feather) {
        feather.replace({ width: 14, height: 14 });
      }
    },
    hide: function (deleteElement) {
      if ($(".div-repeat").length == 1) {
        bootbox.alert({
          title: "Delete",
          message: "One Product should be present. You can not delete this Transfer Switch.",
        });
      } else {
        bootbox.confirm(
          "Are you sure you want to delete this Product?",
          function (result) {
            if (result) {
              var product_id = $(".deleteProduct").data("id");
              console.log(product_id);
              var token = jQuery("input[name='_token']").val();
              if (product_id) {
                $.ajax({
                  url: base_url + "/invoice/deleteProduct",
                  method: "POST",
                  data: {
                    _token: token,
                    id: product_id,
                  },
                  dataType: "json",
                  success: function (data) {
                    calculateTotal();
                  },
                });
              }
              $(this).slideUp(deleteElement);
              --i;
            }
          }
        );
      }
    },
    isFirstItemUndeletable: false,
  });

});
