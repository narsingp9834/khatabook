// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_question")) {
        table = $('#table_question').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/question/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.service = jQuery("#service").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'question_text', orderable: true, searchable: true},
                { data: 'name', name:'specializations.name',orderable: true, searchable: true},
                { data: 'sort_order', orderable: true, searchable: false},
                // { data: 'created_at', searchable: false },
                { data: 'status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })

    $('#service').on('change', function () {
        table.draw();
    })
});


$(function () {

    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jqForm.validate({
         
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
              }
        });
    }


});
