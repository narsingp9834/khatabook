$(function () {
  var jqForm = $("#jquery-val-form");
  if (jqForm.length) {
    jQuery.validator.addMethod(
      "validate_email",
      function (value, element) {
        if (
          /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
            value
          )
        ) {
          return true;
        } else {
          return false;
        }
      },
      "Please enter valid email address."
    );
    jQuery.validator.addMethod(
      "require_field",
      function (value, element) {
        if (value.trim() == "") {
          return false;
        }
        return true;
      },
      "This field is required."
    );
    jQuery.validator.addMethod(
      "phoneNoValidation",
      function (value, element) {
        if (value == "(000)000-0000" || value == "0000000000") {
          return false;
        }
        return true;
      },
      "* Please enter valid phone no."
    );

    jqForm.validate({
      rules: {
        email: {
          email: true,
          validate_email: true,
        },
        phone: {
          phoneNoValidation: true,
          minlength : 13
        }
      },
      messages: {
        'phone': {
            phoneNoValidation:"Please enter valid phone no.",
            minlength :"Please enter at least 10 characters." 
        }
      },
      errorElement: "span",
      errorPlacement: function (error, element) {
        element.closest(".form-group").append(error);
      },
      submitHandler: function (form) {
        $("#loader").show();
        form.submit();
      },
    });
  }
});

$("#phone").on("keydown", function (e) {
    check_phone_format();
});

function check_phone_format() {
  $("#phone").on("input", function () {
    var number = $(this).val().replace(/[^\d]/g, "");

    if (number.length == 4) {
      number = number.replace(/(\d\d\d)/, "($1)");
    } else if (number.length == 7) {
      number = number.replace(/(\d\d\d)(\d\d\d)/, "($1)$2");
    } else if (number.length == 10) {
      number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
    } else {
      number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
    }
    $(this).val(number);
    $("#phone").attr({ maxlength: 13 });
  });
}

$("#jquery-val-form input:radio").change(function () {
    var label = $(this).next('label').text();
    if(label == "Email"){
        $(".type_email").show();
        $(".type_phone").hide();
    }else{
        $(".type_email").hide();
        $(".type_phone").show();
    }
});