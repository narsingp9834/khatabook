var jqForm = $('#jquery-val-form');
$(function () {
    
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        

        jqForm.validate({
            rules: {
                'question_text': {
                    required: true,
                    require_field: true,
                }
            },
            messages: {
                // Define custom error messages if needed
            },
         
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
              }
        });
    }


});
// function generateValidationRules() {
//     var validationRules = {};

//     // Iterate over each 'choice' input and add validation rule
//     jqForm.find('[name^="columns["]').each(function () {
//         validationRules[$(this).attr('name')] = {
//             required: true,
//             require_field: true
//         };
//     });
// console.log(validationRules);
//     return validationRules;
// }