
    
  if (document.getElementById("table_product")) {
    var table = $('#table_product').DataTable({
        processing: true,
        serverSide: true,
        order: [0, 'DESC'],
        dom:
            '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        ajax: {
            url: base_url + "/product",
            data: function (data) {
                data.range = jQuery('#range').val();
            }
        },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "columns": [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            {data:'date', name:'date'},
            { data: 'product_name', name: 'product_name' },
            { data: 'purchase_price', name: 'purchase_price'},
            { data: 'sale_price', name: 'sale_price'},
            { data: 'max_quantity', name: 'max_quantity'},
            { data: 'min_quantity', name: 'min_quantity'},
            { data: 'available_stock', name: 'available_stock'},
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
    });

    jQuery('#range').on('change', function () {
        table.draw();
        fetchProductData();
    });
}

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {

        jqForm.validate({
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

var rangePicker = $("#range").flatpickr({
    mode: "range",
    dateFormat: "m-d-Y",
}); 

$("#date").flatpickr({
    defaultDate: new Date()
});
$('#date').attr('readonly',false);

function fetchProductData() {
    var endpoint = base_url + '/product/fetch-data';
    var token = jQuery("input[name='_token']").val();
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'range':$('#range').val(),
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
           
            $('#totalStock').html(data.data.totalStock);
            $('#totalPaid').html(data.data.totalPaid);
        }
    })
}