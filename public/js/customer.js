$(document).ready(function () {
    initializeFlatpickr(0);
    $(".addg").attr("disabled", true);
    updateAddgFields();
  if (document.getElementById("table_customer")) {
    var table = $("#table_customer").DataTable({
      processing: true,
      serverSide: true,
      order: [0, "DESC"],
      dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
      ajax: {
        url: base_url + "/customer/list",
        data: function (data) {
          data.timeZone = jQuery("input[name='timeZone']").val();
          data.status = jQuery("#status").val();
          data.type = jQuery("#type").val();
        },
      },
      columns: [
        { data: "DT_RowIndex", orderable: false, searchable: false },
        { data: "type", name: "customers.type" },
        { data: "user_fname",name: "user_fname",orderable: true, searchable: true },
        { data: "user_lname",name: "user_lname",orderable: true, searchable: true },
        { data: "email" },
        { data: "user_phone_no" },
        { data: "user_status", orderable: false },
        { data: "action", name: "action", orderable: false, searchable: false },
      ],
    });

    // / Filter data status wise
    jQuery("#status").on("change", function () {
      table.draw();
    });

    jQuery("#type").on("change", function () {
      table.draw();
    });
  }
  $(".select2").select2({
    width: "100%",
  });

  var select2_team = $("#customer_category");

  select2_team.each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
      // the following code is used to disable x-scrollbar when click in select input and
      // take 100% width in responsive also
      dropdownAutoWidth: true,
      width: "100%",
      placeholder: "Select Category",
      dropdownParent: $this.parent(),
    });
  });

  if($("#customer_type").val() == "Residential"){
    $("#custDiv").show();
    $("#comDiv").hide();
  }else{
    $("#custDiv").hide();
    $("#comDiv").show();
  }

  $("#customer_type").change(function () {
    $("#custDiv").hide();
    $("#comDiv").hide();

    if ($(this).val() == "Residential") {
      $("#custDiv").show();
    } else {
      $("#comDiv").show();
    }
  });


  $('input[name="checkGenerator"]').on("change", function () {
    // Show loader when the checkbox is changed
    var checkbox = $(this);
    if (checkbox.is(':checked') && checkbox.val() === "on") {
      $(".addg").attr("disabled", false);
      // if($('#address_type_id').val() != ''){
      //   checkAddressType(function (result) {
      //     // Hide the loader after the checkAddressType function completes
      //     $('#loader').hide();

      //     if (result) {
      //         checkbox.prop('checked', true);

      //         if (checkbox.val() == "on") {
      //             $(".addg").attr("disabled", false);
      //         } else {
      //             $(".addg").attr("disabled", true);
      //         }
      //     } else {
      //         checkbox.prop('checked', false);
      //         $(".addg").attr("disabled", true);
      //         var addressTypeMsg = $('#addressTypeMsg').val();
      //         bootbox.alert({
      //             message: addressTypeMsg,
      //             backdrop: true
      //         });
      //     }
      // });
      // }else{
      //   bootbox.alert({
      //     message: "Please Select Address Type.",
      //     backdrop: true,
      //     callback: function () {
      //       $('#loader').hide();
      //       checkbox.prop('checked', false);
      //       $(".addg").attr("disabled", true);
      //   }
      // });
      // }
    
    }else{
      $(".addg").attr("disabled", true);
    }
  });

  $('#sameAsAbove').on("change", function () {
    // Show loader when the checkbox is changed
    var addresscheckbox = $(this);
    if (addresscheckbox.is(':checked') && addresscheckbox.val() === "on") {
        copyAddressFields();
    }else{
        removeAddressValues();
    }
  });


  });

  $("#user_phone_no").on("keydown", function (e) {
    check_phone_format();
  });

  // $("#address_type_id").on("change", function (e) {
  //   checkAddressType(function (result) {
  //       if(result) {
  //           $(this).attr('checked',true);
  //           if ($('input[name="checkGenerator"]:checked').val() == "on") {
  //               $(".addg").attr("disabled", false);
  //           } else {
  //               $(".addg").attr("disabled", true);
  //           }
  //       }else{
  //           // if($('input[name="checkGenerator"]:checked').val() === "on"){
  //               $('input[name="checkGenerator"]').prop('checked', false);
  //               $(".addg").attr("disabled", true);
  //           // }
  //       }
  //     });
  // });

  if ($('input[name="checkGenerator"]:checked').val() == "on") {
    $('#checkGenerator').trigger('change');
    $(".addg").prop("disabled", false);
  } else {
    $(".addg").prop("disabled", true);
  }


$(function () {
  var jqForm = $("#jquery-val-form");
  if (jqForm.length) {
    jQuery.validator.addMethod(
      "validate_email",
      function (value, element) {
        if (
          /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
            value
          )
        ) {
          return true;
        } else {
          return false;
        }
      },
      "Please enter valid email."
    );
    jQuery.validator.addMethod(
      "require_field",
      function (value, element) {
        if (value.trim() == "") {
          return false;
        }
        return true;
      },
      "This field is required."
    );
    jQuery.validator.addMethod(
      "phoneNoValidation",
      function (value, element) {
        if (value == "(000)000-0000" || value == "0000000000") {
          return false;
        }
        return true;
      },
      "* Please enter valid phone no."
    );
    jQuery.validator.addMethod(
      "strongPassword",
      function (value) {
        return (
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]/.test(
            value
          ) &&
          /[a-z]/.test(value) &&
          /\d/.test(value) &&
          /[A-Z]/.test(value)
        );
      },
      "The password must contain at least 1 number, 1 lower case letter,1 upper case letter and 1 special symbol."
    );

    jqForm.validate({
      rules: {
        email: {
          email: true,
          validate_email: true,
        },
        password: {
          minlength: 8,
          maxlength: 30,
          strongPassword: true,
        },
        user_phone_no: {
          phoneNoValidation: true,
          minlength : 13
        }
      },
      messages: {
        'user_phone_no': {
            phoneNoValidation:"Please enter valid phone no.",
            minlength :"Please enter at least 10 characters." 
        }
      },
      errorElement: "span",
      errorPlacement: function (error, element) {
        element.closest(".form-group").append(error);
      },
      submitHandler: function (form) {
        $("#loader").show();
        form.submit();
      },
    });
  }
});

function check_phone_format() {
  $("#user_phone_no").on("input", function () {
    var number = $(this).val().replace(/[^\d]/g, "");

    if (number.length == 4) {
      number = number.replace(/(\d\d\d)/, "($1)");
    } else if (number.length == 7) {
      number = number.replace(/(\d\d\d)(\d\d\d)/, "($1)$2");
    } else if (number.length == 10) {
      number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
    } else {
      number = number.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
    }
    $(this).val(number);
    $("#user_phone_no").attr({ maxlength: 13 });
  });
}

function checkAddressType(callback) {
  var addressTypeId = $("#address_type_id").val();
  var token = jQuery("input[name='_token']").val();
  $.ajax({
    url: base_url + "/common/checkAddressType",
    method: "POST",
    data: {
      _token: token,
      id: addressTypeId,
    },
    dataType: "json",
    success: function (result) {
        callback(result.result);
    },
  });
}

function updateAddgFields() {
    if ($('input[name="checkGenerator"]:checked').val() == "on") {
      $('#checkGenerator').trigger('change');
      $(".addg").prop("disabled", false);
    } else {
      $(".addg").prop("disabled", true);
    }
}
