
    
  if (document.getElementById("table_plan")) {
    var table = $('#table_plan').DataTable({
        processing: true,
        serverSide: true,
        order: [0, 'DESC'],
        dom:
            '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        ajax: {
            url: base_url + "/plans",
            data: function (data) {
            }
        },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "columns": [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'name', name: 'name' },
            { data: 'year', name: 'year' },
            { data: 'amount', name: 'amount' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
    });
}

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {

        jqForm.validate({
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});