// Datatable
$(document).ready(function () {
    var table;
    if (document.getElementById("table_role")) {
        table = $('#table_role').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/role/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', orderable: true, searchable: true},
                { data: 'created_at', searchable: false },
                { data: 'role_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });

    }
    $('#status').on('change', function () {
        table.draw();
    })
    
});


$(function () {

    $('.submenu').on('change', function (e) {
        if($("[name='submenu[]']:checked").length > 0){
            $('#menu\\[\\]-error').hide();
            var menuInput = $("[name='menu[]']");
                menuInput.attr('required', false);
        }else{
            var menuInput = $("[name='menu[]']");
                menuInput.attr('required', true);
        }
    });

    $('#selectAll').click(function() {
        // Get its current checked state
        var isChecked = $(this).prop('checked');
        
        // Set all the other checkboxes to the same state
        $('.submenu[name="menu[]"]').prop('checked', isChecked);
        $('.submenu[name="submenu[]"]').prop('checked', isChecked);
    });

    // When any of the other checkboxes is clicked
    $('.submenu[name="menu[]"], .submenu[name="submenu[]"]').click(function() {
        // Check if all other checkboxes are checked and update "select all" accordingly
        var allChecked = ($('.submenu[name="menu[]"]:checked').length + $('.submenu[name="submenu[]"]:checked').length) === ($('.submenu[name="menu[]"]').length + $('.submenu[name="submenu[]"]').length);
        $('#selectAll').prop('checked', allChecked);
    });    

    var allCheckedonEdit = ($('.submenu[name="menu[]"]:checked').length + $('.submenu[name="submenu[]"]:checked').length) === ($('.submenu[name="menu[]"]').length + $('.submenu[name="submenu[]"]').length);
    $('#selectAll').prop('checked', allCheckedonEdit);

    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        

        jqForm.validate({
            rules: {
                'name': {
                    required: true,
                    require_field: true,
                },
                'menu[]': {
                    required: function(element) {
                        return $('input.menu:checkbox:checked').length === 0 && $('input.submenu:checkbox:checked').length === 0;
                    }
                }
            },
            messages: {
                // Define custom error messages if needed
            },
         
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
              }
        });
    }


});
