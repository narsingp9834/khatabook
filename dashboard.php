<?php require_once ('../../components/admin_components/cssLink.php'); ?>
<style>
    .span-design {

        margin-top: -15px;


    }

    .dashboard {
        font-size: 25px;
    }

    .inner p {
        font-size: 18px;
        margin-top: -10px;
    }

    .inner h3 {
        font-size: 25px !important;
    }
</style>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php require_once ('../../components/admin_components/header.php'); ?>
        <?php require_once ('../../components/admin_components/sidebar.php'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6">

                                    <h5>Quick Get Details</h5>

                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="dashboard.php">Manage</a></li>
                                        <li class="breadcrumb-item active"><a href="dashboard.php">Quick Get Details</a>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

          
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>250000/-</h3>
                                    <p>Purchase</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>

                                </div>
                                <a href="../Purchase/manage.php" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>5</h3>
                                    <p>Staff</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>10</h3>
                                    <p>Product</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-briefcase" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>50</h3>
                                    <p>Buyer</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>500000/-</h3>
                                    <p>Invoice</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file" aria-hidden="true"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>15000/-</h3>
                                    <p>Quotation</p>
                                </div>

                                <div class="icon">
                                    <i class="fa fa-file" aria-hidden="true"></i>
                                </div>

                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>0</h3>
                                    <p>Delivery Chalan</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file" aria-hidden="true"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>500000/-</h3>
                                    <p>Sale Report</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-flag" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>25000/-</h3>
                                    <p>Loan Offer</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-credit-card" aria-hidden="true"></i>


                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>25000/-</h3>
                                    <p>GSTR - 1</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file" aria-hidden="true"></i>


                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>350000/-</h3>
                                    <p>GSTR -2</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>1750</h3>
                                    <p>GSTR - 3B</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>3</h3>
                                    <p>Premium Plan</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>


                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>2</h3>
                                    <p>Service Order</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>21</h3>
                                    <p>Create Units</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>

                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>5</h3>
                                    <p>Create HSN</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>





                    </div>
                </div>
            </section>

        </div>

    </div>
    <?php require_once ('../../components/admin_components/footer.php'); ?>
    <?php require_once ('../../components/admin_components/jssLink.php'); ?>

    <script>

    </script>



</body>

</html>