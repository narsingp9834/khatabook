<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'email'         =>  'Email',
    'password'      =>  'Password',
    'previous'      =>  '&laquo; Previous',
    'action'        =>  'Action',
    'status'        =>  'Status',
    'update'        =>  'Update',
    'submit'        =>  'Submit',
    'back'          =>  'Back',
    'created_at'    =>  'Created On',
    'modified_at'   =>  'Modified Datetime',
    'cancel'        =>  'Cancel',
    'view'          =>  'View',
    'no'            =>  'No',
    'add'           =>  'Add',
    'name'          =>  'Name',
    'description'   =>  'Description',
    'edit'          =>  'Edit',
    'type'          =>  'Type',
    'country'       =>  'Country',
    'state'         =>  'State',
    'city'          =>  'City',
    'phone'         =>  'Phone No.',
    'new-password' => 'New Password',
    'confirm-password' => 'Confirm New Password',
    'old_password' => 'Old Password',
    'address'       =>  'Address',
    'code'          => 'Code',
    'notes'         => 'Notes',
    'comments'      =>  'Comments',
    'added_by'      => 'Created By', 
    'state_list'    => 'States List',
    'profile_image' => 'Profile Image',
    'updated_by'    => 'Updated By',
    'updated_on'    => 'Updated On',

    'customer'      => [
        'list'      =>  'Customers List',
        'customer'  =>  'Customer',
        'fname'     =>  'First Name',
        'lname'     =>  'Last Name',
        'address1'  =>  'Address Line 1',
        'address2'  =>  'Address Line 2',
        'zip'       =>  'Zip Code',
        'code'      =>  'Country Code',
        'category'  =>  'Category',
        'customer_name'      =>  'Customer Name',
        'customer_email'     =>  'Customer Email',
        'company_contact_name'  =>  'Company Contact Name',
        'company_name' =>   'Company Name',
        'address_list'  =>  'Customer Addresses',
        'details'       =>  'Customer Details'
    ],

    'manufacturer'      =>  [
        'list'          =>  'Manufacturers List',
        'manufacturer'  =>  'Manufacturer',
        'type'          =>  'Type',
    ],

    'generator-type'    =>  [
        'list'          =>  'Generator Types List',
        'generator'     =>  'Generator Type',
    ],

    'fuel-type'         =>  [
        'list'          =>  'Fuel Types List',
        'fuel'          =>  'Fuel Type',
    ],

    'categories'       =>  [
        'list'         =>  'Categories List',
        'categories'   =>  'Category',
    ], 
    
    'generator'         =>  [
        'list'          =>  'Generators List',
        'generator'     =>  'Generator',
        'name'          =>  'Generator Name',
        'model'         =>  'Generator Model Number',
        'model_image'   =>  'Model Number Image',
        'serial_number' =>  'Generator Serial Number',
        'size'          =>  'Generator Size (in KW)',
        'package'       =>  'Select a Maintenance Interval',
        'image'         =>  'Generator Image',
        'detail'        =>  'Generator Details',
        'type'          =>  'Generator Type',
        'manufacturer'  =>  'Generator Manufacturer',
        'last_date_of_maintenance' =>  'Last Date of Maintenance',
        'notes'         => 'Generator Notes'
    ],

    'warning'       =>  [
        'list'          =>  'Warnings List',
        'warning'       =>  'Warning',
    ], 

    'estimate_type' =>  [
        'list'          =>  'Estimate Types List',
        'estimate'      =>  'Estimate Type',
        'size'          =>  'Estimate Size (in KW)',
        'generator_name' => 'Name',
        'generator_size' => 'Size',
        'generator_image'=> 'Image',
        'cost'           => 'Cost',
    ],

    'faq'   =>  [
        'list'          =>  'FAQs List',
        'faq'           => 'FAQ',
        'name'          => 'Title',
        'description'   =>  'Description',
    ],

    'admin-user' => [
        'list'          =>  'Users List',
        'admin_user'    =>  'User',
        'email'         =>  'Email',
        'user_type'     =>  'Type',
        'user_phone_no' =>  'Phone no.',
        'user_country_code' =>  'Country Code',
    ],

    'profile'   =>  [
        'profile'   =>  'Profile',
    ],

    'user'  =>  [
        'fname'     =>  'First Name',
        'lname'     =>  'Last Name',
        'code'      =>  'Country Code',
        'shortname' =>  'Shortname',
    ],

    'zone'    =>  [
        'list'     =>  'Zones List',
        'name'     =>  'Zone',
        'no_of_technicians'=> 'No Of Technicians'
    ],

    'zip-code'    =>  [
        'list'     =>  'Zip Codes List',
        'name'     =>  'Zip Code',
    ],

    'role'        =>  [
        'list'     =>  'Roles List',
        'name'     =>  'Role',
        'menu'     => 'Select Menu'
    ],

    'transfer_switch'        =>  [
        'list'     =>  'Transfer Switches List',
        'name'     =>  'Transfer Switch',
        'model_number' => 'Model Number',
        'serial_number' => 'Serial Number',
        'amperage' => 'Amperage',
        'date_installed'=> 'Date Installed',
        'warranty_expiration_date'=>'Warranty Expiration Date',
        'location'     =>  'Transfer Switch Location',
    ],

    'technician' => [
        'list'     =>  'Technicians List',
        'name'     =>  'Technician',
        'profile_image' => 'Profile Image',
        'serviceable_zone'=> 'Serviceable Zone',
        'serviceable_zip_codes'=> 'Serviceable Zip Codes',
        'specializations' => 'Specializations',
        'specialization_priority'=> 'Specialization Priority',
        'priority' => 'Priority',
        'full_name' => 'Technician Name',
    ],

    'leave' => [
        'list'      =>  'Blockouts List',
        'name'      =>  'Blockout',
        'reason'    =>  'Reason',
        'from_date' =>  'From Date',
        'to_date'   =>  'To Date',
        'select_slot' => 'Select Time Slots'
    ],

    'holiday' => [
        'list'              =>  'Holidays List',
        'name'              =>  'Holiday',
        'holiday_date'      =>  'Holiday Date',
    ],
    
    'address-type' => [
        'list'              =>  'Address Types List',
        'name'              =>  'Address Type',
    ],

    'service' => [
        'name' => 'Pricing',
        'list'              =>  'Pricing List',
        'holiday_applicable'=>  'Holiday Applicable',
        'weekend_applicable'=>  'Weekend Applicable',
        'after_office_work_applicable'=>  'After Office Work Applicable',
        'technician_assignment_applicable'=>  'Technician Assignment Applicable',
        'labor_cost'=> 'Hourly Labor Rate',
        'cost'=>'Service Call Fee'
    ],
  
    'setting' => [
        'office_hours'      =>  'Office Hours',
        'slot_time'=>  'Slot Time',
        'slot_date'=>  'Slot Date',
        'lead_day'=>  'Lead Day',
        'after_office_work_applicable'=>  'After Office Work Applicable',
        'technician_assignment_applicable'=>  'Technician Assignment Applicable',
        'cancellation_charge'=>'Cancellation Charge',
        'hours_before_cancel_request'=>'Hours before user can cancel the request without any charges',
        'reminder_sent_before_days'=>'A reminder should be sent before(days)'
    ],

    'dashboard' => [
        'upcoming_requests'   =>  'Upcoming Requests',
        'pending_requests'=>  'Pending Requests',
        'in_progress_requests'=>  'In-Progress Requests',
        'request_type'=> 'Request Type',
        'view_all'=> 'View All',
        'cancelled_requests'=>'Cancelled Requests',
        'total_attended'=>'Total Attended',
        'emergency_requests' => 'Emergency Requests',
        'estimate_requests' => 'Estimate Requests',
        'not_confirmed'     => 'Not Confirmed Requests',
        'rescheduled_requests'=>'Rescheduled Requests',
        'maintenance_requests'=>'Maintenance Requests',
        'repair_requests'=>'Repair Requests'
    ],
    'appointment' => [
        'list'      =>      'Appointments List',
        'schedule'  =>      'Schedule a Request',
        'warning'   =>      'Generator Warnings and Error Codes',
        'details'   =>      'Appointment Details',
        'type'      =>      'Appointment Type',
        'slot'      =>      'Slot',  
        'remark'=> 'Remark',
        'approved_by'=> 'Approved By',
        'approved_at'=> 'Approved At',
        'cancelled_by'=> 'Cancelled By',
        'cancelled_at'=> 'Cancelled At',
        'confirmed_by'=> 'Confirmed By',
        'confirmed_at'=> 'Confirmed At',
    ],
    'question' => [
        'list' => 'Questions List',
        'name' => 'Question',
        'service'=> 'Service',
        'question_text'=>'Question Text',
        'sort_order'=>'Sort Order',
    ],
    'option' => [
        'name' => 'Option'
    ],
    'country_master' => [
        'phonecode' => 'Phone Code',
        'list'      => 'Countries List',
        'shortname' => 'Short Name'
    ]
];
