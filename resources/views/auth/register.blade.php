@extends('layouts/fullLayoutMaster')

@section('title', 'Register Page')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <style>
        .auth-wrapper.auth-v1 .auth-inner {
            max-width: 800px !important;
        }

        #recaptcha_error {
            font-size: 0.857rem;
            color:#ea5455;
        }
    </style>
@endsection

@section('content')
    <div class="auth-wrapper auth-v1 px-2">
        <div class="auth-inner py-2">
            <!-- Register v1 -->
            {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> --}}
            {{-- <div class="card mb-0">
                <div class="card-body"> --}}

            <form class="mt-2 switch_repeater" id="jquery-val-form" method="POST" action="{{ url('/register-store') }}" autocomplete="off">
                @csrf
                <input type="hidden" id="oldState" value="{{old('state_id')}}">
                <input type="hidden" id="oldCity" value="{{old('city_id')}}">
                <input type="hidden" id="oldGState" value="{{old('gstate_id')}}">
                <input type="hidden" id="oldGCity" value="{{old('gcity_id')}}">
                <input type="hidden" id="addressTypeMsg" value="{{config('messages.addressType')}}">
                <div class="card">
                    <div class="card-body">
                        <a href="javascript:void(0);" class="brand-logo">
                            <img src="{{ asset('images/logo.svg') }}" width="100%" height="100px" />
                        </a>
                        {{-- <h4 class="card-title mb-1">Welcome to Weekley Electric! 👋</h4> --}}
                        <h4 class="mb-2">Please sign-up to your account</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required" for="customer_type">{{ __('labels.type') }}</label>
                                    <select class="form-control select2" data-minimum-results-for-search="Infinity" name="type" id="customer_type" required="">
                                        <option value="Residential" {{ old('type') == 'Residential' ? 'Selected' : '' }}>
                                            Residential</option>
                                        <option value="Commercial" {{ old('type') == 'Commercial' ? 'Selected' : '' }}>
                                            Commercial</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row {{ old('type') == 'Commercial' ? 'd-none' : '' }}" id="custDiv">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="user_fname">{{ __('labels.customer.fname') }}</label>
                                    <input type="text" class="form-control" id="user_fname" name="user_fname"
                                        placeholder="First Name" value="{{ old('user_fname') }}" maxlength="50"
                                        required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="user_lname">{{ __('labels.customer.lname') }}</label>
                                    <input type="text" class="form-control" id="user_lname" name="user_lname"
                                        placeholder="Last Name" value="{{ old('user_lname') }}" maxlength="50"
                                        required="">
                                </div>
                            </div>
                        </div>
                        <div class="row {{ old('type') == 'Commercial' ? '' : 'd-none' }}" id="comDiv">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="company_name">{{ __('labels.customer.company_name') }}</label>
                                    <input type="text" class="form-control" id="company_name" name="company_name"
                                        placeholder="Company Name" value="{{ old('company_name') }}" maxlength="50"
                                        required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="company_contact_name">{{ __('labels.customer.company_contact_name') }}</label>
                                    <input type="text" class="form-control" id="company_contact_name"
                                        name="company_contact_name" placeholder="Company Contact Name"
                                        value="{{ old('company_contact_name') }}" maxlength="50" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required" for="email">{{ __('labels.email') }}</label>
                                    <input type="email" id="email" name="email" class="form-control"
                                        placeholder="Email" value="{{ old('email') }}" autocomplete="new-email"
                                        maxlength="50" required>
                                </div>
                            </div>
                            <div class="col-sm-6" id="pwdDiv">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="basic-default-password">{{ __('labels.password') }}</label>
                                    <div class="input-group form-password-toggle input-group-merge">
                                        <input type="password" id="password" name="password" minlength="8"
                                            class="form-control" value="{{ old('password') }}"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            autocomplete="new-password" required />
                                        <div class="input-group-append">
                                            <div class="input-group-text cursor-pointer">
                                                <i data-feather="eye"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="country_code">{{ __('labels.customer.code') }}</label>
                                    <select class="form-control select2" data-minimum-results-for-search="Infinity" name="user_country_code" id="country_code"
                                        data-placeholder="Select" required>
                                        @if ($countries->isNotEmpty())
                                            @foreach ($countries as $country)
                                                <option value="{{ $country->phonecode }}-{{ $country->shortname }}">
                                                    {{ $country->name }}({{ $country->phonecode }})</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="user_phone_no">{{ __('labels.phone') }}</label>
                                    <input type="text" class="form-control" name="user_phone_no"
                                        id="user_phone_no" placeholder="Phone No." value="{{ old('user_phone_no') }}"
                                        onkeypress="check_phone_format()" required>
                                </div>
                            </div>
                            {{-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="form-label required" for="customer_category">{{ __('labels.customer.category') }}</label>
                                            <select class="form-control select2" name="customer_category[]" id="customer_category" data-placeholder="Select" multiple="" required="">
                                                @if ($customerCategories->isNotEmpty())
                                                    @foreach ($customerCategories as $customerCategory)
                                                        <option value="{{ $customerCategory->category_id }}">{{ $customerCategory->category_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div> --}}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class=""> Address Details</h4>
                        <div class="row">
                            {{-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="address_type_id">{{ __('labels.type') }}</label>
                                    <select class="form-control select2" name="address_type_id" id="address_type_id"
                                        required="">
                                        <option value="">Select Type</option>
                                        @if ($addressTypes->isNotEmpty())
                                            @foreach ($addressTypes as $addressType)
                                                <option value="{{ $addressType->address_type_id }}"
                                                    {{ old('address_type_id') == $addressType->address_type_id ? 'selected' : '' }}>
                                                    {{ $addressType->address_type_name}} 
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div> --}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="country_id">{{ __('labels.country') }}</label>
                                    <select class="form-control select2" data-minimum-results-for-search="Infinity" name="country_id" id="country_id" required="">
                                        @if ($countries->isNotEmpty())
                                            @foreach ($countries as $country)
                                                <option value="{{ $country->country_id }}">{{ $country->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required" for="state_id">{{ __('labels.state') }}</label>
                                    <select class="form-control select2" name="state_id" id="state_id" required="">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required" for="city_id">{{ __('labels.city') }}</label>
                                    <select class="form-control select2" name="city_id" id="city_id" required="">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="address_line_1">{{ __('labels.customer.address1') }}</label>
                                    <textarea class="form-control" name="address_line_1" id="address_line_1"
                                        placeholder="{{ __('labels.customer.address1') }}" maxlength="200" required="">{{ old('address_line_1') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="address_line_2">{{ __('labels.customer.address2') }}</label>
                                    <textarea class="form-control" name="address_line_2" id="address_line_2"
                                        placeholder="{{ __('labels.customer.address2') }}" maxlength="200">{{ old('address_line_2') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="zip_code">{{ __('labels.customer.zip') }}</label>
                                    <input type="number" class="form-control" id="zip_code" name="zip_code"
                                        placeholder="Zip Code" value="{{ old('zip_code') }}" maxlength="5"
                                        onKeyPress="if(this.value.length > 4) return false;" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="description">{{ __('labels.description') }}</label>
                                    <textarea class="form-control" name="description" id="description" placeholder="Description" maxlength="500">{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label></label>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkGenerator"
                                            name="checkGenerator"  {{(old('checkGenerator') == 'on' ? 'checked' : '')}}/>
                                        <label class="custom-control-label" for="checkGenerator" value="on">&nbsp; Add
                                            Generator <i> (if checked, then "Generator" section is mandatory.)</i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"> Generator Details</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="generator_name">{{ __('labels.generator.name') }}</label>
                                    <input type="text" class="form-control addg" id="generator_name"
                                        name="generator_name" placeholder="{{ __('labels.generator.name') }}"
                                        value="{{ old('generator_name') }}" maxlength="100" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="model_number">{{ __('labels.generator.model') }}</label>
                                    <input type="text" class="form-control addg" id="model_number"
                                        name="model_number" placeholder="{{ __('labels.generator.model') }}"
                                        value="{{ old('model_number') }}" maxlength="100" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="model_number_image">{{ __('labels.generator.model_image') }}</label>
                                    <input type="file" class="form-control addg" id="model_number_image"
                                        name="model_number_image">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="serial_number">{{ __('labels.generator.serial_number') }}</label>
                                    <input type="text" class="form-control addg" id="serial_number"
                                        name="serial_number" placeholder="{{ __('labels.generator.serial_number') }}"
                                        value="{{ old('serial_number') }}" maxlength="100" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="manufacturer_id">{{ __('labels.manufacturer.manufacturer') }}</label>
                                    <select class="form-control addg select2" id="manufacturer_id" name="manufacturer_id"
                                        required="">
                                        <option value="">Select Manufacturer</option>
                                        @if ($manufacturers->isNotEmpty())
                                            @foreach ($manufacturers as $manufacturer)
                                                <option value="{{ $manufacturer->manufacturer_id }}">
                                                    {{ $manufacturer->manufacturer_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="generator_type_id">{{ __('labels.generator-type.generator') }}</label>
                                    <select class="form-control addg select2" id="generator_type_id" name="generator_type_id"
                                        required="">
                                        <option value="">Select Generator Type</option>
                                        @if ($generatorTypes->isNotEmpty())
                                            @foreach ($generatorTypes as $generatorType)
                                                <option value="{{ $generatorType->generator_type_id }}">
                                                    {{ $generatorType->generator_type_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="generator_size">{{ __('labels.generator.size') }}</label>
                                    <input type="number" class="form-control addg" id="generator_size"
                                        name="generator_size" placeholder="{{ __('labels.generator.size') }}"
                                        value="{{ old('generator_size') }}" min="1" max="999"
                                        onKeyPress="if(this.value.length > 2) return false;" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="fuel_type_id">{{ __('labels.fuel-type.fuel') }}</label>
                                    <select class="form-control addg select2" id="fuel_type_id" name="fuel_type_id"
                                        required="">
                                        <option value="">Select Fuel Type</option>
                                        @if ($fuelTypes->isNotEmpty())
                                            @foreach ($fuelTypes as $fuelType)
                                                <option value="{{ $fuelType->fuel_type_id }}">
                                                    {{ $fuelType->fuel_type_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="generator_image">{{ __('labels.generator.image') }}</label>
                                    <input type="file" class="form-control addg" id="generator_image"
                                        name="generator_image">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label class="form-label" for="generator_installed_date">{{__("labels.transfer_switch.date_installed")}}</label>
                                    <input type="text" class="form-control ginstalled_date addg" name="generator_installed_date" id="generator_installed_date" value="{{old('generator_installed_date')}}" placeholder="{{__('labels.transfer_switch.date_installed')}}"  onkeypress="return false;" onpaste="return false;">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label required"
                                        for="maintenance_package">{{ __('labels.generator.package') }}</label><br />
                                    <label for="m6"> <input class="addg" id="m6" type="radio"
                                            name="maintenance_package" value="6" required=""> Every 6
                                        Months</label>
                                    <label for="m12"> <input class="addg" id="m12" type="radio"
                                            name="maintenance_package" value="12" required=""> Every 12
                                        Months</label><br />
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label class="form-label" for="description">{{__("labels.generator.notes")}}</label>
                                    <textarea class="form-control addg" id="notes"  name="notes" placeholder="{{__("labels.generator.notes")}}" maxlength="1000">{{old('notes')}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" style="justify-content:unset">
                        <h4 class="card-title">Generator Address Details</h4>
                        <div class="custom-control custom-checkbox ml-1">
                            <input type="checkbox" class="custom-control-input addg" id="sameAsAbove"  name="sameAsAbove" {{(old('sameAsAbove') == 'on' ? 'checked' : '')}}/>
                            <label class="custom-control-label" for="sameAsAbove" value="on" style="font-size:1rem;">&nbsp;Same As Above Address</label>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="address_type_id">{{ __('labels.type') }}<span
                                            class="text-danger">*</span></label>
                                    <select class="form-control select2 addg" name="gaddress_type_id" id="address_type_id"
                                        required="">
                                        <option value="">Select Address Type</option>
                                        @if ($addressTypes->isNotEmpty())
                                            @foreach ($addressTypes as $addressType)
                                                <option value="{{ $addressType->address_type_id }}"
                                                    @if (old('address_type_id') == $addressType->address_type_id) selected @endif>
                                                    {{ $addressType->address_type_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="country_id">{{ __('labels.country') }}<span
                                            class="text-danger">*</span></label>
                                    <select class="form-control select2 addg" data-minimum-results-for-search="Infinity" name="gcountry_id" id="gcountry_id" required="">
                                        @if ($countries->isNotEmpty())
                                            @foreach ($countries as $country)
                                                <option value="{{ $country->country_id }}">{{ $country->name }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="state_id">{{ __('labels.state') }}<span
                                            class="text-danger">*</span></label>
                                    <select class="form-control select2 addg" name="gstate_id" id="gstate_id" required="">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="city_id">{{ __('labels.city') }}<span
                                            class="text-danger">*</span></label>
                                    <select class="form-control select2 addg" name="gcity_id" id="gcity_id" required="">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="address_line_1">{{ __('labels.customer.address1') }}<span
                                            class="text-danger">*</span></label>
                                    <textarea class="form-control addg" name="gaddress_line_1" id="gaddress_line_1"
                                        placeholder="{{ __('labels.customer.address1') }}" maxlength="200" required="">{{ old('gaddress_line_1') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="address_line_2">{{ __('labels.customer.address2') }}</label>
                                    <textarea class="form-control addg" name="gaddress_line_2" id="gaddress_line_2"
                                        placeholder="{{ __('labels.customer.address2') }}" maxlength="200">{{ old('gaddress_line_2') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" for="zip_code">{{ __('labels.customer.zip') }}<span
                                            class="text-danger">*</span></label>
                                    <input type="number" class="form-control addg" id="gzip_code" name="gzip_code"
                                        placeholder="Zip Code" value="{{ old('gzip_code') }}" maxlength="5"
                                        onKeyPress="if(this.value.length > 4) return false;" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="description">{{ __('labels.description') }}</label>
                                    <textarea class="form-control addg" name="gdescription" id="gdescription" placeholder="Description" maxlength="500">{{old('gdescription')}}</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                @php
                $allInput = old();
                @endphp
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{__("labels.add")}} {{__("labels.transfer_switch.name")}} </h4>
                        <button class="btn btn-icon btn-primary addg" type="button" data-repeater-create>
                            <i data-feather="plus" class="mr-25"></i>
                            <span>Add New</span>
                        </button>
                    </div>
                    <div class="card-body">
                        <div data-repeater-list="columns">
                                    @if (isset($allInput['columns']))
                                        @foreach ($allInput['columns'] as $key => $old)
                                            <div class="div-repeat" data-repeater-item>
                                                <div class="row d-flex align-items-start">
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="generator_model">{{__("labels.transfer_switch.name")}} {{__("labels.name")}}<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control addg" id="transfer_switch_name" name="transfer_switch_name" placeholder="{{__("labels.transfer_switch.name")}} {{__("labels.name")}}" value="{{$old['transfer_switch_name']}}" maxlength="50" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="manufacturer_id">{{__("labels.manufacturer.manufacturer")}}<span class="text-danger">*</span></label>
                                                            <select class="form-control gselect2 addg" name="manufacturer_id" required="">
                                                                <option value="">Select Manufacturer</option>
                                                                @foreach ($manufacturers as $manufacturer)
                                                                <option value="{{$manufacturer->manufacturer_id }}" {{$old['manufacturer_id'] == $manufacturer->manufacturer_id ? 'selected' : ''}}>{{$manufacturer->manufacturer_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="model_number">{{__("labels.transfer_switch.model_number")}}<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control addg" id="tmodel_number" name="tmodel_number" placeholder="{{__('labels.transfer_switch.model_number')}}" value="{{$old['tmodel_number']}}" maxlength="100" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mb-50">
                                                        <div class="form-group">
                                                            <label class="form-label" for="customer_type"></label>
                                                            <button class="btn btn-outline-danger text-nowrap px-1 form-control addg" data-repeater-delete type="button"  data-page="add">
                                                                <i data-feather="x" class="mr-25 text-danger"></i>
                                                                <span class="text-danger">Delete</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="generator_model">{{__("labels.transfer_switch.serial_number")}}<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control addg" id="tserial_number" name="tserial_number" placeholder="{{__('labels.transfer_switch.serial_number')}}" value="{{$old['tserial_number']}}" maxlength="100" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="user_lname">{{__("labels.transfer_switch.amperage")}}<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control addg" id="amperage" name="amperage" placeholder="Amperage" value="{{$old['amperage']}}" maxlength="50" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="company_name">{{__("labels.type")}}<span class="text-danger">*</span></label>
                                                            <select class="form-control gselect2 addg" name="switch_type_id" required="">
                                                                <option value="">Select Type</option>
                                                                @foreach ($transferSwitchTypes as $type)
                                                                <option value="{{$type->transfer_switch_type_id }}" {{($old['switch_type_id'] == $type->transfer_switch_type_id) ? 'selected' : ''}}>{{$type->switch_type_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="company_contact_name">{{__("labels.transfer_switch.date_installed")}}</label>
                                                            <input type="text" class="form-control installed_date addg" name="installed_date" id="installed_date{{$key}}" value="{{$old['installed_date']}}" placeholder="{{__('labels.transfer_switch.date_installed')}}" onkeypress="return false;" onpaste="return false;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="warranty_expiration_date">{{__("labels.transfer_switch.warranty_expiration_date")}}</label>
                                                            <input type="text" class="form-control expiration_date" name="warranty_expiration_date" id="warranty_expiration_date{{$key}}" value="{{$old['warranty_expiration_date']}}" placeholder="{{__('labels.transfer_switch.warranty_expiration_date')}}" onkeypress="return false;" onpaste="return false;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="transfer_switch_location">{{__("labels.transfer_switch.location")}}<span class="text-danger">*</span></label>
                                                            <select class="form-control gselect2 addg" data-minimum-results-for-search="Infinity" name="transfer_switch_location" id="transfer_switch_location" required>
                                                                <option value="">Select Transfer Switch Location</option>
                                                                <option value="Inside" {{$old['transfer_switch_location'] == 'Inside' ? 'Selected' : ''}}>Inside</option>
                                                                <option value="Outside" {{$old['transfer_switch_location'] == 'Outside' ? 'Selected' : ''}}>Outside</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="basic-default-password">{{__("labels.notes")}}</label>
                                                            <textarea class="form-control addg" id="description" name="notes" placeholder="{{__('labels.notes')}}" maxlength="500">{{$old['notes']}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    @else
                                    <div class="div-repeat" data-repeater-item>
                                        <div class="row d-flex align-items-start">
                                            <div class="col-md-4 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="generator_model">{{__("labels.transfer_switch.name")}} {{__("labels.name")}}<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control addg" id="transfer_switch_name" name="transfer_switch_name" placeholder="{{__("labels.transfer_switch.name")}} {{__("labels.name")}}" value="{{old('columns[][transfer_swicth_name]')}}" maxlength="50" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="manufacturer_id">{{__("labels.manufacturer.manufacturer")}}<span class="text-danger">*</span></label>
                                                    <select class="form-control gselect2 addg" name="manufacturer_id" required="">
                                                        <option value="">Select Manufacturer</option>
                                                        @foreach ($manufacturers as $manufacturer)
                                                        <option value="{{$manufacturer->manufacturer_id }}" {{old('manufacturer_id') == $manufacturer->manufacturer_id ? 'selected' : ''}}>{{$manufacturer->manufacturer_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="model_number">{{__("labels.transfer_switch.model_number")}}<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control addg" id="tmodel_number" name="tmodel_number" placeholder="{{__('labels.transfer_switch.model_number')}}" value="{{old('model_number')}}" maxlength="100" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12 mb-50">
                                                <div class="form-group">
                                                    <label class="form-label" for="customer_type"></label>
                                                    <button class="btn btn-outline-danger text-nowrap px-1 form-control addg" data-repeater-delete type="button"  data-page="add">
                                                        <i data-feather="x" class="mr-25 text-danger"></i>
                                                        <span class="text-danger">Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="generator_model">{{__("labels.transfer_switch.serial_number")}}<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control addg" id="tserial_number" name="tserial_number" placeholder="{{__('labels.transfer_switch.serial_number')}}" value="{{old('serial_number')}}" maxlength="100" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="user_lname">{{__("labels.transfer_switch.amperage")}}<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control addg" id="amperage" name="amperage" placeholder="Amperage" value="{{old('amperage')}}" maxlength="50" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="company_name">{{__("labels.type")}}<span class="text-danger">*</span></label>
                                                    <select class="form-control gselect2 addg" name="switch_type_id" required="">
                                                        <option value="">Select Type</option>
                                                        @foreach ($transferSwitchTypes as $type)
                                                        <option value="{{$type->transfer_switch_type_id }}" {{(old('switch_type_id') == $type->transfer_switch_type_id) ? 'selected' : ''}}>{{$type->switch_type_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="company_contact_name">{{__("labels.transfer_switch.date_installed")}}</label>
                                                    <input type="text" class="form-control installed_date addg" name="installed_date" id="installed_date0" value="{{old('installed_date')}}" placeholder="{{__('labels.transfer_switch.date_installed')}}" onkeypress="return false;" onpaste="return false;">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="warranty_expiration_date">{{__("labels.transfer_switch.warranty_expiration_date")}}</label>
                                                    <input type="text" class="form-control expiration_date addg" name="warranty_expiration_date" id="warranty_expiration_date0" value="{{old('warranty_expiration_date')}}" placeholder="{{__('labels.transfer_switch.warranty_expiration_date')}}" onkeypress="return false;" onpaste="return false;">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="transfer_switch_location">{{__("labels.transfer_switch.location")}}<span class="text-danger">*</span></label>
                                                    <select class="form-control gselect2 addg" data-minimum-results-for-search="Infinity" name="transfer_switch_location" id="transfer_switch_location" required>
                                                        <option value="">Select Transfer Switch Location</option>
                                                        <option value="Inside" {{old('transfer_switch_location') == 'Inside' ? 'Selected' : ''}}>Inside</option>
                                                        <option value="Outside" {{old('transfer_switch_location') == 'Outside' ? 'Selected' : ''}}>Outside</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="basic-default-password">{{__("labels.notes")}}</label>
                                                    <textarea class="form-control addg" id="description" name="notes" placeholder="{{__('labels.notes')}}" maxlength="500">{{old('notes')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                @endif
                        </div>
                        <div class="d-flex justify-content-center">
                            <div id="g-recaptcha"></div>
                        </div>
                        <div id="recaptcha_error" class="d-flex justify-content-center mb-1"></div>
                          <input type="hidden" id="captchRes" name="recaptcha" value="" /> 
                        <div class="col-md-12 d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary text-center" tabindex="5"
                                style="width: 40%;">Sign up</button>
                        </div>
                        <div class="col-md-12">
                            <p class="text-center mt-2">
                                <span>Already have an account?</span>
                                @if (Route::has('login'))
                                    <a href="{{ route('login') }}">
                                        <span>Sign in instead</span>
                                    </a>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="{{ asset('js/register.js') }}?v={{ Config::get('constants.portal_version') }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

    <script>
        var onloadCallback = function() {
            widgetId = grecaptcha.render('g-recaptcha', {
            'sitekey' : '{{ Helper::getGSiteKey() }}',
            'callback':recaptchaCallback,
            'expired-callback': expCallback
            });
        }
        function recaptchaCallback() {
            return new Promise(function(resolve, reject) { 
                var response = grecaptcha.getResponse();
                if(response){
                    document.getElementById("captchRes").value = response;
                    $("#recaptcha_error").text('');
                    $("#recaptcha_error").removeClass('alert alert-danger');
                        resolve();
                }else{
                    reject();   
                }
                
            })
        }
        function expCallback(){
            window.grecaptcha.execute();
        }

        $("#jquery-val-form").submit(function (e) {

            var captchaResponse = grecaptcha.getResponse();

            if (!captchaResponse) {
            event.preventDefault(); // Prevent form submission
            document.getElementById('recaptcha_error').innerHTML = 'Please select captcha.';
            } else {
            document.getElementById('recaptcha_error').innerHTML = '';
            document.getElementById('captchRes').value = captchaResponse; 
                if(jQuery('#jquery-val-form').valid()){
                    $("#loader").show();
                    $("#jquery-val-form").submit();
                }
            }
        });
    </script>

    @if (old('state_id') != '')
        <script>
            $(document).ready(function() {
                var stateID = {{ old('state_id') }};
                // Initialize Select2 on the "state_id" select element
                $('#state_id').select2();
                // Set the selected option
                $('#state_id').val(stateID).trigger('change');
                getCity(stateID);
            });
        </script>
    @endif

@endsection
