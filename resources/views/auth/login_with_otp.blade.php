@extends('layouts/fullLayoutMaster')

@section('title', 'Login')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<style>
    html .content.app-content {
    padding:  0 !important;
    }
    html .content.app-content {
        padding:  0 !important;
    }
    html .content.app-content {
        padding:  0 !important;
    }
    html .content.app-content {
        padding:  0 !important;
    }
</style>
@endsection

@section('content')
<div class="auth-wrapper auth-v1 px-2">
  <div class="auth-inner py-2">
    <!-- Login v1 -->
    <div class="card mb-0">
      <div class="card-body">
        <a href="javascript:void(0);" class="brand-logo">
            <img src="{{asset('images/new_logo1.jpeg')}}" width="100%" />
          <!-- <h2 class="brand-text text-primary ml-1">Weekley Electric</h2> -->
        </a>

        <h4 class="card-title mb-1">Welcome to EKhataBook! 👋</h4>
        <p class="mb-2">Please sign-in to your account</p>

        <form class="auth-login-form mt-2" method="POST" action="{{ url('send-otp') }}" id="jquery-val-form" autocomplete="off">
          @csrf
          <div class="form-group">
            <label for="login-email" class="form-label">Phone No</label>
            <input type="text" class="form-control @error('user_phone_no') is-invalid @enderror" id="login-email" name="user_phone_no" placeholder="Phone No" value="{{ old('user_phone_no') }}" required="" maxlength="50" />
            @error('user_phone_no')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="text-center align-items-between justify-content-between">
            <button type="submit" class="btn btn-primary btn-block" tabindex="4">Get Otp</button>
          </div>
        </form>
      </div>
    </div>
    <!-- /Login v1 -->
  </div>
</div>
@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
<script>
    $(function () {
        var jqForm = $('#jquery-val-form');
        if (jqForm.length) {
            jqForm.validate({
              rules: {
                    email: {
                        required: true,
                    },
                    password: {
                        required: true,
                        nospaces: true,
                        minlength: 8,
                        maxLength: 30,
                    },
                },
                messages: {
                    email: {
                        required: "Please enter email address",
                        email: "Please enter valid email",
                        validate_email: "Please enter valid email",
                    },
                    password:{  
                        required:  "Please enter password",
                    },
                }
            });
        }
    });
</script>
@endsection