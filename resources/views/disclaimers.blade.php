<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Disclaimers|SASTA AUR SECURE HAI BOSS
        APNA DIGITAL EKHATABOOK</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    @include('common.header')



    <div class="container" style="margin-top: 6rem;">
        <h4 class="home-page-main-heading my-3"  style="color: #ff4343; padding: 5px 12px; margin-bottom: 25px;">Disclaimers</h4>
       
        
           
                <h6>1. Third Party Links</h6>
                    <p>There may be links to third-party websites, services, offers, any events or activities in our service,  
                        that are not controlled by us. We remind you that,
                         our terms of service and privacy policy doesn’t apply for third-party websites. 
                         We are not the assurance or responsibility to such websites, their information,
                          materials or products. Accessing third-parties information, or providing your
                         information to such sites are at your own risks. You agree that we are not 
                         responsible for any loss or damage of any sort in your dealings with such advertisers.</p>
               <h6> 2. Limitation of Liability</h6>
                   <p> To the fullest extent permitted by law, you assume full responsibility for and 
                    we disclaim liability to you for any indirect, consequential, exemplary, incidental, 
                    or punitive damages, including lost profits, even if we had been advised of the possibility 
                    of such damages. Ekhatabook will not be responsible for lost profits, financial data, 
                    lost data or any damages. All these things are not reasonable foreseeable.</p>
                <h6>3.Cases of exclusion from Liability</h6>
                    <p>Liability of Output Books is excluded from the following cases which may occur due to frauds or other persons
                    •	Delay or failure in performance out of our boundary
                    •	Losses
                    •	Damage
                    •	Penalties</p>
                <h6>4. Indemnification</h6>
                <p>By using this service you agree to indemnify Ekhatabook, its employer, employees,
                     supplier and its associated workers from and against any losses, damages, 
                     fines and expenses (including attorney’s fees and costs) arising out of or 
                     relating to any claims that you have used the Services in violation of another
                      party’s rights, in violation of any law, in violations of any provisions of
                       the Terms, or any other claim related to your use of the Services, except
                        where such use is authorized by Ekhatabook.</p>
                <h6>5. Governing Law and Jurisdiction</h6>
                   <p> The Terms of Service and the relationship between you and Ekhatabook shall be
                     governed by the laws of the Republic of India without regard to its conflict of
                      law provisions. You and Ekhatabook agree to submit to the personal and exclusive 
                      jurisdiction of the courts located at Ajmer, India.
                    Any questions regarding the Terms of Service should be addressed to legal@Ekhatabook.com.</p>
          
        
    </div>


@include('common.footer')

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>