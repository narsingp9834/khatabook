<!--=========================== ===============================-->
    
<html>

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Category|SASTA AUR SECURE HAI BOSS
        APNA DIGITAL EKHATABOOK</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
    .card-section{
    margin-top: 70px;
    
}
.card-section-sc{
   background-color: #fff;
   padding: 25px 25px;
   border-radius: 10px;
    
}
.card-left-sc{
    padding: 10px 25px;
    
}
.card-right-sc{
    border-radius: 25px;
    padding: 10px 25px;
}
.img-right{
    border-radius: 25px;
}
.card-section-heading{
    margin-top: 5rem;
    margin-bottom: 25px;
    border-bottom: 2px solid #1967d2;
}
.card-left-sc h6{
   padding: 5px 10px;
   border-radius: 5px;
    background-color: #1967d2;
    color: #fff;
    font-size: 14px;
    width: fit-content;
    margin-bottom: 25px;
}
.card-left-sc p{
    color: #202124;
    font-size: 15px;
}

.tab-cousres-card .span-badge {
    background-color: #ffaa46 !important;
    color: #000000 !important;
    border-radius:5px;
    
}
.heading-main a {
    color: #ff4343;
    border: 2px dotted #ff4343;
    padding: 5px 12px;
    margin-bottom: 25px;
}
</style>

<body>
    
    @include('common.header')

<div class="card-section ">
        
    <div class="container" style="margin-top: 4rem;margin-bottom: 70px;">
        <h4 class="home-page-main-heading heading-main" style="margin-top: 6rem"><a href="{{url('category/products/'.$category->category_id)}}">{{$category->name}}</a></h4>
        
            <div class="tab-content" id="pills-tabContent">
                <!-- Mobile Application-->
                <div class="tab-pane fade show active" id="mobile-application" role="tabpanel" tabindex="0">
                    <div class="row">

                    
                            @foreach ($products as $product)
                            
                                <div class="col-lg-3 col-md-3">
                                    <a href="{{url('cms-product/view/'.$product->cms_product_id)}}" class="courses-a">
                                        <div class="tab-cousres-card">
                                            <div class="tab-cousres-card-img">
                                                <img src="{{$product->image1}}" width="100%" style="height: 150px;" class="img-fluid tab-cousres-card-img-rounded">
                                            </div>
                                            <a href="{{url('cms-product/view/'.$product->cms_product_id)}}"><span class="span-badge">{{$product->name}}</span></a>
                                            <h6 class="tab-cousres-card-heading">{{ substr($product->contents, 0, 50) }}</h6>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        
                    </div>
                </div>
            </div>
        
    </div>
   
</div>

<!--=========================== ===============================-->

<!--=========================== ===============================-->

{{-- <div class="card-section ">
    

    <div class="container">
        <h3 class="card-section-heading">BEST WEB SOLUTIONS</h3>
        
        <div class="row card-section-sc">
            <div class="col-lg-6 col-md-6">
                <div class="card-left-sc">
                    <h6>Details

                    </h6>
                    <p>Maintain a full sales pipeline by continuous prospecting through the generation of custom
                        quotes
                        and invoices. Generate quotes in just a few minutes to save your valuable time</p>
                    <p>Simply convert the quotes directly into invoice and send to clients after their approval.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card-right-sc">
                    <div id="carouselExample2" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                               <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                            <div class="carousel-item">
                                 <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample2"
                            data-bs-slide="prev">
                            <span class="" aria-hidden="true"><i class="bi bi-arrow-left-circle" style="color: #1967d2;font-size: 25px;"></i></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample2"
                            data-bs-slide="next">
                            <span class="" aria-hidden="true"><i class="bi bi-arrow-right-circle" style="color: #1967d2;font-size: 25px;"></i></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div> --}}

<!--=========================== ===============================-->



@include('common.footer')

    <!---======================Sub Footer End Here =====================================-->
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>