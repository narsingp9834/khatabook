@extends('layouts/contentLayoutMaster')

@section('title', "GSTR1")

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('page-style')
    <style>
        html .content {
            margin-left: 124px 
        }
    </style>
@endsection
@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 p-2">
                <div class="card" style="padding:15px">
                    <div class="row" style="padding:0px 12px">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12  mb-1">
                            <input type="text" class="form-control bg-white" placeholder="Select Range" id="range" autocomplete="off" onkeypress="return false;">
                        </div>
    
                        <div class="col-lg-3 col-md-3 col-sm-6   mb-1 ">
                            <select id="month" class="form-control">
                                <option value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>
    
                        <div class="col-lg-3 col-md-3 col-sm-6   mb-1">
                            <input type="text" class="form-control bg-white" placeholder="Select Year" id="year" autocomplete="off" onkeypress="return false;">
                        </div>
                    </div>
                </div>
            </div>    
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-center mb-2"> Purchase - GSTR2 </h3>

                        <div class="" style="display: flex;justify-content: space-around;">
                            <p> IGST</p>
                            <p id="purchaseIgstTax">{{$purchaseIgstTax}}</p>
                        </div>
                        <div class="" style="display: flex;justify-content: space-around;">
                            <p> CGST</p>
                            <p id="purchaseCgstTax">{{$purchaseCgstTax}}</p>
                        </div>
                        <div class="" style="display: flex;justify-content: space-around;">
                            <p> SGST</p>
                            <p id="purchaseSgstTax">{{$purchaseSgstTax}}</p>
                        </div>
                        <div class="" style="display: flex;justify-content: space-around;">
                            <p>Grand Total</p>
                            <p id="totalPurchase1">{{$totalPurchase}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-center mb-2"> Sale - GSTR1</h3>

                        <div class="" style="display: flex;justify-content: space-around;">
                            <p> IGST</p>
                            <p id="sellIgstTax">{{$sellIgstTax}}</p>
                        </div>
                        <div class="" style="display: flex;justify-content: space-around;">
                            <p> CGST</p>
                            <p id="sellCgstTax">{{$sellCgstTax}}</p>
                        </div>
                        <div class="" style="display: flex;justify-content: space-around;">
                            <p> SGST</p>
                            <p id="sellSgstTax">{{$sellSgstTax}}</p>
                        </div>
                        <div class="" style="display: flex;justify-content: space-around;">
                            <p>Grand Total</p>
                            <p id="totalSell1">{{$totalSell}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-center mb-5"> GSTR 3B </h3>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <h5 class="mb-4">Particular</h5>
                                <p>Eligible ITC (Purchase)</p>
                                <p>Outword Supplier (Sale)</p>

                            </div>
                            <div class="col-lg-2 col-md-2">
                                <h5 class="mb-4"> Value</h5>
                                <p id="totalPurchase">{{$totalPurchase}}/-</p>
                                <p id="totalSell">{{$totalSell}}/-</p>

                            </div>

                            <div class="col-lg-2 col-md-2">
                                <h5 class="mb-4">CGST</h5>
                                <p class="purchaseCgstTax">{{$purchaseCgstTax}}/-</p>
                                <p class="sellCgstTax">{{$sellCgstTax}}/-</p>


                            </div>

                            <div class="col-lg-2 col-md-2">
                                <h5 class="mb-4">SGST</h5>
                                <p class="purchaseSgstTax">{{$purchaseSgstTax}}/-</p>
                                <p class="sellSgstTax">{{$sellSgstTax}}/-</p>

                            </div>

                            <div class="col-lg-2 col-md-2">
                                <h5 class="mb-4">IGST</h5>
                                <p class="purchaseIgstTax">{{$purchaseIgstTax}}/-</p>
                                <p class="sellIgstTax">{{$sellIgstTax}}/</p>

                            </div>
                            <h5 class="px-3">Eligilble ITC - Tax Liability = <span id="ITC">{{$ITC}}</span>
                            </h5>


                        </div>



                    </div>
                </div>
            </div>

        </div>
        <!-- /.card-header -->

        <!-- /.card-body -->
    </div>
</section>
    <!--/ Responsive Datatable -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/gst.js') }}?v={{Config::get('constants.portal_version')}}"></script>
    <script>
        $("#year").flatpickr({
            dateFormat: "Y", // Display only year in the input field
            altFormat: "Y", // Display only year in the calendar dropdown
            altInput: true, // Enable alternate input
            mode: "single",
        });
        
        $("#range").flatpickr({
            mode: "range",
            dateFormat: "m-d-Y",
        }); 

        $("#range").click(function () {
            renderGSTStats();
        });

        jQuery('#month').on('change', function () {
            renderGSTStats();
        });

        jQuery('#year').on('change', function () {
            renderGSTStats();
        });

        function renderGSTStats() {
            var endpoint = base_url + '/getGst3Stats';
            var token = jQuery("input[name='_token']").val();
            $.ajax({
                url: endpoint,
                method: 'POST',
                data: {
                    '_token': token,
                    'id': id,
                    'range':$('#range').val(),
                    'month':$('#month').val(),
                    'year' :$('#year').val()
                },
                dataType: "json",
                success: function (data) {
                    $('#purchaseCgstTax').html(data.data.purchaseCgstTax);
                    $('#purchaseSgstTax').html(data.data.purchaseSgstTax);
                    $('#purchaseIgstTax').html(data.data.purchaseIgstTax);
                    $('#sellCgstTax').html(data.data.sellCgstTax);
                    $('#sellSgstTax').html(data.data.sellSgstTax);
                    $('#sellIgstTax').html(data.data.sellIgstTax);
                    $('#totalPurchaseTax').html(data.data.totalPurchaseTax);
                    $('#totalSellTax').html(data.data.totalSellTax);
                    $('#totalSell').html(data.data.totalSell);
                    $('#totalPurchase').html(data.data.totalPurchase);
                    $('#ITC').html(data.data.ITC);
                    $('.purchaseCgstTax').html(data.data.purchaseCgstTax);
                    $('.purchaseSgstTax').html(data.data.purchaseSgstTax);
                    $('.purchaseIgstTax').html(data.data.purchaseIgstTax);
                    $('.sellCgstTax').html(data.data.sellCgstTax);
                    $('.sellSgstTax').html(data.data.sellSgstTax);
                    $('.sellIgstTax').html(data.data.sellIgstTax);
                    $('#totalSell1').html(data.data.totalSell);
                    $('#totalPurchase1').html(data.data.totalPurchase);
                }
            })
        }
    </script>
@endsection



