@extends('layouts/contentLayoutMaster')

@section('title', "Sale")

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body border-bottom">
                        <div class="row">

                            <div class="col-xl-2 col-md-2 col-sm-6 col-12 mt-1">
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="font-weight-bolder mb-0">{{$buyerCount}}</h4>
                                        <p class="font-small-3 mb-0 small-text">No of Recipients</p>
                                    </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xl-2 col-md-2 col-sm-6 col-12 mt-1">
                                <div class="media">
                                   
                                    <div class="media-body">
                                        <h4 class="font-weight-bolder mb-0">{{$invoiceCount}}</h4>
                                        <p class="font-small-3 mb-0 small-text">No of Invoices</p>
                                    </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-3 col-sm-6 col-12 mt-1">
                                <div class="media">
                                  
                                    <div class="media-body">
                                        <h4 class="font-weight-bolder mb-0">{{$totalValue}}</h4>
                                        <p class="font-small-3 mb-0 small-text">Total Value</p>
                                    </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xl-2 col-md-2 col-sm-6 col-12 mt-1">
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="font-weight-bolder mb-0">{{$totalTax}}</h4>
                                        <p class="font-small-3 mb-0 small-text">Total Tax Amount</p>
                                    </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xl-2 col-md-2 col-sm-6 col-12 mt-1">
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="font-weight-bolder mb-0">{{$totalWithTax}}</h4>
                                        <p class="font-small-3 mb-0 small-text">Total Value</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-1 col-md-1 col-sm-6 col-12 mt-1">
                                <div class="media">
                                    <div class="media-body">
                                        <a href="{{url('gstr1/export')}}"><button class="btn btn-success mr-1 exportCustomer">Export</button></a>
                                    </div>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_gstr1">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Bill No</th>
                                <th>Party Name</th>
                                <th>State</th>
                                <th>GSTIN</th>
                                <th>Grand Total</th>
                                <th>Total Pieces</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/gst.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection



