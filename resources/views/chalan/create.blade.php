@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('chalan/save')}}" autocomplete="off" class="add-invoice">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.add")}} Chalan</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="">Buyer Name</label>
                                        <select class="form-control" name="buyer_id" id="buyer_id" required>
                                                <option value="">Select Buyer</option>
                                                @foreach ($buyers as $buyer)
                                                    <option value="{{$buyer->buyer_id}}">{{$buyer->user_fname}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Invoice No</label>
                                        <input type="text" class="form-control" name="invoice_no" id="invoice_no">
                                    </div>
                                </div>


                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">HSN</label>
                                        <select class="form-control" name="hsn_id" id="hsn_id" required>
                                                <option value="">Select HSN</option>
                                                @foreach ($hsns as $hsn)
                                                        <option value="{{$hsn->hsn_id}}">{{$hsn->code}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">CGST</label>
                                        <input type="text" class="form-control" name="cgst" id="cgst"
                                            readonly>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">SGST</label>
                                        <input type="text" class="form-control" name="sgst"
                                        id="sgst"  readonly>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">IGST</label>
                                        <input type="text" class="form-control" name="igst" id="igst"
                                             readonly>
                                    </div>

                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Date</label>
                                        <input type="text" class="form-control bg-white" name="date" id="date" required>
                                    </div>

                                </div>

                            </div> 
                                <div class="justify-content-end align-items-end">
                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                        <i data-feather="plus" class="mr-25"></i>
                                        <span>Add New</span>
                                    </button>

                                    <button class="btn btn-icon btn-primary calculate" type="button">
                                        <span>Calculate Total</span>
                                    </button>
                                </div>      
                            <input type="hidden" id="totalRows" value="1">
                                <div data-repeater-list="columns">
                                    <div class="div-repeat" data-repeater-item>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 col-12">
                                                <div class="form-group">
                                                    <label id="for" class="">Product</label>
                                                    <select class="form-control" name="product_id" id="product_id" required>
                                                            <option value="">Select Product</option>
                                                            @foreach ($products as $product)
                                                                    <option value="{{$product->product_id}}">{{$product->product_name}}           ({{$product->available_stock}})</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-md-4 col-lg-4 col-12">
                                                <div class="form-group">
                                                    <label id="for" class="">Quantity</label>
                                                    <input type="text" class="form-control quantity" name="quantity" id="quantity0"
                                                        placeholder="Enter Quantity" required>
                                                </div>
                                            </div>


                                            <div class="col-md-4 col-lg-4 col-12">
                                                <div class="form-group">
                                                    <label id="for" class="">Price</label>
                                                    <input type="text" class="form-control price" name="price" id="price0"
                                                        placeholder="Enter Price" required>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-lg-4 col-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="customer_type"></label>
                                                    <button class="btn btn-outline-danger text-nowrap px-1 form-control" data-repeater-delete type="button"  data-page="add">
                                                        <i data-feather="x" class="mr-25 text-danger"></i>
                                                        <span class="text-danger">Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">GST Amount</label>
                                        <input type="text" class="form-control" name="gst_amount" id="gst_amount"
                                            readonly>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Total</label>
                                        <input type="text" class="form-control" name="total" id="total" readonly>
                                    </div>
                                </div>


                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Discount</label>
                                        <input type="text" class="form-control" name="discount" id="discount">
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Advance</label>
                                        <input type="text" class="form-control" name="advance_amount" id="advance_amount">
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Grand Total</label>
                                        <input type="text" class="form-control" name="grand_total" id="grand_total" readonly>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Payment Mode</label>
                                        <select class="form-control" name="payment_mode" id="payment_mode" required>
                                                <option value="">Select</option>
                                                <option value="Dacc">Dacc</option>
                                                <option value="Paid">Paid</option>
                                                <option value="Credit">Credit</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('invoice')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/invoice.js') }}?v={{Config::get('constants.portal_version')}}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection