@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <input type="hidden" id="user_type" value="{{Auth::user()->user_type}}">
                <div class="card">
                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_invoice">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Name</th>
                                <th>Phone No</th>
                                <th>Service Name</th>
                                <th>Order Id</th>
                                <th>Request Date</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->

   
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script>

$( document ).ready(function() {      
    // if (document.getElementById("table_invoice")) {
        var table = $('#table_invoice').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            columnDefs : [
                { targets: 1, visible: (document.getElementById('user_type').value == 'SA') },
                { targets: 2, visible:  (document.getElementById('user_type').value == 'Sa') },
            ], 
            ajax: {
                url: base_url + "/requests",
                data: function (data) {
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'user_fname', name: 'user_fname' },
                { data: 'user_phone_no', name: 'user_phone_no'},
                { data: 'name', name: 'name'},
                { data: 'order_id', name: 'order_id'},
                { data: 'created_at', name: 'created_at'},
                { data: 'amount', name: 'amount'},
                { data: 'Status', name: 'Status'},
            ],
        });
    // }
});

$(document).on('change', '.acceptRequest', function(e) {

    var endpoint = base_url+'/updateRequest';
    var token = $("input[name='_token']").val();
    var id = $(this).data('id');
    var status = $(this).val();
        $.ajax({
            url: endpoint,
            method: 'POST',
            data: {
                '_token': token,
                'id': id,
                'status' :$(this).val() 
            },
            dataType: "json",
            success: function (data) {
                if(data.title == 'Error'){
                    $('#loader').hide();
                    toastr.error(data.message, data.title);
                }else{
                    toastr.success(data.message, data.title);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            }
        })
});

    </script>
@endsection


