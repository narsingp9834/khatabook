@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_invoice">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Name</th>
                                <th>Phone No</th>
                                <th>Service Name</th>
                                <th>Amount</th>
                                <th>Order Id</th>
                                <th>Status</th>
                                <th>Duration</th>
                                <th>{{__("labels.action")}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form action="{{url('activate-license')}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Activate Licanse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <input type="hidden" name="user_id" id="user_id" >
                <div class="modal-body">
                    <select class="form-control" name="plan_id" id="plan_id">
                            @foreach ($plans as $plan)
                                <option value="{{$plan->plan_id}}">{{$plan->name}} ({{$plan->amount}})</option>
                            @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form> 
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script>

$( document ).ready(function() {      
    // if (document.getElementById("table_invoice")) {
        var table = $('#table_invoice').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/seller/list",
                data: function (data) {
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'user_fname', name: 'user_fname' },
                { data: 'user_phone_no', name: 'user_phone_no'},
                { data: 'service_name', name: 'service_name'},
                { data: 'amount', name: 'amount'},
                { data: 'order_id', name: 'order_id'},
                { data: 'status', name: 'status'},
                { data: 'duration', name: 'duration'},    
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });
    // }
});

$(document).on('click', '.activateLicense', function (e ) {
    var id = $(this).data('id');
    $('#user_id').val(id);
    $('#exampleModal').modal('show');
});

    </script>
@endsection


