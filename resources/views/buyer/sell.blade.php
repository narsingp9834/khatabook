@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('page-style')
<style>
    .loan-emi-calculator-sc {
        background-color: #fff;
        /* border: 1px solid #dee2e6; */
        width: 100%;
        max-width: 370px;
        margin: 0 auto;
        height: auto;
        padding: 0px 0px;
        margin-bottom: 50px;
        margin-top: 10px;
        padding-bottom:15px;

    }


    .header-heading {
        font-weight: bold;
        text-align: center;
        font-size: 19px;
        margin-bottom: 17px;
        padding-top: 15px;
    }

    .header-heading-span {
        color: #ff6f5f;
    }

    .loan-emi-calculator-body {
        border-radius: 10px;
        padding: 1px 5px;
        background-color: #fff;
        border-bottom: 1px solid #0000;
        box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 0px 1px 0px;
    }

    .monthly-emi-heading {
        text-align: center;
        color: #ff6f5f;
        font-size: 14px;
        padding-top: 10px;
    }

    .price {
        text-align: center;
        color: #ff6f5f;
        font-size: 25px;
        margin-top: -12px;
        font-weight: bold;
    }

    .loan-emi-calculator-body-sc {

        padding: 0px 5px;

    }

    .bottom {
        border-bottom: 1px solid #eeeeee;
        padding: 0px 5px;
    }

    .m-main-heading {
        color: black;
        margin-bottom: 2px;
        margin-top: 9px;
        font-size: 14px;
        padding-left:10px;

    }

    .m-sub-heading {
        font-weight: bold;
        text-align: end;
        margin-top: -20px;
        margin-bottom: 7px;
        font-size: 14px;
        padding-right: 10px;

    }

    .total-payment {
        font-weight: bold;
    }

    .total-payment-sc {
        color: #ff6f5f !important;
    }

    .bottom-dotted {
        border-bottom: 2px dotted #eeeeee;
    }
        /* Apply CSS properties to ui-widgets class */
        .ui-widgets {
            position: relative;
            display: inline-block;
            width: 10rem;
            height: 10rem;
            border-radius: 9rem;
            border: 1.2rem solid palegreen;
            box-shadow: inset 0 0 7px grey;
            border-left-color: palegreen;
            border-top-color: chartreuse;
            border-right-color: darkgreen;
            border-bottom-color: darkgreen;
            text-align: center;
            box-sizing: border-box;
        }
 
        /*  Apply css properties to the second 
            child of ui-widgets class */
        .ui-widgets:nth-child(2) {
            border-top-color: chartreuse;
            border-right-color: white;
            border-left-color: palegreen;
            border-bottom-color: white;
        }
 
        /*  Apply css properties to ui-widgets class 
            and ui-values class*/
        .ui-widgets .ui-values {
            top: 40px;
            position: absolute;
            left: 10px;
            right: 0;
            font-weight: 700;
            font-size: 2.0rem;
 
        }
 
        /*  Apply css properties to ui-widgets 
            class and ui-labels class*/
        .ui-widgets .ui-labels {
 
            left: 0;
            bottom: -16px;
            text-shadow: 0 0 4px grey;
            color: black;
            position: absolute;
            width: 100%;
            font-size: 16px;
        }
</style>
@endsection

@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Buyer Report</h4>
                    </div>

                    <div class="row mt-1 ml-1">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <input type="text" class="form-control bg-white" placeholder="Select Range" id="range" autocomplete="off" onkeypress="return false;">
                        </div>
                    </div>

                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_buyer">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Date</th>
                                <th>Invoice No</th>
                                <th>Payment Mode</th>
                                <th>{{__("labels.action")}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Loan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <section class="loan-emi-calculator-sc"> 
                    <div class="loan-emi-calculator-body">
                        <p class="monthly-emi-heading">Total Loan</p>
                        <h2 class="price" id="totalPrice">₹ 10,0000</h2>
                        <div class="bottom"></div>
                        <br>
                        <div class="row">
                            <div class="col-5">
                                {{-- <section> --}}
                                    <div class="ui-widgets">
                                        <div class="ui-values">100%</div>
                                    </div>
                                  {{-- </section> --}}
                            </div>
                            <div class="col-7">
                                <div class="d-flex pp" style="justify-content: space-between;">
                                    <p><i class="fa fa-circle font-color"></i> Total Score</p>
                                    <h6>100%</h6>
        
                                </div>
        
                                <div class="d-flex pp" style="justify-content: space-between;">
                                    <p><i class="fa fa-circle font-color-sc"></i> Credit Amount</p>
                                    <h6 id="creditAmount">18000</h6>
        
                                </div>
        
                            </div>
                        </div>
                    </div>
    
                    <div class="bottom"></div>
        
                    <div class="loan-emi-calculator-body-sc">
                        
                        <p class="m-main-heading" id="date1">15-04-2024</p>
                        <h6 class="m-sub-heading emi">₹6500 </h6>
                    </div>
                    <div class="bottom"></div>
        
                    <div class="loan-emi-calculator-body-sc">
                        <p class="m-main-heading semi_date" id="date2">10-03-2024</p>
                        <h6 class="m-sub-heading emi">₹6500 </h6>
                    </div>
                    <div class="bottom"></div>
                    <div class="loan-emi-calculator-body-sc">
                        <p class="m-main-heading temi_date" id="date3">25-03-2024</p>
                        <h6 class="m-sub-heading emi">₹7000 </h6>
                    </div>
        
                   
                    <div class="bottom-dotted"></div>
        
                    <div class="loan-emi-calculator-body-sc">
                        <p class="m-main-heading total-payment">Total Payment</p>
                        <h6 class="m-sub-heading total-payment-sc" id="totalEMI">₹20000/- </h6>
                    </div>
                </section>
            </div>
        </div>
        </div>
    </div>
    
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script>

        if (document.getElementById("table_buyer")) {
            var table = $('#table_buyer').DataTable({
                processing: true,
                serverSide: true,
                order: [0, 'DESC'],
                dom:
                    '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                ajax: {
                    url: base_url + "/buyer/bills",
                    data: function (data) {
                        data.range = jQuery('#range').val();
                    }
                },
                "columnDefs": [ {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                } ],
                "columns": [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'date', name: 'date' },
                    { data: 'invoice_no', name: 'invoice_no'},
                    { data: 'payment_mode', name: 'payment_mode' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ],
            });

            jQuery('#range').on('change', function () {
                table.draw();
            });
        }

        var rangePicker = $("#range").flatpickr({
            mode: "range",
            dateFormat: "m-d-Y",
        }); 
        $(document).on('click', '.viewCredit', function(e) {

            var endpoint = base_url+'/buyer/getCreditDetails';
            var token = $("input[name='_token']").val();
            var id = $(this).data('id');
            var amount = $(this).data('amount');

                $('#creditAmount').html(amount);
                $('#totalPrice').html(amount);
                var emi = Math.round(Number(amount)/3)
                $('.emi').html(Math.round(Number(amount)/3));
                $('#totalEMI').html(emi * 3);

                const startDate = new Date('2024-03-31'); // Use any start date
                const nextThreeDates = getNextThreeDatesWithGap($(this).data('date'));
                console.log(nextThreeDates);

                // Iterate through the date array to assign values to custom elements
                nextThreeDates.forEach((date, index) => {
                        $('#date'+(index+1)).html(date);
                });


                $('#exampleModal').modal('show');
        });

            function getNextThreeDatesWithGap(startDate) {
                const dates = [];
                // Convert the input string to a Date object
                const currentDate = new Date(startDate);
                
                // Loop to calculate the next 3 dates with a 15-day gap
                for (let i = 0; i < 3; i++) {
                    // Add 15 days to the current date
                    currentDate.setDate(currentDate.getDate() + 15);
                    // Push the calculated date to the dates array
                    dates.push(formatDate(currentDate));
                }
                
                return dates;
            }

            function formatDate(date) {
                // Convert date to d-m-Y format
                const day = date.getDate();
                const month = date.getMonth() + 1;
                const year = date.getFullYear();
                return `${day}-${month < 10 ? '0' + month : month}-${year}`;
            }
    </script>
@endsection


