@extends('layouts/contentLayoutMaster')

@section('title', 'Purchase - GSTR2')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
<style>
    .card .card-header {
        justify-content: space-around !important;
    }
</style>
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Purchase List</h4>
                        <div class="dt-action-buttons text-center">
                            <b>Total Invoice : <span id="totalInvoice">{{$totalInvoices}}</span> &nbsp;
                                Total Paid : <span id="totalPaid">{{$totalPaid[0]->paid ?? '0'}} </span>&nbsp;
                                Total Unpaid : <span id="totalUnPaid">{{$totalUnPaid[0]->unpaid ?? '0'}}</span>&nbsp;
                                Total Purchase : <span id="totalPurchade">{{$totalPurchade[0]->total ?? '0'}}</span>&nbsp;
                            </b>
                        </div>
                        <div class="dt-action-buttons text-right mt-1">
                            <div class="dt-buttons d-inline-flex">
                                <a href="{{url('supplier/export')}}"><button class="btn btn-success mr-1 exportCustomer">Export</button></a>
                                @if ($supplier == null)
                                    <a href="{{url('supplier/create')}}"><button class="btn btn-success mr-1 exportCustomer">Add Supplier</button></a>
                                @endif
                            </div>
                        </div>
                    </div>
                  
                    <div class="mx-0 row mt-1">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <select class="form-control select2" id="payment_mode">
                                <option value="">Select Mode</option>
                                <option value="Pending">Pending</option>
                                <option value="Paid">Paid</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <input type="text" class="form-control bg-white" placeholder="Select Range" id="range" autocomplete="off" onkeypress="return false;">
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <select id="month" class="form-control">
                                <option value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <input type="text" class="form-control bg-white" placeholder="Select Year" id="year" autocomplete="off" onkeypress="return false;">
                        </div>
                    </div>

                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_supplier">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>{{__("labels.name")}}</th>
                                <th>Phone No</th>
                                <th>Invoice No</th>
                                <th>HSN</th>
                                <th>Total</th>
                                <th>Payment Mode</th>
                                <th>State</th>
                                <th>{{__("labels.action")}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/supplier.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection


