@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <input type="hidden" id="user_type" value="{{Auth::user()->user_type}}">
                <div class="card">
                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_invoice">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Name</th>
                                <th>Phone No</th>
                                <th>Method</th>
                                <th>City</th>
                                <th>Message</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->

   
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script>

$( document ).ready(function() {      
    // if (document.getElementById("table_invoice")) {
        var table = $('#table_invoice').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/enquiry",
                data: function (data) {
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'phone_no', name: 'phone_no' },
                { data: 'method', name: 'method'},
                { data: 'city', name: 'city'},
                { data: 'message', name: 'message'},
                { data: 'created_at', name: 'created_at'},
            ],
        });
    // }
});
    </script>
@endsection


