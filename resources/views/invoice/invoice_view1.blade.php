<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<style>
    body {
        padding: 0;
        margin: 0;
        font-family: "Poppins", sans-serif;
    }

    .poppins-light {
        font-family: "Poppins", sans-serif;
        font-weight: 300;
        font-style: normal;
    }

    .poppins-regular {
        font-family: "Poppins", sans-serif;
        font-weight: 400;
        font-style: normal;
    }

    .poppins-medium {
        font-family: "Poppins", sans-serif;
        font-weight: 500;
        font-style: normal;
    }

    .poppins-semibold {
        font-family: "Poppins", sans-serif;
        font-weight: 600;
        font-style: normal;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Poppins", sans-serif;
        font-weight: 500;
        font-style: normal;
    }

    .invoice-section {
        border-top: 5px solid #6495ED;
        width: 100%;
        max-width: 1000px;
        margin: 2rem auto;
        border-radius: 10px;
        padding: 20px 25px;


    }

    .firm-heading {
        font-size: 17px;
        font-weight: bold;
    }

    .firm-sub-heading {
        font-size: 14px;
        margin-bottom: 4px;
    }

    .gst-no {
        font-size: 14px;
        font-weight: bold;
    }

    .invoice-right {
        text-align: end;

    }

    .invoice-left {
        margin-bottom: 40px;
    }

    .invoice-section-two {
        border: 1px solid #6495ED;
        padding: 15px 25px 75px 25px;
        border-radius: 10px;
        background-color: #6495ed14;
    }

    .heading {
        font-size: 14px;
    }

    .bill-name {
        margin-top: 10px;
        font-weight: bold;
        font-size: 18px;
    }

    .bill-address {
        font-size: 14px;
        margin-bottom: 5px;
    }

    .bill-mobile {
        font-size: 14px;
        font-weight: bold;
    }

    .biller-gst {
        font-size: 14px;
    }

    .invoice-section-two {
        margin-bottom: 40px;

    }

    .invoice-section-two-right {
        text-align: end;
    }

    .total-price {
        margin-top: 10px;
        font-size: 14px;
    }

    .status {
        font-size: 13px;
        font-weight: bold;
        color: #6ab76a;
    }

    .total-price-sc {
        font-weight: bold;
    }

    .invoice-table {
        border: 1px solid #ebebeb;
        border-radius: 10px;
        padding: 20px 25px 70px 25px;
        font-size: 13px;
    }

    .p-sc {
        text-align: end;
        margin-top: 25px;
        margin-bottom: 5px;
    }
    .e-section{
        text-align: end;
    }
    .sign{
        text-align: end;
        margin-top: 25px;
        font-size: 14px;
    }
</style>
</head>

<body>


    <!--================= Invoice Start Here ===================-->
    <div class="container">
        <div class="invoice-section">
            <div class="row invoice-left">

                <div class="col-7 col-7">
                    <h6 class="firm-heading">{{$data['user']->user_fname}}</h6>
                    <p class="firm-sub-heading">{{$data['user']->getSeller->address}} <br> {{$data['user']->getSeller->getCity->name}} {{$data['user']->getSeller->getState->name}}
                    </p>
                    <p class="firm-sub-heading">Phone: {{$data['user']->user_phone_no}}</p>
                    <h5 class="gst-no">GSTIN: {{$data['user']->getSeller->gst_no}}</h5>
                </div>

                <div class="col-5 col-5">
                    <div class="invoice-right">
                        <h6 class="firm-heading">InvoiceNo. {{$data['invoice']->invoice_no}} </h6>
                        <p class="firm-sub-heading">Due Date: {{Helper::getDate($data['invoice']->created_at)}} | <span>Invoice Date: {{Helper::getDate($data['invoice']->created_at)}}</span>
                        </p>
                    </div>
                </div>

            </div>

            <div class="invoice-section-two">

                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <span class="heading">Bill and Ship To </span>
                        <div class="invoice-section-two-left">
                            <h6 class="bill-name">{{$data['buyer']->getUser->user_fname}}</h6>
                            <p class="bill-address">{{$data['buyer']->address}} {{$data['buyer']->getCity->name}} {{$data['buyer']->getState->name}}</p>
                            <h6 class="bill-mobile">{{$data['buyer']->getUser->user_phone_no}}</h6>
                            <p class="biller-gst">GSTIN: {{$data['buyer']->gst_no}}</p>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="invoice-section-two-right">
                            <img src="../../Project-1/paid.png" width="70px" class="img-fluid">
                            <h6 class="total-price">{{$data['buyer']->getUser->user_phone_no}}</h6>
                            {{-- <h3 class="total-price-sc">₹25,000</h3> --}}
                            <span class="status">Invoice : {{ $data['invoice']->payment_mode}}</span>

                        </div>
                    </div>

                </div>
            </div>

            <div class="invoice-table">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Item Details</th>
                            <th scope="col">Price/Unit</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Total Rate</th>
                            <th scope="col">Tax Rate(%)</th>
                            <th scope="col">Tax Amount</th>
                            <th scope="col">Grand Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalProdPrice = 0;	
                        @endphp
                        @foreach ($data['invoice']->products as $key => $product )
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$product->getProduct->product_name}}</td>
                                <td>{{$product->price}}/{{$product->getProduct->unit->name}}</td>
                                <td>{{$product->quantity}}</td>
                                <td>{{$product->price}} * {{$product->quantity}}</td>
                                <td>{{$data['invoice']->cgst + $data['invoice']->sgst + $data['invoice']->igst}}</td>
                                @php
                                    $gstAMount = Helper::getTaxAmount($product->quantity * $product->price,$data['invoice']->cgst + $data['invoice']->sgst + $data['invoice']->igst);
                                @endphp
                                <td>{{$gstAMount}}</td>
                                <td>{{$gstAMount + ($product->price * $product->quantity)}}</td>

                                @php
                                    $totalProdPrice += ($product->quantity * $product->price) + $gstAMount;
                                @endphp
                            </tr>
                        @endforeach

                        {{-- <tr>
                            <td>1</td>
                            <td>Shirt</td>
                            <td>250/PCS</td>
                            <td>100</td>
                            <td>25,000</td>
                            <td>25,000</td>


                        </tr>

                        <tr>
                            <td>1</td>
                            <td>Shirt</td>
                            <td>250/PCS</td>
                            <td>100</td>
                            <td>25,000</td>
                            <td>25,000</td>


                        </tr> --}}


                    </tbody>
                </table>
                <div class="e-section">
                    <h6 class="heading p-sc">Total Amount </h6>
                    <h3 class="total-price-sc p-sc">{{$totalProdPrice}}</h3>
                    <p class="status">Invoice : {{ $data['invoice']->payment_mode}}</p>
                </div>
            </div>

            <p class="sign">AUTHORISED SIGNATURE</p>
        </div>
    </div>


    <!--================= Invoice End Here ===================-->



</body>

</html>