@extends('layouts/contentLayoutMaster')

@section('title', 'Invoice - GSTR1')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Invoice List</h4>
                        @if (Auth::user()->user_type == 'B')
                            <div class="dt-action-buttons text-center">
                                <b>Invoice : <span id="totalInvoice">{{$totalInvoices}}</span> &nbsp;
                                    DACC : <span id="dacc">{{$dacc[0]->dacc ?? '0'}} </span>&nbsp;
                                    Paid : <span id="paid">{{$paid[0]->paid ?? '0'}}</span>&nbsp;
                                    Credit : <span id="credit">{{$credit[0]->credit ?? '0'}}</span>&nbsp;
                                    Bill :<span id="bill">{{$dacc[0]->dacc + $paid[0]->paid + $credit[0]->credit ?? '0'}}</span>&nbsp;
                                </b>
                            </div>
                        @endif
                            <div class="dt-action-buttons text-right">
                                <div class="dt-buttons d-inline-flex">
                                    <a href="{{url('invoice/export')}}"><button class="btn btn-success mr-1 ">Export</button></a>
                                    @if ($sale == null)
                                    <a href="{{url('invoice/create')}}"><button class="btn btn-success mr-1">Add Invoice</button></a>
                                    @endif
                                </div>
                            </div>
                        {{-- @endif --}}
                    </div>

                    <div class="row mt-1" style="padding:0px 12px">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <select class="form-control select2" id="payment_mode">
                                <option value="">Select Mode</option>
                                <option value="Dacc">Dacc</option>
                                <option value="Paid">Paid</option>
                                <option value="Credit">Credit</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <input type="text" class="form-control bg-white" placeholder="Select Range" id="range" autocomplete="off" onkeypress="return false;">
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <select id="month" class="form-control">
                                <option value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-1">
                            <select class="form-control" id="buyer">
                                    <option value="">Select Buyer</option>
                                    @foreach ($buyers as $buyer)
                                        <option value="{{$buyer->buyer_id}}">{{$buyer->user_fname}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_invoice">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Date</th>
                                <th>invoice no</th>
                                <th>Buyer Name</th>
                                <th>GSTIN</th>
                                <th>State</th>
                                <th>Total</th>
                                <th>P Mode</th>
                                <th>Staff</th>
                                <th>{{__("labels.action")}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/invoice.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection


