 <!--=========================== Footer Start Here ===============================-->
 <section class="footer-sc">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="img-section">
                        <h4 class="footer-link-heading"><img src="{{asset('assets/images/logo.jpeg')}}" class="img-fluid"
                                width="180px"></h4>
                        <div class="footer-logo">

                        </div>
                        <p class="footer-logo-disclaimer">
                            EKHATABOOK mission is to develop easy and friedly An entrepreneur Solution.
                          "Every day is a chance to begin again. Don't focus on the failures of yesterday, start today with positive thoughts and expectations." 

                        </p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="link-section">
                        <h5 class="footer-link-heading">Quick Links</h5>
                        <ul>
                            <li><a href="{{url('login')}}">Staff Login </a></li>
                            <li><a href="{{url('login')}}">Buyer login</a></li>
                            <li><a href="{{url('contact-us')}}">Contact Us</a></li>
                            <li><a href="{{url('contact-us')}}">Careers</a></li>
                             <li><a href="{{url('contact-us')}}">Subcription</a></li>
                          
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="link-section">
                        <h5 class="footer-link-heading">Entrepreneur Solutions</h5>
                        <ul>
                            <li><a href="#"> Invoice Software</a></li>
                            <li><a href="#"> E-Commerce</a></li>
                            <li><a href="#"> Staff Tracker</a></li>
                            <li><a href="#"> Sales Tracker</a></li>
                            <li><a href="#"> Data Management</a></li>
                             <li><a href="#"> Legal Management</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="link-section">
                        <h5 class="footer-link-heading">Helpful Resources </h5>
                        <ul>
                             <li><a href="{{url('/t_c')}}"> Terms & Conditions</a></li>
                            <li><a href="{{url('/privacy_policy')}}"> Privacy Policy  </a></li>
                            <li><a href="{{url('/t_c')}}"> Terms of Use </a></li>
                            <li><a href="{{url('/disclaimers')}}"> Disclaimers</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </footer>
</section>
<!---======================Footer End Here =====================================-->


<!---======================Sub Footer Start Here =====================================-->
<section class="sub-footer-sc">
    <p> 2024 - 2025 E-KHATABOOK . All Right Reserved.</p>
</section>
<!---======================Sub Footer End Here =====================================-->