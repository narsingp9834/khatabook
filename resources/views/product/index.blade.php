@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Product List</h4>
                        <div class="dt-action-buttons text-center">
                            <b>Total Stock : <span id="totalStock">{{$totalStock ?? '0'}}</span> &nbsp;
                                Total Bill : <span id="totalPaid">{{$totalBill ?? '0'}} </span>&nbsp;
                            </b>
                        </div>
                        <div class="dt-action-buttons text-right">
                            <div class="dt-buttons d-inline-flex">
                                <a href="{{url('product/create')}}"><button class="btn btn-success mr-1 exportCustomer">Add Product</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1 " style="padding:0px 12px">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <input type="text" class="form-control bg-white" placeholder="Select Range" id="range" autocomplete="off" onkeypress="return false;">
                        </div>
                    </div>

                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_product">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Date</th>
                                <th>{{__("labels.name")}}</th>
                                <th>Purchase Price</th>
                                <th>Sale Price</th>
                                <th>Maximum Quantity</th>
                                <th>Minimum Quantity</th>
                                <th>Remaining</th>
                                <th>{{__("labels.action")}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/product.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection


