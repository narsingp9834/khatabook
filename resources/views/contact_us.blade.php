<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contact us|SASTA AUR SECURE HAI BOSS
        APNA DIGITAL EKHATABOOK</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="assets/css/custom-style.css">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">

    <style>
    .form-card-contact-us{
        background-color: #fff;
        width: 100%;
        max-width:500px;
        padding: 40px 45px;
        border-radius: 10px;
        margin: 25px auto;
        box-shadow: 0 1px 3px #11182717;
        transition: all .5s ease;
    }

.contact-us-left-sc{
    background-color: #fff;
    width: 100%;
    max-width:500px;
    padding: 40px 45px;
    border-radius: 10px;
    margin: 25px auto;
    box-shadow: 0 1px 3px #11182717;
    transition: all .5s ease;
   
    margin-bottom: 15px;
    
}
.contact-us-content{
    margin-bottom: 40px;
}
.contact-us-content h5{
    color: #ff4343;
    font-family: 'Poppins', sans-serif;
    border-bottom: 2px dotted #ff4343;
    width: fit-content;
}
.contact-us-content p{
    color: #202124;
    font-size: 15px;
}
.form-icon{
    position: absolute;
    left: 23px;
    top: 43px;
    cursor: pointer;
    
    color: #dee2e6;
}

.btn-sign {
    background-color: #ff4343 !important;
    color: #fff;
    transition: all .5s ease;
    margin-top: 10px;
    padding: 6px 30px;
    border-radius: 10px;
}
.btn-sign:hover {
    background-color: #ff4343 !important;
   color:#fff;
}

.form-main-heading-span{
    color: #ff4343;
    font-family: 'Poppins', sans-serif;
    border-bottom: 2px dotted #ff4343;
}

.custom-btn {
    background-color: #ff4343 !important;
    border-radius: 5px;
    padding: 8px 20px;
    font-size: 15px;
    color: #ffffff;
    margin-right: 5px;
}
    </style>
</head>

<body>

    @include('common.header')



    <!--=========================== Contact Us Start Here  ===============================-->
    <div class="container" style="margin-top: 5rem;">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="contact-us-left-sc">

                    <div class="contact-us-content">
                        <h5 ><i class="fa fa-phone" ></i> Call Us</h5>
                        <p>+91 - 9079219596</p>
                    </div>

                    <div class="contact-us-content">
                        <h5 ><i class="fa fa-envelope"></i> Mail Us</h5>
                        <p>support@ekhatabook.com</p>
                    </div>

                    <div class="contact-us-content">
                        <h5 ><i class="fa fa-map-marker"></i> Address</h5>
                        <p>Building No.7 Adasrh Nagar Ajmer Rajasthan</p>
                    </div>


                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="contact-us-right-sc">
                    <div class=" form-card-contact-us">
                        <h4 class="form-main-heading">Contact <span class="form-main-heading-span">Us</span></h4>
                        <p style="font-size: 15px;
                        color: #202124;margin-top: -6px;">Drop Us a Message in Kahtabook</p>

                        <!-- Form Section -->
                        <form action="{{url('submit-contact-us')}}" method="POST">
                            @csrf
                            <div class="row">

                                <div class="form-group form-icon-main mb-3 col-lg-6 col-md-6">
                                    <label class="form-label form-custom-label">Full Name</label>
                                    <input type="text" class="form-control form-control-custom" id="name" name="name"
                                        placeholder="Full Name" required>
                                    <i class="fa fa-user-o form-icon"></i>
                                </div>

                                <div class="form-group form-icon-main mb-3 col-lg-6 col-md-6 ">
                                    <label class="form-label form-custom-label">Select Method</label>
                                    <select class="form-select form-control-custom "
                                        aria-label="Default select example" name="method" required>
                                        <option value="">Select</option>
                                        <option value="Invoice Software">Invoice Software</option>
                                        <option value="Best Web Solutions">Best Web Solutions</option>
                                        <option value="Web Learning">Web Learning</option>
                                        <option value="Tax Blog">Tax Blog</option>
                                        <option value="Complaint">Complaint</option>
                                    </select>
                                    <i class="fa fa-hand-pointer-o form-icon"></i>

                                </div>

                                <div class="form-group form-icon-main mb-3 col-lg-6 col-md-6">
                                    <label class="form-label form-custom-label">City</label>
                                    <input type="text" class="form-control form-control-custom" id="" name="city"
                                        placeholder="City" required>
                                    <i class="fa fa-building-o form-icon"></i>
                                </div>

                                <div class="form-group form-icon-main mb-3 col-lg-6 col-md-6">
                                    <label class="form-label form-custom-label">Phone No</label>
                                    <input type="text" class="form-control form-control-custom" id="" name="phone_no"
                                        placeholder="Phone No" required>
                                    <i class="fa fa-phone form-icon"></i>
                                </div>

                                <div class="form-group form-icon-main mb-3">
                                    <label class="form-label form-custom-label">Message</label>
                                    <textarea class="form-control form-control-custom" placeholder="Message"
                                        style="height: 100px" required name="message"></textarea>
                                    

                                </div>



                                <button type="submit" class="btn btn-sign " style="width: 100%;">Submit</button>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>



    <!--=========================== Contact Us End Here ===============================-->







    @include('common.footer')


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>