@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('cms-product/update')}}" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" value="{{$product->cms_product_id}}">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.edit")}} Product</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="">Category</label>
                                        <select id="category_id" name="category_id" class="form-control" required>
                                            <option value="">Select</option>
                                            @foreach ($categories as $category)
                                                <option value="{{$category->category_id}}" {{$product->category_id == $category->category_id ? 'selected' : ''}}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="">Name</label>
                                        <input type="text" name="name" id="name" class="form-control"  required value="{{$product->name}}"> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="">Contents</label>
                                        <textarea class="form-control" name="contents" id="contents" required> {{$product->contents}}</textarea>
                                    </div>
                                </div>

                                @for ($i = 1; $i <= 4; $i++)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="">Image{{$i}}</label>
                                            <input type="file" name="image{{$i}}" id="image{{$i}}" class="form-control" accept="image/png,image/gif,image/jpeg,image/jpg,image/svg+xml">
                                            @if(isset($product) && $product->{"image$i"})
                                                <img src="{{ $product->{"image$i"} }}" style="width:100px; height:100px;">
                                            @endif
                                        </div>
                                    </div>
                                @endfor
 

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('cms-product')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/plan.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection