<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('supplier_id');
            $table->bigInteger('seller_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('date')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('product')->nullable();
            $table->string('hsn_code')->nullable();
            $table->string('price')->nullable();
            $table->string('quantity')->nullable();
            $table->string('igst')->nullable();
            $table->string('cgst')->nullable();
            $table->string('sgst')->nullable();
            $table->string('total')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('state_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
