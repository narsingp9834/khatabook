<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('invoice_id');
            $table->bigInteger('seller_id')->nullable();
            $table->bigInteger('buyer_id')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('invoice_suffix')->nullable();
            $table->bigInteger('hsn_id')->nullable();
            $table->bigInteger('igst')->nullable();
            $table->bigInteger('cgst')->nullable();
            $table->bigInteger('sgst')->nullable();
            $table->bigInteger('discount')->nullable();
            $table->bigInteger('gst_amount')->nullable();
            $table->bigInteger('total')->nullable();
            $table->bigInteger('grand_total')->nullable();
            $table->enum('invoice_type',['Invoice', 'Quotation','Chalan'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
