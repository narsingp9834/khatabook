<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHSNSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h_s_n_s', function (Blueprint $table) {
            $table->bigIncrements('hsn_id');
            $table->bigInteger('seller_id')->nullable();
            $table->string('code')->nullable();
            $table->string('percentage')->nullable();
            $table->enum('status',['Active', 'Inactive'])->default('Active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h_s_n_s');
    }
}
