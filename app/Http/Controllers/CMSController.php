<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CMSProduct;
use Illuminate\Http\Request;
use App\Traits\CommonTrait;
use DataTables;

class CMSController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Category";
    }

    public function productIndex(Request $request) {

        try {
            if ($request->ajax()) {
                $data = CMSProduct::query();
            
                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->cms_product_id);
                    
                    $btn = "
                    <a href='".url('/cms-product/edit/'.$id)."' class='item-edit text-dark' title='Edit Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='CMSProduct' title='Delete Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->format('d-m-Y');
                })
                ->editColumn('category_id', function ($row){
                    return Category::where('category_id',$row->category_id)->value('name');
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = $this->title;
            return view('cms-product.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('category');
        }
    }

    public function productCreate() {
        $title = $this->title;
        $categories = Category::all();
        return view('cms-product.create',compact('title','categories'));
    }

    public function productStore(Request $request) {
        try {
            $input = $request->all();

            // Loop through each uploaded image
            for ($i = 1; $i <= 4; $i++) {
                if ($request->hasFile('image' . $i)) {
                    $imageName = time() . '_' . $i . '.' . $request->file('image' . $i)->extension();

                    // Move the uploaded image to the public/images directory
                    $request->file('image' . $i)->move(public_path('images'), $imageName);

                    // Store the image path in the $imageArray
                    $imagePath = env('APP_URL') . '/public/images/' . $imageName;
                    $input['image'.$i] = $imagePath;
                }
            }
            CMSProduct::create($input);
            toastr()->success("Product Created Successfully");
            return redirect('cms-product');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('cms-product/create');
        }
    }

    public function productEdit($planId) {
        try {
            $title = $this->title;
            $product = CMSProduct::find(decrypt($planId));
            $categories = Category::all();
            return view('cms-product.edit',compact('title','product','categories'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('cms-product');
        }
    }

    public function productUpdate(Request $request) {
        try {
            $input = $request->all();
            $searchInput['cms_product_id'] = $input['product_id'];

            $imageArray = [];

            for ($i = 1; $i <= 4; $i++) {
                if ($request->hasFile('image' . $i)) {
                    $imageName = time() . '_' . $i . '.' . $request->file('image' . $i)->extension();

                    // Move the uploaded image to the public/images directory
                    $request->file('image' . $i)->move(public_path('images'), $imageName);

                    // Store the image path in the $imageArray
                    $imagePath = env('APP_URL') . '/public/images/' . $imageName;
                    $input['image'.$i] = $imagePath;
                }
            }

            CMSProduct::updateOrcreate($searchInput, $input);
            toastr()->success("Product Updated Successfully");
            return redirect('cms-product');

        } catch (\Exception $e) {
            dd($e);
            toastr()->error(Config('messages.500'));
            return redirect('cms-product');
        }
    }

    
    public function productView($id) {
        try {
            $title = $this->title;
            $product = CMSProduct::find($id);
            $category = Category::where('category_id',$product->category_id)->first();
            return view('cms-product.view',compact('title','product','category'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('cms-product');
        }
    }
}
