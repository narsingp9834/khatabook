<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Product;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Product";
        $this->middleware('auth');
    }

    public function index(Request $request) {

        try {
            $sellerId = $this->getSellerId();
            if ($request->ajax()) {
                $data = Product::join('units','units.unit_id','products.unit_id')->where('products.seller_id',$sellerId);
                
                if($request->range != '') {

					if (strpos($request->range, " to ") !== false) {

						list($startDate, $endDate) = explode(" to ", $request->range);

						$data = $data->where(function($query) use ($startDate, $endDate) {
							$query->whereRaw("DATE_FORMAT(STR_TO_DATE(products.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
								  ->whereRaw("DATE_FORMAT(STR_TO_DATE(products.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
						});						
							
					} else {
						$data = $data->whereRaw("DATE_FORMAT(STR_TO_DATE(products.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
					}
				}                                            

                return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    $id = encrypt($row->product_id);
                    if ($row->status == 'Active') {
                        $status = "<button title='Active' data-id='$id' data-type='Inactive' data-model='Product' data-field='status' class='btn btn-success status'>Active</button>";
                    } else {
                        $status = "<button title='Inactive' data-id='$id' data-type='Active' data-model='Product' data-field='status' class='btn btn-danger status'>Inactive</button>";
                    }
                    return $status;
                })
                ->editColumn('created_at', function ($row) {
                    return $row->created_at->format('d-m-Y');;
                })
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->product_id);
                    
                    $btn = "
                    <a href='".url('/product/edit/'.$id)."' class='item-edit text-dark' title='Edit Product'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Product' title='Delete Product'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = $this->title;

            $totalStock = Product::select(DB::raw("SUM(products.max_quantity * products.purchase_price) as totalStock"))->where('seller_id',$sellerId)->get();
            $totalStock = $totalStock[0]->totalStock; 

            $dacc = Invoice::select(DB::raw("SUM(invoices.grand_total) as dacc"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Dacc')
            ->get();

            $paid = Invoice::select(DB::raw("SUM(invoices.grand_total) as paid"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Paid')
            ->get();


            $credit = Invoice::select(DB::raw("SUM(invoices.grand_total) as credit"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Credit')
            ->get();

            $totalBill = $dacc[0]->dacc + $paid[0]->paid + $credit[0]->credit;

            return view('product.index',compact('title','totalStock','totalBill'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('product/create');
        }
    }

    public function create() {
        $title = $this->title;

        $sellerId = $this->getSellerId();
        $hsns = $this->getHSNS($sellerId);
        $units = $this->getUnits($sellerId);
        return view('product.create',compact('title','units','hsns'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
    
            $input['seller_id'] = $this->getSellerId();
            $input['max_quantity'] = (int)$input['max_quantity'];
            $input['min_quantity'] = (int)$input['min_quantity'];
            $input['purchase_price'] = (int)$input['purchase_price'];
            $input['sale_price'] = (int)$input['sale_price'];
            $input['available_stock'] = $input['max_quantity'];
            Product::create($input);
            toastr()->success("Product Created Successfully");
            return redirect('product');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('product/create');
        }
    }

    public function edit($productId) {
        try {
            $title = $this->title;
            $product = Product::find(decrypt($productId));
            $sellerId = $this->getSellerId();
            $hsns = $this->getHSNS($sellerId);
            $units = $this->getUnits($sellerId);
            return view('product.edit',compact('title','product','units','hsns'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('product');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['product_id'] = $input['product_id'];
            $input['max_quantity'] = (int)$input['max_quantity'];
            $input['min_quantity'] = (int)$input['min_quantity'];
            $input['purchase_price'] = (int)$input['purchase_price'];
            $input['sale_price'] = (int)$input['sale_price'];
            $input['available_stock'] = $input['max_quantity'];
            Product::updateOrcreate($searchInput, $input);
            toastr()->success("Product Updated Successfully");
            return redirect('product');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('product');
        }
    }

    public function fetchData(Request $request) {
        try{

            $sellerId = $this->getSellerId();

            $totalStock = Product::select(DB::raw("SUM(products.max_quantity * products.purchase_price) as totalStock"))->where('seller_id',$sellerId);

            $totalInvoices = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice');
            // ->count();

            $dacc = Invoice::select(DB::raw("SUM(invoices.grand_total) as dacc"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Dacc');
            // ->get();

            $paid = Invoice::select(DB::raw("SUM(invoices.grand_total) as paid"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Paid');


            $credit = Invoice::select(DB::raw("SUM(invoices.grand_total) as credit"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Credit');


            if ($request->range != '') {
                if (strpos($request->range, " to ") !== false) {
                    list($startDate, $endDate) = explode(" to ", $request->range);
                    // Parse dates with specific format
                    $startDate = date_create_from_format('m-d-Y', $startDate)->format('Y-m-d');
                    $endDate = date_create_from_format('m-d-Y', $endDate)->format('Y-m-d');
                    
                    $totalStock->where(function($query) use ($startDate, $endDate) {
                        $query->where('products.date', '>=', $startDate)
                              ->where('products.date', '<=', $endDate);
                    });
            
                    $dacc = $dacc->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });
            
                    $paid = $paid->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });
            
                    $credit = $credit->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });
                } else {
                    $totalStock->where('products.date', '=', date('Y-m-d', strtotime($request->range)));
                    $dacc = $dacc->where('invoices.date', '=', date('Y-m-d', strtotime($request->range)));
                    $paid = $paid->where('invoices.date', '=', date('Y-m-d', strtotime($request->range)));
                    $credit = $credit->where('invoices.date', '=', date('Y-m-d', strtotime($request->range)));
                }
            }
            
        
               // $totalStock = $totalStock->get();
            // $totalStock = $totalStock[0]->totalStock;
            $totalStockResult = $totalStock->first(); // or ->get() if you expect multiple results

        // Access the totalStock value
            $totalStock = $totalStockResult->totalStock;
            $dacc     = $dacc->get();
            $dacc     = $dacc[0]->dacc ?? '0';
            $paid   = $paid->get();
            $paid   = $paid[0]->paid ?? '0';
            $credit   = $credit->get();
            $credit   = $credit[0]->credit ?? '0';

            $totalPaid = $dacc + $paid + $credit;

            $data = [
                'totalStock' => $totalStock ?? '0',
                'totalPaid'=>$totalPaid,
            ];

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Deleted Successfully';
            $resultArr['data'] = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }
}

