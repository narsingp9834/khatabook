<?php

namespace App\Http\Controllers;

use App\Models\Buyer;
use App\Models\HSN;
use App\Models\Invoice;
use App\Models\InvoiceProduct;
use App\Models\Product;
use App\Models\Staff;
use Illuminate\Http\Request;
use App\Traits\CommonTrait;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Pdf;
use Excel;
use App\Models\Seller;
use App\Models\User;
use App\Exports\InvoiceExport;

class InvoiceController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Invoice";
        $this->middleware('auth');
    }

    public function index(Request $request, $sale=null) {

        try {
            $sellerId = $this->getSellerId();
            if ($request->ajax()) {
                $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
                ->join('users','users.id','buyers.user_id')  
                ->join('states','states.state_id','buyers.state_id')
                ->where('invoices.seller_id',$sellerId)
                ->where('invoice_type','Invoice');
                
                if ($request->payment_mode != '') {
                    $data = $data->where('payment_mode', $request->payment_mode);
                }
                
                if($request->range != '') {

					if (strpos($request->range, " to ") !== false) {

						list($startDate, $endDate) = explode(" to ", $request->range);

						$data = $data->where(function($query) use ($startDate, $endDate) {
							$query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
								  ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
						});						
							
					} else {
						$data = $data->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
					}
				}

                if($request->month != '') {
                    $monthMap = [
                        'January' => '01',
                        'February' => '02',
                        'March' => '03',
                        'April' => '04',
                        'May' => '05',
                        'June' => '06',
                        'July' => '07',
                        'August' => '08',
                        'September' => '09',
                        'October' => '10',
                        'November' => '11',
                        'December' => '12',
                    ];
                    $numericMonth = $monthMap[$request->month];
                    $data = $data->where(DB::raw("MONTH(invoices.date)"), '=', $numericMonth);
                }

                if($request->year != '') {
                    $data = $data->where(DB::raw("YEAR(invoices.date)"), '=', $request->year);
                }

                if($request->buyer != '') {
                    $data = $data->where('invoices.buyer_id', $request->buyer);
                }

                if (Auth::user()->user_type == 'S') {
                    $staffId = Staff::where('user_id',Auth::user()->id)->value('staff_id');
                    $data = $data->where('invoices.staff_id',$staffId);
                }

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('unit', function ($row) { 
                    $invoice = Invoice::find($row->invoice_id);
                    $invoice->load('products');
                    foreach ($invoice->products as $product) {
                        $tproduct = Product::find($product->product_id);
                        $tproduct->load('unit');
                        $unit = $tproduct->unit->name;
                        return $unit;
                    }
                })
                ->editColumn('payment_mode', function ($row) {
                    $id = $row->invoice_id;
                    $mode = $row->payment_mode;
                    if($mode == "Dacc" || $mode == "Credit") {
                        return "<select class='form-control updateMode' data-id='$id' data-minimum-results-for-search='Infinity'>
                            <option value='Dacc' " . ($mode == 'Dacc' ? 'selected' : '') . ">Dacc</option>
                            <option value='Paid' " . ($mode == 'Paid' ? 'selected' : '') . ">Paid</option>
                            <option value='Credit' " . ($mode == 'Credit' ? 'selected' : '') . ">Credit</option>
                        </select>"; 
                    } else {
                        return $mode;
                    } 
                })
                ->editColumn('status', function ($row) {
                    $id = encrypt($row->invoice_id);
                    if ($row->status == 'Active') {
                        $status = "<button title='Active' data-id='$id' data-type='Inactive' data-model='Invoice' data-field='status' class='btn btn-success status'>Active</button>";
                    } else {
                        $status = "<button title='Inactive' data-id='$id' data-type='Active' data-model='Invoice' data-field='status' class='btn btn-danger status'>Inactive</button>";
                    }
                    return $status;
                })
                ->editColumn('created_at', function ($row) {
                    return $row->created_at->format('d-m-Y');;
                })
                ->addColumn('action', function ($row) {
                    $id  = encrypt($row->invoice_id);
                    $btn = "<a href='".url('/invoice/download/'.$id)."' class='item-edit text-dark' title='Download Invoice'><svg viewBox='0 0 24 24' width='16' height='16' stroke='currentColor' stroke-width='2' fill='none' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg></a>";

                    $btn .= "<a href='".url('/invoice/edit/'.$id)."' class='item-edit text-dark ' style='margin-left:2px;' title='Edit Invoice'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Invoice' title='Delete Invoice'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->addColumn('staff', function ($row) {
                    $userId = Staff::where('staff_id',$row->staff_id)->value('user_id');
                    $user = User::find($userId);
                    if ($user != null) {
                        return $user->user_fname;
                    } else {
                        return null;
                    }
                })
                ->rawColumns(['action','status','payment_mode'])
                ->make(true);
            }

            $title = $this->title;
            $buyers = $this->getBuyers($sellerId);

            $totalInvoices = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->count();

            $dacc = Invoice::select(DB::raw("SUM(invoices.grand_total) as dacc"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Dacc')
            ->get();

            $paid = Invoice::select(DB::raw("SUM(invoices.grand_total) as paid"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Paid')
            ->get();


            $credit = Invoice::select(DB::raw("SUM(invoices.grand_total) as credit"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Credit')
            ->get();

            return view('invoice.index',compact('title','sale','buyers','totalInvoices','dacc','paid','credit'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('invoice/list');
        }
    }

    public function create() {
        $title = $this->title;

        $sellerId = $this->getSellerId();
        $buyers   = $this->getBuyers( $sellerId);
        $hsns     = $this->getHSNS( $sellerId);
        $products = $this->getProducts($sellerId);

        $invoice_no = 101;
        $invoice = Invoice::where('seller_id',$sellerId)->where('invoice_type','Invoice')->orderBy('invoice_id','desc')->first();
        if($invoice != null) {
            $invoice_no = (int)$invoice->invoice_no +1;
        }

        return view('invoice.create',compact('title','products','buyers','hsns','invoice_no'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
            $input['seller_id'] = $this->getSellerId();
            if (Auth::user()->user_type == 'S') {
                $staffId = Staff::where('user_id',Auth::user()->id)->value('staff_id');
                $input['staff_id'] = $staffId;
            }
            $input['invoice_type'] = 'Invoice';
            $invoice = Invoice::create($input);

            $totalProfit = 0;

            foreach ($input['columns'] as $key => $column) {
                $productData = array();
                $productData['invoice_id'] = $invoice->invoice_id;
                $productData['product_id']  = $column['product_id'];
                $productData['quantity']    = $column['quantity'];
                $productData['price']       = $column['price'];
                InvoiceProduct::create($productData);

                $product = Product::find( $column['product_id']);
                $product->updateAvailableStock($column['quantity']);

                $profit = ($column['price'] - $product->purchase_price) * $column['quantity'];
                $totalProfit += $profit;
            }

            Invoice::where('invoice_id',$invoice->invoice_id)->update(['profit'=>$totalProfit]);
            toastr()->success("Invoice Created Successfully");
            return redirect('invoice/list');
        } catch (\Exception $e) {
            dd($e);
            toastr()->error(Config('messages.500'));
            return redirect('invoice/create');
        }
    }

    public function edit($invoiceId) {
        try {
            $title = $this->title;
            $sellerId = $this->getSellerId();
            $invoice = Invoice::find(decrypt($invoiceId));
            $invoice->load('products');
            $buyers   = $this->getBuyers($sellerId);

            $hsns     = $this->getHSNS( $sellerId);
            $products = $this->getProducts($sellerId);
            return view('invoice.edit',compact('title','invoice','buyers', 'hsns','products'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('invoice/list');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['invoice_id'] = $input['invoice_id'];
            $invoice = Invoice::find($input['invoice_id']);
            
            Invoice::updateOrcreate($searchInput, $input);

            $totalProfit = 0;
            foreach ($input['columns'] as $key => $column) {
                $productData = array();
                $productData['invoice_id'] = $input['invoice_id'];
                $productData['product_id']  = $column['product_id'];
                $productData['quantity']    = $column['quantity'];
                $productData['price']       = $column['price'];

                $product = Product::find( $column['product_id']);

                if(isset($column['invoice_product_id'])) {
                    $invoiceProduct = InvoiceProduct::find( $column['invoice_product_id']);
                    $oldQuantity    = $invoiceProduct->quantity;
                    $product->restoreStock($oldQuantity);

                    $search['invoice_product_id'] = $column['invoice_product_id'];
                    InvoiceProduct::updateOrcreate($search,$productData);
                }else{
                    InvoiceProduct::create($productData);
                }
                $product->updateAvailableStock($column['quantity']);

                $profit = ($column['price'] - $product->purchase_price) * $column['quantity'];
                $totalProfit += $profit;

            }
            Invoice::where('invoice_id',$invoice->invoice_id)->update(['profit'=>$totalProfit]);
            toastr()->success("Invoice Updated Successfully");
            return redirect('invoice/list');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('invoice/list');
        }
    }

    public function gstHSN(Request $request) {
        try {
            $hsn = HSN::find($request->id);
            $percentage = (int)$hsn->percentage;

            $buyer = Buyer::find($request->buyer);
            if ($buyer->state_id != '33') {
                $data['cgst'] = null;
                $data['sgst'] = null;
                $data['igst'] = $percentage;
            } else {
                $data['cgst'] =  $percentage/2;
                $data['sgst'] = $percentage/2;
                $data['igst'] = null;
            }

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Retrieved Successfully';
            $resultArr['data']    = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    public function deleteProduct(Request $request) {
        try{
            InvoiceProduct::where('invoice_product_id',$request->id)->delete();

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Deleted Successfully';
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }   

    public function download($invoiceId) {
        try {
             //get seller details
             $user = Auth::user();
             if (Auth::user()->user_type == 'Buyer') {
                $buyerId = Buyer::where('user_id',$user->id)->value('buyer_id');
                $sellerId = Buyer::where('buyer_id',$buyerId)->value('seller_id');
                $user->getSeller = Seller::find($sellerId); 
                $data['user'] = $user;
             } else {
                $user->load('getSeller');
                $data['user'] = $user;
             }
           

            //get invoice details
            $invoice = Invoice::find(decrypt($invoiceId));
            $invoice->load('products');
            $invoice->load('getHSN');
            
            //get buyer details
            $buyer = Buyer::find($invoice->buyer_id);
            $buyer->load('getCity');
            $buyer->load('getState');
            $buyer->load('getUser');
            $data['invoice'] = $invoice;
            $data['buyer'] = $buyer;

            view()->share('data', $data);
            $pdf = PDF::loadView('invoice/invoice_view3', $data)->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true, 'show_warnings' => true]);
            $pdf->getDomPDF()->set_option("enable_php", true);
            return $pdf->download('invoice.pdf');
        } catch (\Exception $e) {
            dd($e);
            toastr()->error(Config('messages.500'));
            return redirect('invoice');
        }
    }

    public function makeInvoice(Request $request) {
        try {
            $sellerId = $this->getSellerId();
            $invoice_no = 101;
            $invoice = Invoice::where('seller_id',$sellerId)->where('invoice_type','Invoice')->orderBy('invoice_id','desc')->first();
            if($invoice != null) {
                $invoice_no = (int)$invoice->invoice_no +1;
            }

            Invoice::where('invoice_id',decrypt($request->id))->update(['invoice_type'=>'Invoice','invoice_no'=>$invoice_no.'/2024-2025']);

            $invoice = Invoice::find(decrypt($request->id));
            $invoice->load('products');

            foreach ($invoice->products as $key => $value) {
                $product = Product::find( $value->product_id);
                $product->updateAvailableStock($value->quantity);
            }

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Invoice created Successfully';
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    public function export()
    {
        try{
            return Excel::download(new InvoiceExport, 'sale.csv');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect()->back();
        }
    }

    public function filterData(Request $request) {
        try{

            $monthMap = [
                'January' => '01',
                'February' => '02',
                'March' => '03',
                'April' => '04',
                'May' => '05',
                'June' => '06',
                'July' => '07',
                'August' => '08',
                'September' => '09',
                'October' => '10',
                'November' => '11',
                'December' => '12',
            ];

            $sellerId = $this->getSellerId();
            $totalInvoices = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice');
            // ->count();

            $dacc = Invoice::select(DB::raw("SUM(invoices.grand_total) as dacc"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Dacc');
            // ->get();

            $paid = Invoice::select(DB::raw("SUM(invoices.grand_total) as paid"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Paid');


            $credit = Invoice::select(DB::raw("SUM(invoices.grand_total) as credit"))->join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->where('invoices.payment_mode','Credit');

            
            if($request->month != '') {
                $numericMonth = $monthMap[$request->month];
                $totalInvoices = $totalInvoices->where(DB::raw("MONTH(invoices.date)"), '=', $numericMonth);

                $dacc     = $dacc->where(DB::raw("MONTH(invoices.date)"), '=', $numericMonth);

                $paid   = $paid->where(DB::raw("MONTH(invoices.date)"), '=', $numericMonth);

                $credit   = $credit->where(DB::raw("MONTH(invoices.date)"), '=', $numericMonth);
            }


            if ($request->range != '') {
                if (strpos($request->range, " to ") !== false) {
                    list($startDate, $endDate) = explode(" to ", $request->range);
                    
                    // Parse dates with specific format
                    $startDate = date_create_from_format('m-d-Y', $startDate)->format('Y-m-d');
                    $endDate = date_create_from_format('m-d-Y', $endDate)->format('Y-m-d');


                    $dacc->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });

                    $paid->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });

                    $credit->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });

                    $totalInvoices->where(function($query) use ($startDate, $endDate) {
                        $query->where('invoices.date', '>=', $startDate)
                              ->where('invoices.date', '<=', $endDate);
                    });


                    // $dacc = $dacc->where(function($query) use ($startDate, $endDate) {
                    //     $query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) >= ?", [$startDate])
                    //           ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) <= ?", [$endDate]);
                    // });
            
                    // $paid = $paid->where(function($query) use ($startDate, $endDate) {
                    //     $query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) >= ?", [$startDate])
                    //           ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) <= ?", [$endDate]);
                    // });
            
                    // $credit = $credit->where(function($query) use ($startDate, $endDate) {
                    //     $query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) >= ?", [$startDate])
                    //           ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) <= ?", [$endDate]);
                    // });
                } else {
                    $totalInvoices = $totalInvoices->whereRaw("DATE_FORMAT(STR_invoices.date, '%Y-%m-%d') >= ?", [$request->range]);
                    $dacc = $dacc->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) >= ?", [$request->range]);
                    $paid = $paid->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) >= ?", [$request->range]);
                    $credit = $credit->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d')) >= ?", [$request->range]);
                }
            }
        

            if($request->year != '') {
                $totalInvoices = $totalInvoices->where(DB::raw("YEAR(invoices.date)"), '=', $request->year);

                $dacc = $dacc->where(DB::raw("YEAR(invoices.date)"), '=', $request->year);

                $paid = $paid->where(DB::raw("YEAR(invoices.date)"), '=', $request->year);

                $credit = $credit->where(DB::raw("YEAR(invoices.date)"), '=', $request->year);
            }

            $totalInvoices = $totalInvoices->count();
            $dacc     = $dacc->get();
            $dacc     = $dacc[0]->dacc ?? '0';
            $paid   = $paid->get();
            $paid   = $paid[0]->paid ?? '0';
            $credit   = $credit->get();
            $credit   = $credit[0]->credit ?? '0';

            $bill = $dacc + $paid + $credit;

            $data = [
                'totalInvoices' => $totalInvoices,
                'dacc'=>$dacc,
                'paid'=>$paid,
                'credit'=>$credit,
                'bill'=>$bill
            ];

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Deleted Successfully';
            $resultArr['data'] = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            dd($e);
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }
}
