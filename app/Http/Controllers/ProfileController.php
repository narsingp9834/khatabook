<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Seller;
use App\Models\User;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use CommonTrait;

    public function __construct()
    {
        $this->title = "Profile";
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function editProfile(){
        try{
            $user = $this->user;
            $user->load('getSeller');
            $user->duration = $this->getPlanDuration($user->id);
            $title = $this->title;
            $states = $this->getStates();
            $cities = $this->getCities();
            return view('/profile/edit', compact('title', 'user', 'states', 'cities'));
        }catch(\Exception $e){
            toastr()->error(Config('messages.500'));
            return redirect('/home');
        }
    }

    public function update(Request $request){
        try{
            $input = $request->except('_token');
            $user  = User::find($input['id']);

            // $data = $this->duplicateUserRecord($input, 'user_phone_no', 'User', 'id');
            // if($data->isNotEmpty()){
            //     toastr()->error('User '.Config('messages.phoneExist')); 
            //     return redirect('profile')->withInput();
            // }

            $searchInput['id'] = $input['id'];
            $user = User::updateorcreate($searchInput,$input);
            
            $sellerSearch['seller_id'] = $input['seller_id'];
            Seller::updateorcreate($sellerSearch,$input);
            toastr()->success('Profile '.Config('messages.update'));
            return redirect('home');
        }catch(\Exception $e){
            dd($e);
            toastr()->error(Config('messages.500'));
            return redirect('/profile');
        }
    }

    public function changePassword(){
        try{
            $user = $this->user;
            $title = 'Change Password';
            return view('/profile/change-password', compact('title', 'user'));
        }catch(\Exception $e){
            toastr()->error(Config('messages.500'));
            return redirect('/home');
        }
    }

    public function updatePassword(Request $request){
        try{
            $input = $request->except('_token');



            $input['password'] = Hash::make($request->password);
            $searchInput['id'] = $input['id'];
            User::updateorcreate($searchInput,$input);
            toastr()->success('Password '.Config('messages.update'));
            return redirect('change-password/');

            

        }catch(\Exception $e){
            toastr()->error(Config('messages.500'));
            return redirect('/change-password');
        }
    }
}
