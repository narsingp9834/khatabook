<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CMSProduct;
use Illuminate\Http\Request;
use App\Traits\CommonTrait;
use DataTables;

class CategoryController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Category";
    }

    public function index(Request $request) {

        try {
            if ($request->ajax()) {
                $sellerId = $this->getSellerId();
                $data = Category::query();
            
                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->category_id);
                    
                    $btn = "
                    <a href='".url('/category/edit/'.$id)."' class='item-edit text-dark' title='Edit Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Category' title='Delete Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->format('d-m-Y');
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = $this->title;
            return view('category.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('category');
        }
    }

    public function create() {
        $title = $this->title;
        return view('category.create',compact('title'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();

            Category::create($input);
            toastr()->success("Category Created Successfully");
            return redirect('category');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('category/create');
        }
    }

    public function edit($planId) {
        try {
            $title = $this->title;
            $category = Category::find(decrypt($planId));
            return view('category.edit',compact('title','category'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('category');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['category_id'] = $input['category_id'];

            Category::updateOrcreate($searchInput, $input);
            toastr()->success("Category Updated Successfully");
            return redirect('category');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('plans');
        }
    }

    public function products($id) {
        $category = Category::find($id);
        $products = CMSProduct::where('category_id',$id)->get();
        return view('category.view',compact('products','category'));
    }
}
