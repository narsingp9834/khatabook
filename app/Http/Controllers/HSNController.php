<?php

namespace App\Http\Controllers;

use App\Models\HSN;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DataTables;

class HSNController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "HSN";
        $this->middleware('auth');
    }

    public function index(Request $request) {

        try {
            if ($request->ajax()) {
                $sellerId = $this->getSellerId();
                $data = HSN::where('seller_id',$sellerId);
                

                return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    $id = encrypt($row->hsn_id);
                    if ($row->status == 'Active') {
                        $status = "<button title='Active' data-id='$id' data-type='Inactive' data-model='HSN' data-field='status' class='btn btn-success status'>Active</button>";
                    } else {
                        $status = "<button title='Inactive' data-id='$id' data-type='Active' data-model='HSN' data-field='status' class='btn btn-danger status'>Inactive</button>";
                    }
                    return $status;
                })
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->hsn_id);
                    
                    $btn = "
                    <a href='".url('/hsn/edit/'.$id)."' class='item-edit text-dark' title='Edit HSN'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='HSN' title='Delete HSN'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = $this->title;
            return view('hsn.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('supplier/create');
        }
    }

    public function create() {
        $title = $this->title;
        return view('hsn.create',compact('title'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
    
            $input['seller_id'] = $this->getSellerId();
            HSN::create($input);
            toastr()->success("HSN Created Successfully");
            return redirect('hsn');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('hsn/create');
        }
    }

    public function edit($hsnId) {
        try {
            $title = $this->title;
            $hsn = HSN::find(decrypt($hsnId));
            return view('hsn.edit',compact('title','hsn'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('hsn');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['hsn_id'] = $input['hsn_id'];

            HSN::updateOrcreate($searchInput, $input);
            toastr()->success("HSN Updated Successfully");
            return redirect('hsn');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('hsn');
        }
    }
}
