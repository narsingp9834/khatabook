<?php

namespace App\Http\Controllers;

use App\Exports\PurchaseExport;
use App\Models\Supplier;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Excel;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Supplier";
        $this->middleware('auth');
    }

    public function index(Request $request, $supplier=null) {

        try {
            $sellerId = $this->getSellerId();
            $monthMap = [
                'January' => '01',
                'February' => '02',
                'March' => '03',
                'April' => '04',
                'May' => '05',
                'June' => '06',
                'July' => '07',
                'August' => '08',
                'September' => '09',
                'October' => '10',
                'November' => '11',
                'December' => '12',
            ];

            $title = $this->title;
        
            if ($request->ajax()) {
                $data = Supplier::join('states','states.state_id','suppliers.state_id')->where('seller_id',$sellerId);
                
                if ($request->payment_mode != '') {
                    $data = $data->where('payment_mode', $request->payment_mode);
                }
                
                if($request->range != '') {

					if (strpos($request->range, " to ") !== false) {

						list($startDate, $endDate) = explode(" to ", $request->range);

						$data = $data->where(function($query) use ($startDate, $endDate) {
							$query->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
								  ->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
						});						
							
					} else {
						$data = $data->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
					}
				}

                if($request->month != '') {
                    $numericMonth = $monthMap[$request->month];
                    $data = $data->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth);
                }

                if($request->year != '') {
                    $data = $data->where(DB::raw("YEAR(suppliers.date)"), '=', $request->year);
                }

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->supplier_id);
                    
                    $btn = "
                    <a href='".url('/supplier/view/'.$id)."' class='item-edit text-secondary'  title='View Supplier'><svg xmlns='http://www.w3.org/2000/svg' width=24 height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-eye font-small-4'><path d='M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z'></path><circle cx='12' cy='12' r='3''></circle></svg></a>

                    <a href='".url('/supplier/edit/'.$id)."' class='item-edit text-dark' title='Edit Supplier'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Supplier' title='Delete Supplier'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
            }

                       
            $totalInvoices = Supplier::where('seller_id',$sellerId)->count();
            $totalPaid     = Supplier::select(DB::raw("SUM(suppliers.total) as paid"))->where('payment_mode','Paid')->where('seller_id',$sellerId)->get();
            $totalUnPaid   = Supplier::select(DB::raw("SUM(suppliers.total) as unpaid"))->where('payment_mode','Pending')->where('seller_id',$sellerId)->get();
            $totalPurchade   = Supplier::select(DB::raw("SUM(suppliers.total)as total"))->where('seller_id',$sellerId)->get(); 
            return view('supplier.index',compact('title','supplier','totalInvoices','totalPaid','totalUnPaid','totalPurchade'));
        } catch (\Exception $e) {
            dd($e);
            toastr()->error(Config('messages.500'));
            return redirect('supplier/create');
        }
    }

    public function create() {
        $title = $this->title;
        $states = $this->getStates();
        return view('supplier.create',compact('title','states'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
            
            $price = (int)$input['price'] * (int)$input['quantity'];
            $gstAmount = 0;
            if($input['igst'] != null) {
                $percentageDecimal = (int)$input['igst'] / 100;

                // Calculate GST amount
                $gstAmount += $price * $percentageDecimal;
            }

            if($input['cgst'] != null) {
                $percentageDecimal = (int)$input['cgst'] / 100;

                // Calculate GST amount
                $gstAmount += $price * $percentageDecimal;
            }

            if($input['sgst'] != null) {
                $percentageDecimal = (int)$input['sgst'] / 100;

                // Calculate GST amount
                $gstAmount += $price * $percentageDecimal;
            }

            $totalAmount    = $price + $gstAmount;
            $input['total'] = $totalAmount;
    
            $input['seller_id'] = $this->getSellerId();
            Supplier::create($input);
            toastr()->success("Supplier Created Successfully");
            return redirect('supplier/list');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('supplier/create');
        }
    }

    public function edit($supplierId) {
        try {
            $title = $this->title;
            $supplier = Supplier::find(decrypt($supplierId));
            $states = $this->getStates();
            return view('supplier.edit',compact('title','states','supplier'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('supplier/list');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();
            $price = (int)$input['price'] * (int)$input['quantity'];
            $gstAmount = 0;
            if($input['igst'] != null) {
                $percentageDecimal = (int)$input['igst'] / 100;

                // Calculate GST amount
                $gstAmount += $price * $percentageDecimal;
            }

            if($input['cgst'] != null) {
                $percentageDecimal = (int)$input['cgst'] / 100;

                // Calculate GST amount
                $gstAmount += $price * $percentageDecimal;
            }

            if($input['sgst'] != null) {
                $percentageDecimal = (int)$input['sgst'] / 100;

                // Calculate GST amount
                $gstAmount += $price * $percentageDecimal;
            }

            $totalAmount    = $price + $gstAmount;
            $input['total'] = $totalAmount;

            $searchInput['supplier_id'] = $input['supplier_id'];

            Supplier::updateOrcreate($searchInput, $input);
            toastr()->success("Supplier Updated Successfully");
            return redirect('supplier/list');

        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('supplier/list');
        }
    }

    public function view($supplierId) {
        try {
            $title = $this->title;
            $supplier = Supplier::find(decrypt($supplierId));
            $states = $this->getStates();
            return view('supplier.view',compact('title','states','supplier'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('supplier');
        }
    }

    public function export()
    {
        try{
            return Excel::download(new PurchaseExport, 'purchase.csv');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect()->back();
        }
    }

    public function filerData(Request $request) {
        try{

            $monthMap = [
                'January' => '01',
                'February' => '02',
                'March' => '03',
                'April' => '04',
                'May' => '05',
                'June' => '06',
                'July' => '07',
                'August' => '08',
                'September' => '09',
                'October' => '10',
                'November' => '11',
                'December' => '12',
            ];

            $sellerId = $this->getSellerId();
            $totalInvoices = Supplier::where('seller_id',$sellerId);
            $totalPaid     = Supplier::select(DB::raw("SUM(suppliers.total) as paid"))->where('payment_mode','Paid')->where('seller_id',$sellerId);
            $totalUnPaid   = Supplier::select(DB::raw("SUM(suppliers.total) as unpaid"))->where('payment_mode','Pending')->where('seller_id',$sellerId);
            $totalPurchade   = Supplier::select(DB::raw("SUM(suppliers.total)as total"))->where('seller_id',$sellerId);
            
            if($request->month != '') {
                $numericMonth = $monthMap[$request->month];
                $totalInvoices = $totalInvoices->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth);

                $totalPaid     = $totalPaid->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth);

                $totalUnPaid   = $totalUnPaid->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth);

                $totalPurchade   = $totalPurchade->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth);
            }


            if($request->range != '') {

                if (strpos($request->range, " to ") !== false) {

                    list($startDate, $endDate) = explode(" to ", $request->range);

                    $totalInvoices = $totalInvoices->where(function($query) use ($startDate, $endDate) {
                        $query->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
                              ->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
                    });	
                    
                    $totalPaid = $totalPaid->where(function($query) use ($startDate, $endDate) {
                        $query->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
                              ->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
                    });	

                    $totalUnPaid = $totalUnPaid->where(function($query) use ($startDate, $endDate) {
                        $query->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
                              ->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
                    });	

                    $totalPurchade = $totalPurchade->where(function($query) use ($startDate, $endDate) {
                        $query->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
                              ->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
                    });	
                        
                } else {
                    $totalInvoices = $totalInvoices->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);


                    $totalPaid = $totalPaid->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);

                    $totalUnPaid = $totalUnPaid->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);

                    $totalPurchade = $totalPurchade->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
                }
            }

            if($request->year != '') {
                $totalInvoices = $totalInvoices->where(DB::raw("YEAR(suppliers.date)"), '=', $request->year);

                $totalPaid = $totalPaid->where(DB::raw("YEAR(suppliers.date)"), '=', $request->year);

                $totalUnPaid = $totalUnPaid->where(DB::raw("YEAR(suppliers.date)"), '=', $request->year);

                $totalPurchade = $totalPurchade->where(DB::raw("YEAR(suppliers.date)"), '=', $request->year);
            }

            $totalInvoices = $totalInvoices->count();
            $totalPaid     = $totalPaid->get();
            $totalPaid     = $totalPaid[0]->paid ?? '0';
            $totalUnPaid   = $totalUnPaid->get();
            $totalUnPaid   = $totalUnPaid[0]->unpaid ?? '0';
            $totalPurchade   = $totalPurchade->get();
            $totalPurchade   = $totalPurchade[0]->total ?? '0';

            $data = [
                'totalInvoices' => $totalInvoices,
                'totalPaid'=>$totalPaid,
                'totalUnPaid'=>$totalUnPaid,
                'totalPurchade'=>$totalPurchade
            ];

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Deleted Successfully';
            $resultArr['data'] = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }
}
