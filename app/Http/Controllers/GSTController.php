<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Invoice;
use App\Models\Supplier;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Excel;
use App\Exports\GSTR1Export;
use App\Models\Buyer;

class GSTController extends Controller
{
    use CommonTrait;
    public function __construct()
  	{
	    $this->middleware('auth');
	}
    public function gstr1(Request $request) {
        try {
            $title = "GST";
            $sellerId = $this->getSellerId();
            $buyerCount = Invoice::select('buyer_id')
            ->where('invoices.seller_id',$sellerId)
            ->distinct()
            ->count();

            $invoiceCount = Invoice::select('buyer_id')
            ->where('invoices.seller_id',$sellerId)
            ->count();

            $invoices = Invoice::with('products')->where('invoices.seller_id',$sellerId)->get();
            $totalValue = 0;
            foreach ($invoices as $key => $invoice) {
                foreach ($invoice->products as $key => $product) {
                    $totalValue += $product->quantity * $product->price; 
                }
            }

            $totalTaxRate = Invoice::select(DB::raw('SUM(igst) as igst','SUM(sgst) as sgst','SUM(cgst) as cgst'))
            ->where('seller_id', $sellerId)
            ->first();

            $totalTax = Helper::getTaxAmount($totalValue,$totalTaxRate->igst +$totalTaxRate->sgst + $totalTaxRate->cgst);

            $totalWithTax =  $totalTax + $totalValue;

            if ($request->ajax()) {
                $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')
                ->join('sellers','sellers.seller_id','invoices.seller_id')    
                ->where('invoices.seller_id',$sellerId)
                ->where('invoice_type','Invoice');
                return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($row) {
                    
                    return Helper::getDate($row->created_at);
                })
                ->addColumn('total_amount', function ($row) {
                    
                    $invoice = Invoice::with('products')->where('invoices.invoice_id',$row->invoice_id)->first();
                    $totalValue = 0;
                    
                    foreach ($invoice->products as $key => $product) {
                        $totalValue += $product->quantity * $product->price; 
                    }
                    return $totalValue;
                })
                // ->addColumn('party_name', function ($row) {
                //     $buyer = Buyer::find($row->buyer_id);  
                //     if ($buyer != null) {
                //         $buyer->load('getUser');
                //         return $buyer->getUser->user_fname;
                //     } else {
                //         return null;
                //     }
                // })
                ->addColumn('state', function ($row) {
                    $buyer = Buyer::find($row->buyer_id);  
                    if ($buyer != null) {
                        $buyer->load('getState');
                        return $buyer->getState->name;
                    } else {
                        return null;
                    }
                })
                ->addColumn('total_pieces', function ($row) {
                    
                    $invoice = Invoice::with('products')->where('invoices.invoice_id',$row->invoice_id)->first();
                    $totalPieces = 0;
                    
                    foreach ($invoice->products as $key => $product) {
                        $totalPieces += $product->quantity; 
                    }
                    // dd($totalPieces);
                    return $totalPieces;
                })
                // ->rawColumns(['action','status'])
                ->make(true);
            }
            return view('gst.gst1',compact('title','buyerCount','invoiceCount','totalValue','totalTax','totalWithTax'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('gstr1');
        }
    }

    public function gstr3(Request $request) {
        try {
            $sellerId = $this->getSellerId();
            $purchaseData = Supplier::select(
                'price',
                'quantity',
                'cgst',
                'sgst',
                'igst',
                'total'
            )->where('suppliers.seller_id',$sellerId)
            ->get();
            

            $purchaseCgstTax = 0;
            $purchaseSgstTax = 0;
            $purchaseIgstTax = 0;
            $totalPurchase   = 0;

            // Calculate taxes for each row
            foreach ($purchaseData as $row) {
                // $subtotal = (int)$row->price * (int)$row->quantity;
                $subtotal = (int)$row->total;
                $totalPurchase  +=  $subtotal;
                $purchaseCgstTax = $subtotal * ((int)$row->cgst / 100);
                $purchaseSgstTax = $subtotal * ((int)$row->sgst / 100);
                $purchaseIgstTax = $subtotal * ((int)$row->igst / 100);
            }

            $totalPurchaseTax = $purchaseCgstTax + $purchaseSgstTax + $purchaseIgstTax;
            
            $sellData = Invoice::with('products')
            ->where('invoices.seller_id',$sellerId)
            ->get();

            $sellCgstTax = 0;
            $sellSgstTax = 0;
            $sellIgstTax = 0;
            $totalSell   = 0;

            // Calculate taxes for each row
            foreach ($sellData as $row) {
        
                foreach ($row->products as $key => $product) {
                    $subtotal = $product->price * $product->quantity;
                    $subtotal =  $subtotal - ($subtotal * $row->discount/100);
                    $totalSell += $subtotal;
                $sellCgstTax += $subtotal * ($row->cgst / 100);
                $sellSgstTax += $subtotal * ($row->sgst / 100);
                $sellIgstTax += $subtotal * ($row->igst / 100);

                }
            }

            $totalSellTax = $sellCgstTax + $sellSgstTax + $sellIgstTax;

            $ITC =  $totalSellTax - $totalPurchaseTax;
            return view('gst.gst3',compact('purchaseCgstTax', 'purchaseSgstTax', 'purchaseIgstTax','sellCgstTax', 'sellSgstTax', 'sellIgstTax', 'totalPurchaseTax','totalSellTax', 'totalSell','totalPurchase','ITC'));  
        } catch (\Throwable $th) {
            dd($th);
        }
    }

    public function getGst3Stats(Request $request) {
        try {
            $sellerId = $this->getSellerId();
            $purchaseData = Supplier::select(
                'price',
                'quantity',
                'cgst',
                'sgst',
                'igst',
                'total'
            )->where('suppliers.seller_id',$sellerId);

            if($request->range != '') {

                if (strpos($request->range, " to ") !== false) {

                    list($startDate, $endDate) = explode(" to ", $request->range);

                    $purchaseData = $purchaseData->where(function($query) use ($startDate, $endDate) {
                        $query->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
                              ->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
                    });						
                        
                } else {
                    $purchaseData = $purchaseData->whereRaw("DATE_FORMAT(STR_TO_DATE(suppliers.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
                }
            }

            if($request->month != '') {
                $monthMap = [
                    'January' => '01',
                    'February' => '02',
                    'March' => '03',
                    'April' => '04',
                    'May' => '05',
                    'June' => '06',
                    'July' => '07',
                    'August' => '08',
                    'September' => '09',
                    'October' => '10',
                    'November' => '11',
                    'December' => '12',
                ];
                $numericMonth = $monthMap[$request->month];
                $purchaseData = $purchaseData->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth);
            }

            if($request->year != '') {
                $purchaseData = $purchaseData->where(DB::raw("YEAR(suppliers.date)"), '=', $request->year);
            }

            $purchaseData = $purchaseData->get();
            

            $purchaseCgstTax = 0;
            $purchaseSgstTax = 0;
            $purchaseIgstTax = 0;
            $totalPurchase   = 0;

            // Calculate taxes for each row
            foreach ($purchaseData as $row) {
                $subtotal = (int)$row->price * (int)$row->quantity;
                $totalPurchase  +=  (int)$row->total;
                $purchaseCgstTax = $subtotal * ((int)$row->cgst / 100);
                $purchaseSgstTax = $subtotal * ((int)$row->sgst / 100);
                $purchaseIgstTax = $subtotal * ((int)$row->igst / 100);
            }

            $totalPurchaseTax = $purchaseCgstTax + $purchaseSgstTax + $purchaseIgstTax;

            // $totalPurchase = $totalPurchase + $totalPurchaseTax;

            $sellData = Invoice::with('products')->where('invoices.seller_id',$sellerId);
            
            if($request->range != '') {

                if (strpos($request->range, " to ") !== false) {

                    list($startDate, $endDate) = explode(" to ", $request->range);

                    $sellData = $sellData->where(function($query) use ($startDate, $endDate) {
                        $query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
                              ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
                    });						
                        
                } else {
                    $sellData = $sellData->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
                }
            }

            if($request->month != '') {
                $monthMap = [
                    'January' => '01',
                    'February' => '02',
                    'March' => '03',
                    'April' => '04',
                    'May' => '05',
                    'June' => '06',
                    'July' => '07',
                    'August' => '08',
                    'September' => '09',
                    'October' => '10',
                    'November' => '11',
                    'December' => '12',
                ];
                $numericMonth = $monthMap[$request->month];
                $sellData = $sellData->where(DB::raw("MONTH(invoices.date)"), '=', $numericMonth);
            }

            if($request->year != '') {
                $sellData = $sellData->where(DB::raw("YEAR(invoices.date)"), '=', $request->year);
            }

            $sellData = $sellData->get();

            $sellCgstTax = 0;
            $sellSgstTax = 0;
            $sellIgstTax = 0;
            $totalSell   = 0;

            // Calculate taxes for each row
            foreach ($sellData as $row) {
        
                foreach ($row->products as $key => $product) {
                    $subtotal = $product->price * $product->quantity;
                    $subtotal =  $subtotal - ($subtotal * $row->discount/100);
                    $totalSell += $row->grand_total;
                $sellCgstTax += $subtotal * ($row->cgst / 100);
                $sellSgstTax += $subtotal * ($row->sgst / 100);
                $sellIgstTax += $subtotal * ($row->igst / 100);

                }
            }

            $totalSellTax = $sellCgstTax + $sellSgstTax + $sellIgstTax;

            // $totalSell = $totalSell + $totalSellTax;

            $ITC =  $totalSellTax - $totalPurchaseTax;

            $data = [
                'purchaseCgstTax' =>$purchaseCgstTax,
                'purchaseSgstTax' =>$purchaseSgstTax,
                'purchaseIgstTax' =>$purchaseIgstTax,
                'sellCgstTax' =>$sellCgstTax,
                'sellSgstTax' =>$sellSgstTax,
                'sellIgstTax'=>$sellIgstTax,
                'totalPurchaseTax'=>$totalPurchaseTax,
                'totalSellTax'=>$totalSellTax,
                'totalSell'=>$totalSell,
                'totalPurchase'=>$totalPurchase,
                'ITC'=>$ITC
            ];

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Retrieved Successfully';
            $resultArr['data']    = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            dd($e);
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    public function gstr1Export()
    {
        try{
            return Excel::download(new GSTR1Export, 'purchase.csv');
        } catch (\Exception $e) {
            dd($e);
            toastr()->error(Config('messages.500'));
            return redirect()->back();
        }
    }
}
