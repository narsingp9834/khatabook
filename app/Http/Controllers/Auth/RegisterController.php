<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\CommonMail;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Generator;
use App\Models\OrgCustomer;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
use App\Models\AddressType;
use App\Models\Notification;
use App\Models\Seller;
use App\Models\ZipCode;
use App\Traits\EmailTrait;
use App\Models\TransferSwitchType;
use App\Models\TransferSwitch;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, CommonTrait, EmailTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //     ]);
    // }

    // Register
    // public function showRegistrationForm()
    // {
    //     $pageConfigs = ['blankPage' => true];

    //     return view('/auth/register', [
    //     'pageConfigs' => $pageConfigs
    //     ]);
    // }

    // Register
    public function showRegistrationForm()
    {
        try{
          $countries = $this->getCountries();
          $customerCategories = $this->getCategories(1);
          $addressTypes = $this->getAddressTypes(1);
          $manufacturers = $this->getManufacturers(1,'generator');
          $generatorTypes = $this->getGeneratorTypes(1);
          $fuelTypes = $this->getFuelTypes(1);
          $transferSwitchs = $this->getTransferSwitch(1);
          $transferSwitchTypes = TransferSwitchType::where('switch_type_status','Active')->orderBy('switch_type_name')->get();
  
          return view('/auth/register',compact('countries','customerCategories','addressTypes','manufacturers','generatorTypes','fuelTypes','transferSwitchs','transferSwitchTypes'));
      }catch(\Exception $e) {
        toastr()->error(Config('messages.500'));
        return redirect('login/');
      }
    }

    public function store(Request $request){
      DB::beginTransaction();
      try{
        $password = '';
        $input = $request->all();
        $password = $input['password'];
        $input['password'] = Hash::make($input['password']);
        $input['user_type'] = "B";
        $input['email'] = $input['user_phone_no'];

  
        // Check whether same phone no is exist or not
        $phoneData = $this->duplicateUserRecord($input, 'user_phone_no', 'User');
  
        if($phoneData->isNotEmpty()){
            toastr()->error('User '.Config('messages.phoneExist')); 
            return redirect()->back()->withInput();
        }

      //   $addressTypeData = AddressType::find($input['address_type_id']);
      // if(!($addressTypeData->address_type == "Residence")) {
      //   $data = ZipCode::where('zip_code', $input['zip_code'])->whereNotNull('zip_code')->where('org_id',1)->whereNull('deleted_at')->get();
      //   if (($data->isEmpty()) || (isset($data[0]['zip_code_status']) && $data[0]['zip_code_status'] == 'Inactive')) {
      //     toastr()->error(Config('messages.zip'));
      //     return redirect()->back()->withInput();
      //   }
      // }
  
        // Save data into users table
        $user = User::create($input);
  
        if(!empty($user)){

          User::where('id',$user->id)->update(['added_by_user_id'=>$user->id]);

          // Save data into customers table
          $input['user_id'] = $user->id;
          
          $seller = Seller::create($input);
             
        
          
          Auth::login($user);
          
          toastr()->success(Config('messages.register'));
          DB::commit();
          return redirect('home/');
        }
  
      }catch(\Exception $e) {
        DB::rollback();
        toastr()->error(Config('messages.500'));
        return redirect()->back()->withInput();
      }
    }
}
