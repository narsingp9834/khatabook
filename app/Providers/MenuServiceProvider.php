<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // get all data from menu.json file
        $verticalMenuJson = file_get_contents(base_path('resources/data/menu-data/verticalMenu.json'));
        $verticalMenuData = json_decode($verticalMenuJson);
        $horizontalMenuJson = file_get_contents(base_path('resources/data/menu-data/horizontalMenu.json'));
        $horizontalMenuData = json_decode($horizontalMenuJson);

        $billerMenuJson = file_get_contents(base_path('resources/data/menu-data/billerMenu.json'));
        $billerMenuData = json_decode($billerMenuJson);

        $staffMenuJson = file_get_contents(base_path('resources/data/menu-data/StaffMenu.json'));
        $staffMenuData = json_decode($staffMenuJson);

        $buyerMenuJson = file_get_contents(base_path('resources/data/menu-data/buyerMenu.json'));
        $buyerMenuData = json_decode($buyerMenuJson);

         // Share all menuData to all the views
        \View::share('menuData',[$verticalMenuData, $horizontalMenuData, $billerMenuData, $staffMenuData,$buyerMenuData]);
    }
}
