<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminAptMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */ 
    public function __construct($details=null,$notification=null){
        $this->details = $details;
        $this->notification = $notification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){

        $details = $this->details;
        $notification = $this->notification;
        $subject = $details['subject'];
        return  $this->subject($subject)->with(
            [
                'details' => $details, 
                'notification'=>$notification
            ]
        )->markdown('email.admin_apt_mail');
    }
}
