<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'method',
        'city',
        'phone_no',
        'message',
        'created_at',
        'updated_at'
    ];

    public $primaryKey = 'contact_us_id';
    public $table = 'contact_us';
}
