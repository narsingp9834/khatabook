<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPlanRequest extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'plan_id',
        'Status',
        'created_at',
        'updated_at'
    ];

    public $primaryKey = 'user_plan_request_id';
    public $table = 'user_plan_requests';
}
