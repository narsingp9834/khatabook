<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CMSProduct extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'category_id',
        'contents',
        'image1',
        'image2',
        'image3',
        'image4',
        'created_at',
        'updated_at'
    ];

    public $primaryKey = 'cms_product_id';
    public $table = 'cms_product';
}
