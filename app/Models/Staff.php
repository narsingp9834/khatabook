<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    public $primaryKey = 'staff_id';

    protected $fillable = [
    	'user_id',
        'seller_id'
    ];
    public $timestamps = false;
}
