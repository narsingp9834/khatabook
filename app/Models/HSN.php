<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HSN extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'seller_id',
        'code',
        'percentage',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $primaryKey = 'hsn_id';
}
