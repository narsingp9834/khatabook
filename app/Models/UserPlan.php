<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'plan_id',
    ];

    public $primaryKey = 'user_plan_id';
    public $timestamps = false;
}
