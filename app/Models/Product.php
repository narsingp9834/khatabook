<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'seller_id',
        'product_name',
        'date',
        'unit_id',
        'hsn_id',
        'max_quantity',
        'min_quantity',
        'available_stock',
        'purchase_price',
        'sale_price',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $primaryKey = 'product_id';

     // Method to update available stock
     public function updateAvailableStock($quantity)
     {
         $this->available_stock -= $quantity;
         $this->save();
     }

     // Method to restore available stock when invoice is deleted or edited
    public function restoreStock($quantity)
    {
        $this->available_stock += $quantity;
        $this->save();
    }

    public function unit() {
        return $this->hasOne(Unit::class,'unit_id', 'unit_id');
    }
}
