<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'name',
        'contents',
        'year',
        'amount',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $primaryKey = 'plan_id';
}
