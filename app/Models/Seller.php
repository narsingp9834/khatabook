<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;

    public $primaryKey = 'seller_id';

    protected $fillable = [
    	'user_id',
        'gst_no',
        'state_id',
        'city_id',
        'pin_no',
        'address',
        'description',
        'scanner_link'
    ];
    public $timestamps = false;

    public function getCity() {
        return $this->hasOne(City::class,'city_id', 'city_id');
    }

    public function getState() {
        return $this->hasOne(State::class,'state_id', 'state_id');
    }

    public function getUser() {
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
