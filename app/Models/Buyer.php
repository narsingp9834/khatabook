<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buyer extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'seller_id',
        'gst_no',
        'target',
        'phone_no',
        'state_id',
        'city_id',
        'pin_no',
        'address',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $primaryKey = 'buyer_id';


    public function getCity() {
        return $this->hasOne(City::class,'city_id', 'city_id');
    }

    public function getState() {
        return $this->hasOne(State::class,'state_id', 'state_id');
    }

    public function getUser() {
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
