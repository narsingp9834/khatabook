<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'seller_id',
        'staff_id',
        'buyer_id',
        'invoice_no',
        'invoice_suffix',
        'hsn_id',
        'igst',
        'cgst',
        'sgst',
        'discount',
        'gst_amount',
        'total',
        'grand_total',
        'profit',
        'invoice_type',
        'payment_mode',
        'date',
        'advance_amount',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $primaryKey = 'invoice_id';

    public function products() {
        return $this->hasMany(InvoiceProduct::class,'invoice_id', 'invoice_id')->with('getProduct');
    }

    public function getHSN() {
        return $this->hasOne(HSN::class,'hsn_id', 'hsn_id');
    }
}
