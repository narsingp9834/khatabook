<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use App\Models\Notification as MailNotification;
use App\Models\User;

class PasswordReset extends Notification
{
    use Queueable;
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // if (static::$toMailCallback) {
        //     return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        // }

        $url = url(rtrim(config('app.url'),"/") . route('password.reset', [
            'token' => $this->token,
            'email' => $notifiable->getEmailForPasswordReset(),
        ], false));

        $notification = MailNotification::where(['notification_type' => 'email', 'notification_for' => 'forgot_password', 'status' => 'Active'])->first();

        $user = User::leftJoin('customers','customers.user_id','users.id')->where('users.email',$notifiable->getEmailForPasswordReset())->first();

        $user_fname = $user_lname = "";
        if(isset($user->user_fname) && $user->user_fname!=''){
            $user_fname = $user->user_fname;
        }
        if(isset($user->user_lname) && $user->user_lname!=''){
            $user_lname = $user->user_lname;
        }
        $username = $user_fname." ".$user_lname;
        if(($user['user_fname'] == null) &&($user['user_lname'] == null)){
            $username = $user->company_name;
        }

        return (new MailMessage)
                      ->view('email.reset_password', ['name'=>$notifiable->name,'url' => $url,'username'=>$username,'notification'=>$notification])
                      ->subject($notification->subject);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
