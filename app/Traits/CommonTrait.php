<?php
namespace App\Traits;

use App\Mail\CommonMail;
use App\Models\Buyer;
use App\Models\City;
use App\Models\HSN;
use App\Models\Product;
use App\Models\Staff;
use Illuminate\Http\Request;

use App\Models\State;
use App\Models\Unit;
use App\Models\User;
use App\Models\UserPlan;
use Carbon\Carbon;
use Auth;
use App\Models\Plan;

trait CommonTrait {

    public function getStates(){
        $states = State::where('state_status','Active')->orderBy('name','asc')->get();
        return $states;
    }

    public function getCities(){
        $states = City::where('city_status','Active')->orderBy('name','asc')->get();
        return $states;
    }

    // delete entry from table
    public function deleteRecord($request, $model, $field){
        $prefix = '\App\Models';
        $model_name = $prefix. "\\". $model;
        $data = $model_name::findOrFail(decrypt($request->id));
        $data->delete();
    
        if ($model == 'Plan') {
            UserPlan::where('plan_id',decrypt($request->id))->delete();
        }

        $resultArr['title'] = 'Success';
        $resultArr['message'] = 'Record deleted successfully.';
        echo json_encode($resultArr);
        exit;
    }

    //get logged in user seller id
    public function getSellerId() {
        $user = Auth::user();
        if ($user->user_type == "B") {
            $user->load('getSeller');
            return $user->getSeller->seller_id;
        } else {
            return Staff::where('user_id',$user->id)->value('seller_id');
        }
    }

    // update status
    public function modifyStatus($request, $model, $field){
        $prefix = '\App\Models';
        $model_name = $prefix. "\\". $model;
        $data = $model_name::findOrFail(decrypt($request->id));
        $data->$field = $request->type;
        $data->save();
        $resultArr['title'] = 'Success';
        $resultArr['message'] = 'Status updated successfully.';
        echo json_encode($resultArr);
        exit;
    }

    public function getHSNS($sellerId) {
        return HSN::where('status','Active')->where('seller_id',$sellerId)->get();
    }

    public function getUnits($sellerId) {
        return Unit::where('status','Active')->where('seller_id',$sellerId)->get();
    }

    public function getBuyers($sellerId) {
        return Buyer::join('users','users.id','buyers.user_id')->where('status','Active')->where('seller_id',$sellerId)->get();
    }

    public function getProducts($sellerId) {
        return Product::where('status','Active')->where('seller_id',$sellerId)->get();
    }

    public function duplicateUserRecord($input, $field, $model, $searchId=null){
        $prefix = '\App\Models';
        $model_name = $prefix. "\\". $model;
       
        if($searchId!=''){
            $data = $model_name::where($searchId,'!=',$input[$searchId])->where($field, $input[$field])->whereNotNull($field)->whereNull('deleted_at')->get();
        }else{
            $data = $model_name::where($field, $input[$field])->whereNotNull($field)->whereNull('deleted_at')->get();
        }
       
        return $data;
    }

    public function checkPlan($userId) {
        $user = User::find($userId);
        if (isset($user)) {
            // Get the current date
            $currentDate = Carbon::now();
            
            // dump($currentDate);

            $userPlan = UserPlan::where('user_id',$userId)->first();
            if ($userPlan != null) {
                    $plan = Plan::find($userPlan->plan_id);
                    $checkDate = $user->created_at->addYears($plan->year);
                    if ($currentDate <= $checkDate) {
                        return true;
                    } else {
                        return false;
                    }
            } else {
                $sevenDaysDate = ($user->created_at->addDays(7));
                if ($currentDate <= $sevenDaysDate) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function getPlan($userId) {
        $user = User::find($userId);
        // Get the current date
        $currentDate = Carbon::now();
        
        // dump($currentDate);

        $userPlan = UserPlan::where('user_id',$userId)->first();
        if ($userPlan != null) {
                $plan = Plan::find($userPlan->plan_id);
                $checkDate = $user->created_at->addYears($plan->year);
                if ($currentDate <= $checkDate) {
                    return $plan->name;
                } else {
                    return null;
                }
        } else {
            $sevenDaysDate = ($user->created_at->addDays(7));
            if ($currentDate <= $sevenDaysDate) {
                return "Demo";
            }
        }
    }

    public function getPlanStatus($userId) {
        $user = User::find($userId);
        // Get the current date
        $currentDate = Carbon::now();
        
        // dump($currentDate);

        $userPlan = UserPlan::where('user_id',$userId)->orderBy('user_plans.user_plan_id','desc')->first();
        if ($userPlan != null) {
                $plan = Plan::find($userPlan->plan_id);
                $checkDate = $user->created_at->addYears($plan->year);
                if ($currentDate <= $checkDate) {
                    return 'Active';
                } else {
                    return 'Inactive';
                }
        } else {
            $sevenDaysDate = ($user->created_at->addDays(7));
            if ($currentDate <= $sevenDaysDate) {
                return "Demo";
            } else {
                return 'Inactive';
            }
        }
    }

    public function getPlanDuration($userId) {
        $user = User::find($userId);
        // Get the current date
        $currentDate = Carbon::now();
        $startDate = $user->created_at->format('d-m-Y');


        $userPlan = UserPlan::where('user_id',$userId)->first();
        // dd($userPlan);
        if ($userPlan != null) {
                $plan = Plan::find($userPlan->plan_id);
                $checkDate = $user->created_at->addYears($plan->year);
                if ($currentDate <= $checkDate) {
                    // $endDate =  $user->created_at->addYears($plan->year);
                    $checkDate = $checkDate->format('d-m-Y');

                   
                    // return 
                    return $startDate .' to '.$checkDate ;
                } else {
                    return null;
                }
        } else {
            $checkDate = $user->created_at->addDays(7);
            $sevenDaysDate = ($user->created_at->addDays(7));
            if ($currentDate <= $sevenDaysDate) {
                return $startDate .' to '.$checkDate;
            } else {
                return 'Inactive';
            }
        }
    }
}



