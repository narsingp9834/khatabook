<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Traits\CommonTrait;
use App\Models\Invoice;
use App\Helpers\Helper;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class GSTR1Export implements FromCollection, WithHeadings
{
    use CommonTrait;
    public function __construct()
    {
        $this->heading = ['GSTIN', 'Invoice No', 'Date', 'Total Amount', 'Rate', 'IGST', 'CGST', 'SGST'];

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   

            $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')
            ->join('sellers','sellers.seller_id','invoices.seller_id')    
            ->where('invoices.seller_id',$this->getSellerId())
            ->where('invoice_type','Invoice')
            ->get();
            
            $data->transform(function ($item) {
        
                $generatorData1 = [
                    $item->gst_no,
                    $item->invoice_no,
                    Helper::getDate($item->created_at), 
                    $this->getTotalAmount($item->invoice_id), 
                    $item->igst + $item->cgst + $item->sgst, 
                    $item->igst,
                    $item->cgst,
                    $item->sgst,
                ];

                return $generatorData1;
            });

            return $data;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return $this->heading;
        
    }

    private function getTotalAmount($invoice_id) {
        $invoice = Invoice::with('products')->where('invoices.invoice_id',$invoice_id)->first();
                    $totalValue = 0;
                    
                    foreach ($invoice->products as $key => $product) {
                        $totalValue += $product->quantity * $product->price; 
                    }
        return $totalValue;
    }
}
