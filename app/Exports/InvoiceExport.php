<?php

namespace App\Exports;

use App\Models\Invoice;
use App\Traits\CommonTrait;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\Helper;
use App\Models\Product;
use App\Models\Seller;

class InvoiceExport implements FromCollection,WithHeadings
{
    use CommonTrait;
    public function __construct()
    {
        $this->heading = ['Date', 'Invoice No', 'Buyer Name', 'GSTIN', 'State', 'Gross Total', 'IGST', 'CGST', 'SGST','Grand Total', 'Quantity'];

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $sellerId = $this->getSellerId();
        $seller   = Seller::find($sellerId);
        $seller->load('getUser');
            $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
            ->join('users','users.id','buyers.user_id')  
            ->join('states','states.state_id','buyers.state_id')
            ->where('invoices.seller_id',$sellerId)
            ->where('invoice_type','Invoice')
            ->get();
            
            $data->transform(function ($item) use ($seller){
                
                $invoice = Invoice::find($item->invoice_id);
                $invoice->load('products');
                $grossTotal = 0;
                $quantity   = 0;
                $unit = '';

                foreach ($invoice->products as $key => $product) {
                    $grossTotal += $product->quantity * $product->price; 
                    $quantity   += $product->quantity;

                    $tproduct = Product::find($product->product_id);
                    $tproduct->load('unit');
                    $unit = $tproduct->unit->name;
                }

                $generatorData1 = [
                    Helper::getDate($item->created_at),
                    $item->invoice_no,
                    $item->user_fname,
                    $item->gst_no, 
                    $item->name, 
                    $grossTotal,
                    Helper::getTaxAmount($grossTotal, $item->igst) . '(' . ($item->igst ?? '0') . '%)',
                    Helper::getTaxAmount($grossTotal, $item->cgst) . '(' . ($item->cgst ?? '0') . '%)',
                    Helper::getTaxAmount($grossTotal, $item->sgst) . '(' . ($item->sgst ?? '0') . '%)',                    
                    $grossTotal + Helper::getTaxAmount($grossTotal,$item->igst) + Helper::getTaxAmount($grossTotal,$item->cgst) + Helper::getTaxAmount($grossTotal,$item->sgst),
                    $quantity.'('.$unit.')'
                ];

                return $generatorData1;
            });

            return $data;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return $this->heading;
        
    }
}
